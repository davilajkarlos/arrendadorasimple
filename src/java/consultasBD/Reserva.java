/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package consultasBD;

import Generico.Conexion;
import Generico.FuncionesGenericas;
import static consultasBD.Genericas.ObtenerId;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author k1fox
 */
public class Reserva {
    public static JSONObject AccionReserva(JSONObject obj, String accion) throws Exception {
        Conexion objCon = new Conexion();
        Connection con = objCon.getConexion();
        JSONObject error = new JSONObject();
        try {
            switch(accion){
                case "Alta":
                     JSONObject objeto = GuardarReservaDB(con, obj);
                     obj.put("xIdReserva", objeto.get("ID"));
                     ReservaAutos.GuardarReserva_AutosDB(con, obj);
                     return objeto;                 
                    
                case "Baja":
                    return EliminarReservaDB(con, obj);
                    
                case "Modificacion":
                    return ModificarReservaDB(con, obj);
                    
                default:
                    error.put("Cod", -1);
                    error.put("Mensaje", "El argumento recibido es inválido");
                    error.put("Nivel", "Dominio");
            }
            
        } catch (Exception ex) {
            con.rollback();
            Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, ex);
            if (FuncionesGenericas.IsJSONObject(ex.getMessage())){
                error = new JSONObject(ex.getMessage());
            } else {
                error.put("Cod", -1);
                error.put("Mensaje", ex.getMessage());
                error.put("Nivel", "Controlador");
            }
        } finally {
            con.close();
        }
        return error;
    }
    
    public static JSONObject GuardarReservaDB (Connection con, JSONObject obj) throws Exception{
        JSONObject retorno = new JSONObject();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("insert into clasificacion values (null, ?, ?, 1, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

            st.setString(1, obj.getString("xReservaOCompra"));
            st.setString(2, obj.getString("xFechaReserva"));
            st.setString(3, obj.getString("xFechaEntrega"));
            st.setString(4, obj.getString("xFechaDevolucion"));
            st.setString(5, obj.getString("xHora"));
            st.setFloat(6, obj.getFloat("xPrecio"));
            st.setString(7, obj.getString("xMetodoPago"));
            st.setInt(8, obj.getInt("xIdCliente"));
            st.setInt(9, obj.getInt("xIdUsuario"));
            st.setInt(10, obj.getInt("xKilometrajeInicio"));
            st.setInt(11, obj.getInt("xKilometrajeFinal"));

            st.executeUpdate();
            
            retorno.put("Cod", 0);
            retorno.put("Mensaje", "Se ha guardado correctamente la Reserva");
            retorno.put("ID", ObtenerId(con));
            
        } catch (SQLException ex) {
            retorno.put("Cod", ex.getErrorCode());
            retorno.put("Mensaje", ex.getMessage());
            retorno.put("Nivel", "Persistencia");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
    
    public static JSONObject ModificarReservaDB(Connection con, JSONObject obj) throws Exception{
        JSONObject retorno = new JSONObject();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("update clasificacion " +
                                        " set `SeleccionRoC` = ?, " +
                                        " `Fecha_Reserva` = ?, " +
                                        " `Fecha_Entrega` = ?, " +
                                        " `FechaDevolucion` = ?, " +
                                        " `Hora` = ?, " +
                                        " `Precio` = ?, " +
                                        " `MetodoPago` = ?, " +
                                        " `Clientes_idClientes` = ?, " +
                                        " `_idUsuarios` = ?, " +
                                        " `kilometrajeInicio` = ?, " +
                                        " `kilometrajeFinal` = ?, " +
                                        " where `idReserva`=?");
            
            st.setString(1, obj.getString("xReservaOCompra"));
            st.setString(2, obj.getString("xFechaReserva"));
            st.setString(3, obj.getString("xFechaEntrega"));
            st.setString(4, obj.getString("xFechaDevolucion"));
            st.setString(5, obj.getString("xHora"));
            st.setFloat(6, obj.getFloat("xPrecio"));
            st.setString(7, obj.getString("xMetodoPago"));
            st.setInt(8, obj.getInt("xIdCliente"));
            st.setInt(9, obj.getInt("xIdUsuario"));
            st.setInt(10, obj.getInt("xKilometrajeInicio"));
            st.setInt(11, obj.getInt("xKilometrajeFinal"));
            st.setInt(12, obj.getInt("xId"));

            st.executeUpdate();
            
            retorno.put("Cod", 0);
            retorno.put("Mensaje", "Se ha modificado la Reserva");
            retorno.put("ID", obj.getInt("xId"));
            
        } catch (SQLException ex) {
            retorno.put("Cod", ex.getErrorCode());
            retorno.put("Mensaje", ex.getMessage());
            retorno.put("Nivel", "Persistencia");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
    
    public static JSONObject EliminarReservaDB (Connection con, JSONObject obj) throws Exception{
        JSONObject retorno = new JSONObject();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("update reserva set activosn=0 where idReserva=?");

            st.setInt(1, obj.getInt("xId"));

            st.executeUpdate();
            
            retorno.put("Cod", 0);
            retorno.put("Mensaje", "Se ha eliminado la Reserva");
            retorno.put("ID", obj.getInt("xId"));
            
        } catch (SQLException ex) {
            retorno.put("Cod", ex.getErrorCode());
            retorno.put("Mensaje", ex.getMessage());
            retorno.put("Nivel", "Persistencia");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
    
    public static JSONArray ColeccionReservaes(JSONObject obj, String accion) throws Exception{
         Conexion objCon = new Conexion();
         Connection con = objCon.getConexion();
         JSONObject error = new JSONObject();
         JSONArray resultado = new JSONArray();
         try {
            switch(accion){
                case "Todos":
                    return ObtenerColeccionReservaesDB(con, obj);
                    
                default:
                    error.put("Cod", -1);
                    error.put("Mensaje", "El argumento recibido es inválido");
                    error.put("Nivel", "Dominio");
            }
            
        } catch (Exception ex) {
            con.rollback();
            Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, ex);
            if (FuncionesGenericas.IsJSONObject(ex.getMessage())){
                error = new JSONObject(ex.getMessage());
            } else {
                error.put("Cod", -1);
                error.put("Mensaje", ex.getMessage());
                error.put("Nivel", "Controlador");
            }
        } finally {
            con.close();
        }
         resultado.put(error);
        return resultado;
    }
    
    public static JSONArray ObtenerColeccionReservaesDB(Connection con, JSONObject obj) throws Exception{
        JSONArray retorno = new JSONArray();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            String where = "";
            if (obj == null){
                obj = new JSONObject();
            }
            
            if (obj.has("IdReserva")){
                where = " and idReserva = ?";
            }
            st = con.prepareStatement("select " +
                                    "		res.*, " +
                                    "	 	cli.`idClientes`, " +
                                    "	 	cli.`Documento`, " +
                                    "	 	cli.`Nombre`, " +
                                    "	 	cli.`Apellido`, " +
                                    "	 	usu.`idUsuarios`, " +
                                    "	 	usu.`NombreUsuario`, " +
                                    "	 	aut.`idAutos`, " +
                                    "	 	aut.`Matricula`, " +
                                    "	 	mco.`_idMarca`, " +
                                    "	 	mco.`Modelo`, " +
                                    "	 	mar.`idMarca`, " +
                                    "	 	mar.`Marca` " +
                                    " 	from reserva res " +
                                    " 	inner join clientes cli on " +
                                    " 		res.`Clientes_idClientes` = cli.`idClientes` " +
                                    " 	inner join usuarios usu on " +
                                    " 		res.`_idUsuarios` = usu.`idUsuarios` " +
                                    " 	inner join corresponde cor on " +
                                    " 		res.`idReserva` = cor.`_idReserva` " +
                                    " 	inner join autos aut on " +
                                    " 		cor.`_idAutos` = aut.`idAutos` " +
                                    " 	inner join modelocoche mco on " +
                                    " 		mco.`idModeloAutos` = aut.`Modelo` " +
                                    "	inner join marca mar on " +
                                    "		mco.`_idMarca` = mar.`idMarca` " +
                                    "	where res.`ActivoSN` = 1 "+
                                        where);
            
            if (obj.has("IdReserva")){
                st.setInt(1, obj.getInt("IdReserva"));
            }
            rs = st.executeQuery();
            while (rs.next()) {
                
                JSONObject clasificacion = new JSONObject();
                clasificacion.put("Id", rs.getInt("idReserva"));
                clasificacion.put("IdCliente", rs.getInt("idClientes"));
                clasificacion.put("CI", rs.getInt("Documento"));
                clasificacion.put("IdUsuario", rs.getInt("idUsuarios"));
                clasificacion.put("IdAuto", rs.getInt("idAutos"));
                clasificacion.put("IdModelo", rs.getInt("idModeloAutos"));
                clasificacion.put("IdMarca", rs.getInt("idMarca"));
                clasificacion.put("IdCorresponde", rs.getInt("idCorresponde"));
                clasificacion.put("ReservaCompra", rs.getString("SeleccionRoC"));
                clasificacion.put("FechaReserva", rs.getString("Fecha_Reserva"));
                clasificacion.put("FechaEntrega", rs.getString("Fecha_Entrega"));
                clasificacion.put("FechaDevolucion", rs.getString("FechaDevolucion"));
                clasificacion.put("Hora", rs.getString("Hora"));
                clasificacion.put("Precio", rs.getString("Precio"));
                clasificacion.put("MetodoPago", rs.getString("MetodoPago"));
                clasificacion.put("NombreCliente", rs.getString("Nombre"));
                clasificacion.put("ApellidoCliente", rs.getString("Apellido"));
                clasificacion.put("NombreUsuario", rs.getString("NombreUsuario"));
                clasificacion.put("KilometrajeInicio", rs.getString("KilometrajeInicio"));
                clasificacion.put("KilometrajeFinal", rs.getString("KilometrajeFinal"));
                clasificacion.put("Matricula", rs.getString("Matricula"));
                clasificacion.put("Modelo", rs.getString("Modelo"));
                clasificacion.put("Marca", rs.getString("Marca"));
                
                retorno.put(clasificacion);
            }
            
        } catch (SQLException ex) {
            throw new Exception("Persistencia--> " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
}
