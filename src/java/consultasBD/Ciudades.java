/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package consultasBD;

import Generico.Conexion;
import Generico.FuncionesGenericas;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author k1fox
 */
public class Ciudades {
    
    public static JSONObject AccionCiudades(JSONObject obj, String accion) throws Exception {
        Conexion objCon = new Conexion();
        Connection con = objCon.getConexion();
        JSONObject error = new JSONObject();
        try {
            switch(accion){
                case "Alta":
                    return GuardarCiudadesDB(con, obj);
                    
                case "Baja":
                    return EliminarCiudadesDB(con, obj);
                    
                case "Modificacion":
                    return ModificarCiudadesDB(con, obj);
                    
                default:
                    error.put("Cod", -1);
                    error.put("Mensaje", "El argumento recibido es inválido");
                    error.put("Nivel", "Dominio");
            }
            
        } catch (Exception ex) {
            con.rollback();
            Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, ex);
            if (FuncionesGenericas.IsJSONObject(ex.getMessage())){
                error = new JSONObject(ex.getMessage());
            } else {
                error.put("Cod", -1);
                error.put("Mensaje", ex.getMessage());
                error.put("Nivel", "Controlador");
            }
        } finally {
            con.close();
        }
        return error;
    }
    
    public static JSONArray ColeccionCiudades(JSONObject obj, String accion) throws Exception{
         Conexion objCon = new Conexion();
         Connection con = objCon.getConexion();
         JSONObject error = new JSONObject();
         JSONArray resultado = new JSONArray();
         try {
            switch(accion){
                case "Todos":
                    return ObtenerColeccionCiudades(con, obj);
                    
                default:
                    error.put("Cod", -1);
                    error.put("Mensaje", "El argumento recibido es inválido");
                    error.put("Nivel", "Dominio");
            }
            
        } catch (Exception ex) {
            con.rollback();
            Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, ex);
            if (FuncionesGenericas.IsJSONObject(ex.getMessage())){
                error = new JSONObject(ex.getMessage());
            } else {
                error.put("Cod", -1);
                error.put("Mensaje", ex.getMessage());
                error.put("Nivel", "Controlador");
            }
        } finally {
            con.close();
        }
         resultado.put(error);
        return resultado;
    }
    
    public static JSONObject GuardarCiudadesDB (Connection con, JSONObject obj) throws Exception{
        JSONObject retorno = new JSONObject();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("insert into ciudad values (null, ?, ?, 1)");

            st.setString(1, obj.getString("xNombre"));
            st.setInt(2, obj.getInt("xIdDepartamento"));

            st.executeUpdate();
            
            retorno.put("Cod", 0);
            retorno.put("Mensaje", "Se ha guardado correctamente la Ciudad");
            retorno.put("ID", ObtenerCiudadDB(con, obj).getInt("Id"));
            
        } catch (SQLException ex) {
            retorno.put("Cod", ex.getErrorCode());
            retorno.put("Mensaje", ex.getMessage());
            retorno.put("Nivel", "Persistencia");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
    
    public static JSONObject ModificarCiudadesDB(Connection con, JSONObject obj) throws Exception{
        JSONObject retorno = new JSONObject();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("update ciudad set nombre=?, _idDepartamento=? where idCiudad=?");

            st.setString(1, obj.getString("xNombre"));
            st.setInt(2, obj.getInt("xIdDepartamento"));
            st.setInt(3, obj.getInt("xId"));

            st.executeUpdate();
            
            retorno.put("Cod", 0);
            retorno.put("Mensaje", "Se ha modificado la Ciudad");
            retorno.put("ID", obj.getInt("xId"));
            
        } catch (SQLException ex) {
            retorno.put("Cod", ex.getErrorCode());
            retorno.put("Mensaje", ex.getMessage());
            retorno.put("Nivel", "Persistencia");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
    
    public static JSONObject EliminarCiudadesDB (Connection con, JSONObject obj) throws Exception{
        JSONObject retorno = new JSONObject();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("update ciudad set activosn=0 where idCiudad=?");

            st.setInt(1, obj.getInt("xId"));

            st.executeUpdate();
            
            retorno.put("Cod", 0);
            retorno.put("Mensaje", "Se ha eliminado la Ciudad");
            retorno.put("ID", obj.getInt("xId"));
            
        } catch (SQLException ex) {
            retorno.put("Cod", ex.getErrorCode());
            retorno.put("Mensaje", ex.getMessage());
            retorno.put("Nivel", "Persistencia");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
    
    public static JSONObject ObtenerCiudadDB(Connection con, JSONObject obj) throws Exception{
        JSONObject retorno = new JSONObject();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("select * from ciudad where nombre = ? and _idDepartamento = ?");
            
            st.setString(1, obj.getString("xNombre"));
            st.setInt(2, obj.getInt("xIdDepartamento"));
            
            rs = st.executeQuery();
            if (rs.next()) {
                retorno.put("Id", rs.getInt("idCiudad"));
                retorno.put("Nombre", rs.getString("Nombre"));
                retorno.put("Departamento", rs.getInt("_idDepartamento"));
            }
            
        } catch (SQLException ex) {
            throw new Exception("Persistencia--> " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
    
    public static JSONArray ObtenerColeccionCiudades(Connection con, JSONObject obj) throws Exception{
        JSONArray retorno = new JSONArray();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            String where = "";
            if (obj == null){
                obj = new JSONObject();
            }
            
            if (obj.has("IdDepartamento")){
                where = "and d.idDepartamento = ?";
            }
            
            st = con.prepareStatement("select c.`idCiudad`, " +
                                        "c.`Nombre`as Ciudad," +
                                        "d.`idDepartamento`, " +
                                        "d.`Nombre`as Departamento, " +
                                        "p.`idPais`, " +
                                        "p.`Nombre` as Pais " +
                                        "from ciudad c " +
                                        "inner join departamento d on " +
                                        "c._idDepartamento = d.idDepartamento " +
                                        "inner join pais p on " +
                                        "d._idPais = p.idPais " +
                                        "where c.idCiudad <> 0 " +
                                        "and c.activosn = 1 "+
                                        where);
            
            if (obj.has("IdDepartamento")){
                st.setInt(1, obj.getInt("IdDepartamento"));
            }
            
            rs = st.executeQuery();
            while (rs.next()) {
                
                JSONObject ciudad = new JSONObject();
                ciudad.put("Id", rs.getInt("idCiudad"));
                ciudad.put("IdDepartamento", rs.getInt("idDepartamento"));
                ciudad.put("IdPais", rs.getInt("idPais"));
                ciudad.put("Ciudad", rs.getString("Ciudad"));
                ciudad.put("Departamento", rs.getString("Departamento"));
                ciudad.put("Pais", rs.getString("Pais"));
                retorno.put(ciudad);
            }
            
        } catch (SQLException ex) {
            throw new Exception("Persistencia--> " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
}
