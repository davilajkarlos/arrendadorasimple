/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package consultasBD;

import Generico.Conexion;
import Generico.FuncionesFecha;
import Generico.FuncionesGenericas;
import static consultasBD.Genericas.ObtenerId;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author k1fox
 */
public class Depositos {
    public static JSONObject AccionDeposito(JSONObject obj, String accion) throws Exception {
        Conexion objCon = new Conexion();
        Connection con = objCon.getConexion();
        JSONObject error = new JSONObject();
        try {
            switch(accion){
                case "Alta":
                    JSONObject deposito = GuardarDepositoDB(con, obj);
                    deposito.get("ID");
                    obj.put("xIdDeposito", deposito.get("ID"));
                    SucursalesDepositos.GuardarSucursal_DepositoDB(con, obj);
                    return deposito;
                case "Baja":
                    return EliminarDepositoDB(con, obj);
                    
                case "Modificacion":
                    return ModificarDepositoDB(con, obj);
                    
                default:
                    error.put("Cod", -1);
                    error.put("Mensaje", "El argumento recibido es inválido");
                    error.put("Nivel", "Dominio");
            }
            con.commit();
        } catch (Exception ex) {
            con.rollback();
            Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, ex);
            if (FuncionesGenericas.IsJSONObject(ex.getMessage())){
                error = new JSONObject(ex.getMessage());
            } else {
                error.put("Cod", -1);
                error.put("Mensaje", ex.getMessage());
                error.put("Nivel", "Controlador");
            }
        } finally {
            con.close();
        }
        return error;
    }
    
    public static JSONObject GuardarDepositoDB (Connection con, JSONObject obj) throws Exception{
        JSONObject retorno = new JSONObject();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("insert into depositos values (null, ?, 1, ?, ?, ?)");

            st.setString(1, obj.getString("xNombre"));
            st.setString(2, obj.getString("xLocalidad"));
            st.setString(3, obj.getString("xNombreCalle"));
            st.setString(4, obj.getString("xNumeroCalle"));

            st.executeUpdate();
            
            retorno.put("Cod", 0);
            retorno.put("Mensaje", "Se ha guardado correctamente el Deposito");
            retorno.put("ID", ObtenerId(con));
            
        } catch (SQLException ex) {
            retorno.put("Cod", ex.getErrorCode());
            retorno.put("Mensaje", ex.getMessage());
            retorno.put("Nivel", "Persistencia");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
    
    public static JSONObject ModificarDepositoDB(Connection con, JSONObject obj) throws Exception{
        JSONObject retorno = new JSONObject();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("update depositos " +
                                        " set `Nombre` = ?, " +
                                        " `Localidad` = ?, " +
                                        " `NombreCalle` = ?, " +
                                        " `NumeroCalle` = ? " +
                                        " where `idDepositos`=?");

            st.setString(1, obj.getString("xNombre"));
            st.setString(2, obj.getString("xLocalidad"));
            st.setString(3, obj.getString("xNombreCalle"));
            st.setString(4, obj.getString("xNumeroCalle"));
            st.setInt(5, obj.getInt("xId"));

            st.executeUpdate();
            
            retorno.put("Cod", 0);
            retorno.put("Mensaje", "Se ha modificado el deposito");
            retorno.put("ID", obj.getInt("xId"));
            
        } catch (SQLException ex) {
            retorno.put("Cod", ex.getErrorCode());
            retorno.put("Mensaje", ex.getMessage());
            retorno.put("Nivel", "Persistencia");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
    
    public static JSONObject EliminarDepositoDB (Connection con, JSONObject obj) throws Exception{
        JSONObject retorno = new JSONObject();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("update deposito set activosn=0 where idDepositos=?");

            st.setInt(1, obj.getInt("xId"));

            st.executeUpdate();
            
            retorno.put("Cod", 0);
            retorno.put("Mensaje", "Se ha eliminado el deposito");
            retorno.put("ID", obj.getInt("xId"));
            
        } catch (SQLException ex) {
            retorno.put("Cod", ex.getErrorCode());
            retorno.put("Mensaje", ex.getMessage());
            retorno.put("Nivel", "Persistencia");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
    
    public static JSONObject ObtenerDepositoDB(Connection con, JSONObject obj) throws Exception{
        JSONObject retorno = new JSONObject();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("select * from deposito where idDeposito=?");
            
            st.setInt(1, obj.getInt("xId"));
           
            rs = st.executeQuery();
            if (rs.next()) {
                retorno.put("Id", rs.getInt("idDeposito"));
                retorno.put("Nombre", rs.getString("Nombre"));
                retorno.put("Localidad", rs.getInt("localidad"));
                retorno.put("NombreCalle", rs.getInt("nombreCalle"));
                retorno.put("NumeroCalle", rs.getInt("numeroCalle"));
            }
            
        } catch (SQLException ex) {
            throw new Exception("Persistencia--> " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
    
    public static JSONArray ColeccionDepositoes(JSONObject obj, String accion) throws Exception{
         Conexion objCon = new Conexion();
         Connection con = objCon.getConexion();
         JSONObject error = new JSONObject();
         JSONArray resultado = new JSONArray();
         try {
            switch(accion){
                case "Todos":
                    return ObtenerColeccionDepositoesDB(con, obj);
                    
                default:
                    error.put("Cod", -1);
                    error.put("Mensaje", "El argumento recibido es inválido");
                    error.put("Nivel", "Dominio");
            }
            
        } catch (Exception ex) {
            con.rollback();
            Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, ex);
            if (FuncionesGenericas.IsJSONObject(ex.getMessage())){
                error = new JSONObject(ex.getMessage());
            } else {
                error.put("Cod", -1);
                error.put("Mensaje", ex.getMessage());
                error.put("Nivel", "Controlador");
            }
        } finally {
            con.close();
        }
         resultado.put(error);
        return resultado;
    }
    
    public static JSONArray ObtenerColeccionDepositoesDB(Connection con, JSONObject obj) throws Exception{
        JSONArray retorno = new JSONArray();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            String where = "";
            if (obj == null){
                obj = new JSONObject();
            }
            
            if (obj.has("IdDeposito")){
                where = " and idDepositos = ?";
            }
            st = con.prepareStatement("select * from depositos where `ActivoSN` = 1 "+
                                        where);
            
            if (obj.has("IdDeposito")){
                st.setInt(1, obj.getInt("IdDeposito"));
            }
            rs = st.executeQuery();
            while (rs.next()) {
                
                JSONObject deposito = new JSONObject();
                deposito.put("Id", rs.getInt("idDepositos"));
                deposito.put("Nombre", rs.getString("Nombre"));
                deposito.put("Localidad", rs.getString("localidad"));
                deposito.put("NombreCalle", rs.getString("nombreCalle"));
                deposito.put("NumeroCalle", rs.getString("numeroCalle"));
                
                retorno.put(deposito);
            }
            
        } catch (SQLException ex) {
            throw new Exception("Persistencia--> " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
}
