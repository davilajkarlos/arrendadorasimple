/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package consultasBD;

import Generico.Conexion;
import Generico.FuncionesGenericas;
import static consultasBD.Genericas.ObtenerId;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author k1fox
 */
public class Sucursal {
    
    public static JSONObject AccionSucursal(JSONObject obj, String accion) throws Exception {
        Conexion objCon = new Conexion();
        Connection con = objCon.getConexion();
        JSONObject error = new JSONObject();
        try {
            switch(accion){
                case "Alta":
                    return GuardarSucursalesDB(con, obj);
                    
                case "Baja":
                    return EliminarSucursalesDB(con, obj);
                    
                case "Modificacion":
                    return ModificarSucursalesDB(con, obj);
                    
                default:
                    error.put("Cod", -1);
                    error.put("Mensaje", "El argumento recibido es inválido");
                    error.put("Nivel", "Dominio");
            }
            
        } catch (Exception ex) {
            con.rollback();
            Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, ex);
            if (FuncionesGenericas.IsJSONObject(ex.getMessage())){
                error = new JSONObject(ex.getMessage());
            } else {
                error.put("Cod", -1);
                error.put("Mensaje", ex.getMessage());
                error.put("Nivel", "Controlador");
            }
        } finally {
            con.close();
        }
        return error;
    }
    
    public static JSONObject GuardarSucursalesDB (Connection con, JSONObject obj) throws Exception{
        JSONObject retorno = new JSONObject();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("insert into sucursal values (null, ?, ?, 1, ?, ?, ?, ?, ?)");

            st.setString(1, obj.getString("xNombre"));
            st.setInt(2, obj.getInt("xIdPais"));
            st.setInt(3, obj.getInt("xIdDepartamento"));
            st.setInt(4, obj.getInt("xIdCiudad"));
            st.setString(5, obj.getString("xLocalidad"));
            st.setString(6, obj.getString("xNombreCalle"));
            st.setString(7, obj.getString("xNumeroCalle"));

            st.executeUpdate();
            
            retorno.put("Cod", 0);
            retorno.put("Mensaje", "Se ha guardado correctamente la Sucursal");
            retorno.put("ID", ObtenerId(con));
            
        } catch (SQLException ex) {
            retorno.put("Cod", ex.getErrorCode());
            retorno.put("Mensaje", ex.getMessage());
            retorno.put("Nivel", "Persistencia");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
    
    public static JSONObject ModificarSucursalesDB(Connection con, JSONObject obj) throws Exception{
        JSONObject retorno = new JSONObject();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("update sucursal "
                                    + "set nombreDeposito=?, "
                                    + "_idPais=?, "
                                    + "_idDepartamento=?, "
                                    + "_idCiudad=?, "
                                    + "localidad=?, "
                                    + "nombreCalle=?, "
                                    + "numeroCalle=? "
                                    + "where idSucursal=?");

            st.setString(1, obj.getString("xNombre"));
            st.setInt(2, obj.getInt("xIdPais"));
            st.setInt(3, obj.getInt("xIdDepartamento"));
            st.setInt(4, obj.getInt("xIdCiudad"));
            st.setString(5, obj.getString("xLocalidad"));
            st.setString(6, obj.getString("xNombreCalle"));
            st.setString(7, obj.getString("xNumeroCalle"));
            st.setInt(8, obj.getInt("xId"));

            st.executeUpdate();
            
            retorno.put("Cod", 0);
            retorno.put("Mensaje", "Se ha modificado la sucursal");
            retorno.put("ID", obj.getInt("xId"));
            
        } catch (SQLException ex) {
            retorno.put("Cod", ex.getErrorCode());
            retorno.put("Mensaje", ex.getMessage());
            retorno.put("Nivel", "Persistencia");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
    
    public static JSONObject EliminarSucursalesDB (Connection con, JSONObject obj) throws Exception{
        JSONObject retorno = new JSONObject();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("update sucursal set activosn=0 where idSucursal=?");

            st.setInt(1, obj.getInt("xId"));

            st.executeUpdate();
            
            retorno.put("Cod", 0);
            retorno.put("Mensaje", "Se ha eliminado la sucursal");
            retorno.put("ID", obj.getInt("xId"));
            
        } catch (SQLException ex) {
            retorno.put("Cod", ex.getErrorCode());
            retorno.put("Mensaje", ex.getMessage());
            retorno.put("Nivel", "Persistencia");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
    
    public static JSONObject ObtenerSucursalDB(Connection con, JSONObject obj) throws Exception{
        JSONObject retorno = new JSONObject();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("select * from sucursal where idSucursal=?");
            
            st.setInt(1, obj.getInt("xId"));
           
            rs = st.executeQuery();
            if (rs.next()) {
                retorno.put("Id", rs.getInt("idSucursal"));
                retorno.put("Nombre", rs.getString("NombreDeposito"));
                retorno.put("Pais", rs.getInt("_idPais"));
                retorno.put("Departamento", rs.getInt("_idDepartamento"));
                retorno.put("Ciudad", rs.getInt("_idCiudad"));
                retorno.put("Localidad", rs.getInt("localidad"));
                retorno.put("NombreCalle", rs.getInt("nombreCalle"));
                retorno.put("NumeroCalle", rs.getInt("numeroCalle"));
            }
            
        } catch (SQLException ex) {
            throw new Exception("Persistencia--> " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
    
    public static JSONArray ColeccionSucursales(JSONObject obj, String accion) throws Exception{
         Conexion objCon = new Conexion();
         Connection con = objCon.getConexion();
         JSONObject error = new JSONObject();
         JSONArray resultado = new JSONArray();
         try {
            switch(accion){
                case "Todos":
                    return ObtenerColeccionSucursalesDB(con, obj);
                    
                default:
                    error.put("Cod", -1);
                    error.put("Mensaje", "El argumento recibido es inválido");
                    error.put("Nivel", "Dominio");
            }
            
        } catch (Exception ex) {
            con.rollback();
            Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, ex);
            if (FuncionesGenericas.IsJSONObject(ex.getMessage())){
                error = new JSONObject(ex.getMessage());
            } else {
                error.put("Cod", -1);
                error.put("Mensaje", ex.getMessage());
                error.put("Nivel", "Controlador");
            }
        } finally {
            con.close();
        }
         resultado.put(error);
        return resultado;
    }
    
    public static JSONArray ObtenerSucursales() throws Exception {
        Conexion objCon = new Conexion();
        Connection con = objCon.getConexion();

        try {
            return ObtenerSucursalesDB(con);
        } catch (Exception ex) {
            con.rollback();
            Logger.getLogger(Sucursal.class.getName()).log(Level.SEVERE, null, ex);
            throw new Exception("Dominio--> " + ex.getMessage());
        } finally {
            con.close();
        }
    }
    
    public static JSONArray ObtenerSucursalesDB(Connection con) throws Exception{
        JSONArray retorno = new JSONArray();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("select suc.*, ciu.`Nombre` as Ciudad from sucursal suc " +
                                        " inner join ciudad ciu on suc.`_idCiudad`=ciu.`idCiudad` " +
                                        " where suc.`ActivoSN` = 1 and suc.idSucursal <> 0");
            
            rs = st.executeQuery();
            while (rs.next()) {
                
                JSONObject sucursal = new JSONObject();
                sucursal.put("Id", rs.getInt("idSucursal"));
                sucursal.put("Nombre", rs.getString("NombreDeposito"));
                sucursal.put("Ciudad", rs.getString("Ciudad"));
                
                retorno.put(sucursal);
            }
            
        } catch (SQLException ex) {
            throw new Exception("Persistencia--> " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
    
    public static JSONArray ObtenerColeccionSucursalesDB(Connection con, JSONObject obj) throws Exception{
        JSONArray retorno = new JSONArray();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            String where = "";
            if (obj == null){
                obj = new JSONObject();
            }
            
            if (obj.has("IdSucursal")){
                where = " and suc.idSucursal = ?";
            }
            st = con.prepareStatement("select suc.*,  " +
                                        "ciu.`Nombre` as Ciudad, " +
                                        "dep.`Nombre` as Departamento, " +
                                        "pa.`Nombre` as Pais " +
                                        "from sucursal suc " +
                                        "inner join ciudad ciu on " +
                                        "suc.`_idCiudad`=ciu.`idCiudad` " +
                                        "inner join departamento dep on " +
                                        "ciu.`_idDepartamento` = dep.`idDepartamento` " +
                                        "inner join pais pa on " +
                                        "dep.`_idPais` = pa.`idPais` " +
                                        "where suc.`ActivoSN` = 1 " +
                                        "and `idSucursal` <> 0 "+
                                        where);
            
            if (obj.has("IdSucursal")){
                st.setInt(1, obj.getInt("IdSucursal"));
            }
            rs = st.executeQuery();
            while (rs.next()) {
                
                JSONObject sucursal = new JSONObject();
                sucursal.put("Id", rs.getInt("idSucursal"));
                sucursal.put("IdCiudad", rs.getInt("_idCiudad"));
                sucursal.put("IdDepartamento", rs.getInt("_idDepartamento"));
                sucursal.put("IdPais", rs.getInt("_idPais"));
                sucursal.put("Nombre", rs.getString("NombreDeposito"));
                sucursal.put("Localidad", rs.getString("Localidad"));
                sucursal.put("NombreCalle", rs.getString("NombreCalle"));
                sucursal.put("NumeroCalle", rs.getString("NumeroCalle"));
                sucursal.put("Ciudad", rs.getString("Ciudad"));
                sucursal.put("Departamento", rs.getString("Departamento"));
                sucursal.put("Pais", rs.getString("Pais"));
                
                retorno.put(sucursal);
            }
            
        } catch (SQLException ex) {
            throw new Exception("Persistencia--> " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
    
}
