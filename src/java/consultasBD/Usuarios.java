/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package consultasBD;

import Generico.Conexion;
import Generico.FuncionesGenericas;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author k1fox
 */
public class Usuarios {
    
    public static JSONObject AccionUsuario(JSONObject obj, String accion) throws Exception {
        Conexion objCon = new Conexion();
        Connection con = objCon.getConexion();
        JSONObject error = new JSONObject();
        try {
            switch(accion){
                case "Alta":
                    return GuardarUsuarioDB(con, obj);
                    
                case "Baja":
                    break;
                    
                case "Modificacion":
                    break;
                    
                default:
                    error.put("Cod", -1);
                    error.put("Mensaje", "El argumento recibido es inválido");
                    error.put("Nivel", "Dominio");
            }
            
        } catch (Exception ex) {
            con.rollback();
            Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, ex);
            if (FuncionesGenericas.IsJSONObject(ex.getMessage())){
                error = new JSONObject(ex.getMessage());
            } else {
                error.put("Cod", -1);
                error.put("Mensaje", ex.getMessage());
                error.put("Nivel", "Controlador");
            }
        } finally {
            con.close();
        }
        return error;
    }
    
    private static JSONObject GuardarUsuarioDB (Connection con, JSONObject obj) throws Exception{
        JSONObject retorno = new JSONObject();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("insert into usuarios (NombreUsuario, `Contrasena`, `_idTipoUsuarios`,`ActivoSN`,`_idSucursal`) " +
                                      "values (?,md5(?),?,?,?)");

            st.setString(1, obj.getString("xNombre"));
            st.setString(2, obj.getString("xContraseña"));
            st.setInt(3, obj.getInt("xTipoUsuario"));
            st.setInt(4, 1);
            st.setInt(5, obj.getInt("xIdSucursal"));

            st.executeUpdate();
            
            retorno.put("Cod", 0);
            retorno.put("Mensaje", "Se han guardado correctamente");
            retorno.put("ID", ObtenerUsuarioDB(con, obj).getInt("Id"));
            
        } catch (SQLException ex) {
            retorno.put("Cod", ex.getErrorCode());
            retorno.put("Mensaje", ex.getMessage());
            retorno.put("Nivel", "Persistencia");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
    
    private static long getId(Connection con) throws Exception{
        long autoIncremental = -1;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("SELECT `AUTO_INCREMENT` " +
                                "FROM  INFORMATION_SCHEMA.TABLES " +
                                "WHERE TABLE_SCHEMA = 'arrendadora' " +
                                "AND   TABLE_NAME = 'usuarios'");
            
            rs = st.executeQuery();
            if (rs.next()) {
                autoIncremental += rs.getLong("AUTO_INCREMENT");
            }
            
        } catch (SQLException ex) {
            throw new Exception("Persistencia--> " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return autoIncremental;
    }
    
    public static JSONObject ObtenerUsuarioDB(Connection con, JSONObject obj) throws Exception{
        JSONObject retorno = new JSONObject();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("select * from usuarios where nombreUsuario = ? and Contrasena = md5(?)");
            
            st.setString(1, obj.getString("xNombre"));
            st.setString(2, obj.getString("xContraseña"));
            
            rs = st.executeQuery();
            if (rs.next()) {
                retorno.put("Id", rs.getInt("idUsuarios"));
                retorno.put("Nombre", rs.getString("NombreUsuario"));
                retorno.put("Tipo", rs.getInt("_idTipoUsuarios"));
                retorno.put("Activo", rs.getInt("ActivoSN"));
                retorno.put("Sucursal", rs.getInt("_idSucursal"));
            }
            
        } catch (SQLException ex) {
            throw new Exception("Persistencia--> " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
    
    
}
