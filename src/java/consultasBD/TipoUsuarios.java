/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package consultasBD;

import Generico.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author k1fox
 */
public class TipoUsuarios {
    public static JSONArray ObtenerTiposUsuarios() throws Exception {
        Conexion objCon = new Conexion();
        Connection con = objCon.getConexion();

        try {
            return ObtenerTiposUsuariosDB(con);
        } catch (Exception ex) {
            con.rollback();
            Logger.getLogger(Sucursal.class.getName()).log(Level.SEVERE, null, ex);
            throw new Exception("Dominio--> " + ex.getMessage());
        } finally {
            con.close();
        }
    }
    
    private static JSONArray ObtenerTiposUsuariosDB(Connection con) throws Exception{
        JSONArray retorno = new JSONArray();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("select * from tipousuarios where `ActivoSN` = 1 and idTipoUsuarios <> 0");
            
            rs = st.executeQuery();
            while (rs.next()) {
                
                JSONObject tipoUsuario = new JSONObject();
                tipoUsuario.put("Id", rs.getInt("idTipoUsuarios"));
                tipoUsuario.put("Tipo", rs.getString("Tipo"));
                
                retorno.put(tipoUsuario);
            }
            
        } catch (SQLException ex) {
            throw new Exception("Persistencia--> " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
}
