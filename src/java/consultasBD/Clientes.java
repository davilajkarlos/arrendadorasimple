/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package consultasBD;

import Generico.Conexion;
import Generico.FuncionesFecha;
import Generico.FuncionesGenericas;
import static consultasBD.Genericas.ObtenerId;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author k1fox
 */
public class Clientes {
     public static JSONObject AccionCliente(JSONObject obj, String accion) throws Exception {
        Conexion objCon = new Conexion();
        Connection con = objCon.getConexion();
        JSONObject error = new JSONObject();
        try {
            switch(accion){
                case "Alta":
                    return GuardarClienteDB(con, obj);
                    
                case "Baja":
                    return EliminarClienteDB(con, obj);
                    
                case "Modificacion":
                    return ModificarClienteDB(con, obj);
                    
                default:
                    error.put("Cod", -1);
                    error.put("Mensaje", "El argumento recibido es inválido");
                    error.put("Nivel", "Dominio");
            }
            
        } catch (Exception ex) {
            con.rollback();
            Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, ex);
            if (FuncionesGenericas.IsJSONObject(ex.getMessage())){
                error = new JSONObject(ex.getMessage());
            } else {
                error.put("Cod", -1);
                error.put("Mensaje", ex.getMessage());
                error.put("Nivel", "Controlador");
            }
        } finally {
            con.close();
        }
        return error;
    }
    
    public static JSONObject GuardarClienteDB (Connection con, JSONObject obj) throws Exception{
        JSONObject retorno = new JSONObject();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("insert into clientes values (null, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,1)");

            st.setLong(1, obj.getLong("xCI"));
            st.setString(2, obj.getString("xNombre"));
            st.setString(3, obj.getString("xApellido"));
            st.setString(4, FuncionesFecha.normalizarFecha(obj.getString("xFecha")));
            st.setString(5, obj.getString("xSexo"));
            st.setInt(6, obj.getInt("xIdPais"));
            st.setInt(7, obj.getInt("xIdCiudad"));
            st.setInt(8, obj.getInt("xIdDepartamento"));
            st.setString(9, obj.getString("xCalle"));
            st.setString(10, obj.getString("xNumeroCasa"));
            st.setString(11, obj.getString("xCorreo"));

            st.executeUpdate();
            
            retorno.put("Cod", 0);
            retorno.put("Mensaje", "Se ha guardado correctamente el Cliente");
            retorno.put("ID", ObtenerId(con));
            
        } catch (SQLException ex) {
            retorno.put("Cod", ex.getErrorCode());
            retorno.put("Mensaje", ex.getMessage());
            retorno.put("Nivel", "Persistencia");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
    
    public static JSONObject ModificarClienteDB(Connection con, JSONObject obj) throws Exception{
        JSONObject retorno = new JSONObject();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("update clientes " +
                                        "set `Nombre`=?, " +
                                        "`Apellido`=?, " +
                                        "`FechaNacimiento`=?, " +
                                        "`Sexo`=?, " +
                                        "`_idPais`=?, " +
                                        "`_idDepartamento`=?, " +
                                        "`_idCiudad`=?, " +
                                        "`Calle`=?, " +
                                        "`Correo`=? " +
                                        "where `idClientes`=?");

            st.setLong(1, obj.getLong("xCI"));
            st.setString(2, obj.getString("xNombre"));
            st.setString(3, obj.getString("xApellido"));
            st.setString(4, FuncionesFecha.normalizarFecha(obj.getString("xFecha")));
            st.setString(5, obj.getString("xSexo"));
            st.setInt(6, obj.getInt("xIdPais"));
            st.setInt(7, obj.getInt("xIdCiudad"));
            st.setInt(8, obj.getInt("xIdDepartamento"));
            st.setString(9, obj.getString("xCalle"));
            st.setString(10, obj.getString("xNumeroCasa"));
            st.setString(11, obj.getString("xCorreo"));
            st.setInt(12, obj.getInt("xId"));

            st.executeUpdate();
            
            retorno.put("Cod", 0);
            retorno.put("Mensaje", "Se ha modificado el cliente");
            retorno.put("ID", obj.getInt("xId"));
            
        } catch (SQLException ex) {
            retorno.put("Cod", ex.getErrorCode());
            retorno.put("Mensaje", ex.getMessage());
            retorno.put("Nivel", "Persistencia");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
    
    public static JSONObject EliminarClienteDB (Connection con, JSONObject obj) throws Exception{
        JSONObject retorno = new JSONObject();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("update cliente set activosn=0 where idClientes=?");

            st.setInt(1, obj.getInt("xId"));

            st.executeUpdate();
            
            retorno.put("Cod", 0);
            retorno.put("Mensaje", "Se ha eliminado el cliente");
            retorno.put("ID", obj.getInt("xId"));
            
        } catch (SQLException ex) {
            retorno.put("Cod", ex.getErrorCode());
            retorno.put("Mensaje", ex.getMessage());
            retorno.put("Nivel", "Persistencia");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
    
    public static JSONObject ObtenerClienteDB(Connection con, JSONObject obj) throws Exception{
        JSONObject retorno = new JSONObject();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("select * from  cliente where idCliente=?");
            
            st.setInt(1, obj.getInt("xId"));
           
            rs = st.executeQuery();
            if (rs.next()) {
                retorno.put("Id", rs.getInt("idCliente"));
                retorno.put("Nombre", rs.getString("NombreDeposito"));
                retorno.put("Pais", rs.getInt("_idPais"));
                retorno.put("Departamento", rs.getInt("_idDepartamento"));
                retorno.put("Ciudad", rs.getInt("_idCiudad"));
                retorno.put("Localidad", rs.getInt("localidad"));
                retorno.put("NombreCalle", rs.getInt("nombreCalle"));
                retorno.put("NumeroCalle", rs.getInt("numeroCalle"));
            }
            
        } catch (SQLException ex) {
            throw new Exception("Persistencia--> " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
    
    public static JSONArray ColeccionClientees(JSONObject obj, String accion) throws Exception{
         Conexion objCon = new Conexion();
         Connection con = objCon.getConexion();
         JSONObject error = new JSONObject();
         JSONArray resultado = new JSONArray();
         try {
            switch(accion){
                case "Todos":
                    return ObtenerColeccionClienteesDB(con, obj);
                    
                default:
                    error.put("Cod", -1);
                    error.put("Mensaje", "El argumento recibido es inválido");
                    error.put("Nivel", "Dominio");
            }
            
        } catch (Exception ex) {
            con.rollback();
            Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, ex);
            if (FuncionesGenericas.IsJSONObject(ex.getMessage())){
                error = new JSONObject(ex.getMessage());
            } else {
                error.put("Cod", -1);
                error.put("Mensaje", ex.getMessage());
                error.put("Nivel", "Controlador");
            }
        } finally {
            con.close();
        }
         resultado.put(error);
        return resultado;
    }
    
    public static JSONArray ObtenerColeccionClienteesDB(Connection con, JSONObject obj) throws Exception{
        JSONArray retorno = new JSONArray();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            String where = "";
            if (obj == null){
                obj = new JSONObject();
            }
            
            if (obj.has("IdCliente")){
                where = " and cli.idClientes = ?";
            }
            st = con.prepareStatement("select cli.*, " +
                                        "ciu.`Nombre` as Ciudad, " +
                                        "dep.`Nombre` as Departamento, " +
                                        "pa.`Nombre` as Pais " +
                                        "from clientes cli " +
                                        "inner join ciudad ciu on " +
                                        "cli.`_idCiudad`=ciu.`idCiudad` " +
                                        "inner join departamento dep on " +
                                        "ciu.`_idDepartamento` = dep.`idDepartamento` " +
                                        "inner join pais pa on " +
                                        "dep.`_idPais` = pa.`idPais` " +
                                        "where cli.`ActivoSN` = 1 "+
                                        where);
            
            if (obj.has("IdCliente")){
                st.setInt(1, obj.getInt("IdCliente"));
            }
            rs = st.executeQuery();
            while (rs.next()) {
                
                JSONObject cliente = new JSONObject();
                cliente.put("Id", rs.getInt("idCliente"));
                cliente.put("CI", rs.getInt("Documento"));
                cliente.put("IdCiudad", rs.getInt("_idCiudad"));
                cliente.put("IdDepartamento", rs.getInt("_idDepartamento"));
                cliente.put("IdPais", rs.getInt("_idPais"));
                cliente.put("Nombre", rs.getString("Nombre"));
                cliente.put("Apellido", rs.getString("Apellido"));
                cliente.put("Fecha", rs.getString("FechaNacimiento"));
                cliente.put("Sexo", rs.getString("Sexo"));
                cliente.put("Calle", rs.getString("Calle"));
                cliente.put("NumeroCasa", rs.getString("NumeroCasa"));
                cliente.put("NumeroCalle", rs.getString("NumeroCalle"));
                cliente.put("Correo", rs.getString("Correo"));
                cliente.put("Ciudad", rs.getString("Ciudad"));
                cliente.put("Departamento", rs.getString("Departamento"));
                cliente.put("Pais", rs.getString("Pais"));
                
                retorno.put(cliente);
            }
            
        } catch (SQLException ex) {
            throw new Exception("Persistencia--> " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
    
}
