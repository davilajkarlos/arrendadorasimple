/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package consultasBD;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.json.JSONObject;

/**
 *
 * @author k1fox
 */
public class Genericas {
    
    public static long ObtenerId(Connection con) throws Exception{
        long retorno = -1;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement(" select last_insert_id() ");
            
           
            rs = st.executeQuery();
            if (rs.next()) {
                retorno = rs.getLong(1);
            }
            
        } catch (SQLException ex) {
            throw new Exception("Persistencia--> " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
}
