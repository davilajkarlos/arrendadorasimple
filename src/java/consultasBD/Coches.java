/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package consultasBD;

import Generico.Conexion;
import Generico.FuncionesGenericas;
import static consultasBD.Genericas.ObtenerId;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author k1fox
 */
public class Coches {
    public static JSONObject AccionCoche(JSONObject obj, String accion) throws Exception {
        Conexion objCon = new Conexion();
        Connection con = objCon.getConexion();
        JSONObject error = new JSONObject();
        try {
            switch(accion){
                case "Alta":
                    return GuardarCocheDB(con, obj);
                    
                case "Baja":
                    return EliminarCocheDB(con, obj);
                    
                case "Modificacion":
                    return ModificarCocheDB(con, obj);
                    
                default:
                    error.put("Cod", -1);
                    error.put("Mensaje", "El argumento recibido es inválido");
                    error.put("Nivel", "Dominio");
            }
            
        } catch (Exception ex) {
            con.rollback();
            Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, ex);
            if (FuncionesGenericas.IsJSONObject(ex.getMessage())){
                error = new JSONObject(ex.getMessage());
            } else {
                error.put("Cod", -1);
                error.put("Mensaje", ex.getMessage());
                error.put("Nivel", "Controlador");
            }
        } finally {
            con.close();
        }
        return error;
    }
    
    public static JSONObject GuardarCocheDB (Connection con, JSONObject obj) throws Exception{
        JSONObject retorno = new JSONObject();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("insert into autos values (null, 1, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

            st.setInt(1, obj.getInt("xIdModelo"));
            st.setString(2, obj.getString("xMatricula"));
            st.setString(3, obj.getString("xColor"));
            st.setInt(4, obj.getInt("xDisponibilidad"));
            st.setDouble(5, obj.getDouble("xPrecio"));
            st.setInt(6, obj.getInt("xTipoClasificacion"));
            st.setInt(7, obj.getInt("xIdDeposito"));
            st.setInt(8, obj.getInt("xAsientos"));
            st.setInt(9, obj.getInt("xMaleta"));
            st.setInt(10, obj.getInt("xKilometrajeTotal"));


            st.executeUpdate();
            
            retorno.put("Cod", 0);
            retorno.put("Mensaje", "Se ha guardado correctamente aa Coche");
            retorno.put("ID", ObtenerId(con));
            
        } catch (SQLException ex) {
            retorno.put("Cod", ex.getErrorCode());
            retorno.put("Mensaje", ex.getMessage());
            retorno.put("Nivel", "Persistencia");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
    
    public static JSONObject ModificarCocheDB(Connection con, JSONObject obj) throws Exception{
        JSONObject retorno = new JSONObject();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("update autos " +
                                        " set `Modelo` = ?, " +
                                        "  `Matricula` = ?, " +
                                        "  `Color` = ?, " +
                                        "  `Disponibilidad` = ?, " +
                                        "  `Precio` = ?, " +
                                        "  `TipoClasificacion` = ?, " +
                                        "  `Depositos_idDepositos` = ?, " +
                                        "  `Asientos` = ?, " +
                                        "  `Maleta` = ?, " +
                                        "  `kilometrajeTotal` = ? " +
                                        " where `idAutos`=?");

            st.setInt(1, obj.getInt("xIdModelo"));
            st.setString(2, obj.getString("xMatricula"));
            st.setString(3, obj.getString("xColor"));
            st.setInt(4, obj.getInt("xDisponibilidad"));
            st.setDouble(5, obj.getDouble("xPrecio"));
            st.setInt(6, obj.getInt("xTipoClasificacion"));
            st.setInt(7, obj.getInt("xIdDeposito"));
            st.setInt(8, obj.getInt("xAsientos"));
            st.setInt(9, obj.getInt("xMaleta"));
            st.setInt(10, obj.getInt("xKilometrajeTotal"));
            st.setInt(11, obj.getInt("xId"));

            st.executeUpdate();
            
            retorno.put("Cod", 0);
            retorno.put("Mensaje", "Se ha modificado la Coche");
            retorno.put("ID", obj.getInt("xId"));
            
        } catch (SQLException ex) {
            retorno.put("Cod", ex.getErrorCode());
            retorno.put("Mensaje", ex.getMessage());
            retorno.put("Nivel", "Persistencia");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
    
    public static JSONObject EliminarCocheDB (Connection con, JSONObject obj) throws Exception{
        JSONObject retorno = new JSONObject();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("update autos set activosn=0 where idAutos=?");

            st.setInt(1, obj.getInt("xId"));

            st.executeUpdate();
            
            retorno.put("Cod", 0);
            retorno.put("Mensaje", "Se ha eliminado la Coche");
            retorno.put("ID", obj.getInt("xId"));
            
        } catch (SQLException ex) {
            retorno.put("Cod", ex.getErrorCode());
            retorno.put("Mensaje", ex.getMessage());
            retorno.put("Nivel", "Persistencia");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
    
    public static JSONObject ObtenerCocheDB(Connection con, JSONObject obj) throws Exception{
        JSONObject retorno = new JSONObject();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("select * from autos where idAutos=?");
            
            st.setInt(1, obj.getInt("xId"));
           
            rs = st.executeQuery();
            if (rs.next()) {
                retorno.put("Id", rs.getInt("idAutos"));
                retorno.put("Modelo", rs.getString("Modelo"));
                retorno.put("Matricula", rs.getString("Matricula"));
                retorno.put("Color", rs.getString("Color"));
                retorno.put("Disponibilidad", rs.getString("Disponibilidad"));
                retorno.put("Precio", rs.getString("Precio"));
                retorno.put("TipoClasificacion", rs.getString("TipoClasificacion"));
                retorno.put("IdDeposito", rs.getString("Depositos_idDepositos"));
                retorno.put("Asientos", rs.getString("Asientos"));
                retorno.put("Maleta", rs.getString("Maleta"));
                retorno.put("kilometrajeTotal", rs.getString("kilometrajeTotal"));
            }
            
        } catch (SQLException ex) {
            throw new Exception("Persistencia--> " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
    
    public static JSONArray ColeccionCoches(JSONObject obj, String accion) throws Exception{
         Conexion objCon = new Conexion();
         Connection con = objCon.getConexion();
         JSONObject error = new JSONObject();
         JSONArray resultado = new JSONArray();
         try {
            switch(accion){
                case "Todos":
                    return ObtenerColeccionCochesDB(con, obj);
                case "Disponibles":
                    return ObtenerColeccionCochesDisponiblesDB(con, obj);
                default:
                    error.put("Cod", -1);
                    error.put("Mensaje", "El argumento recibido es inválido");
                    error.put("Nivel", "Dominio");
            }
            
        } catch (Exception ex) {
            con.rollback();
            Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, ex);
            if (FuncionesGenericas.IsJSONObject(ex.getMessage())){
                error = new JSONObject(ex.getMessage());
            } else {
                error.put("Cod", -1);
                error.put("Mensaje", ex.getMessage());
                error.put("Nivel", "Controlador");
            }
        } finally {
            con.close();
        }
         resultado.put(error);
        return resultado;
    }
    
    public static JSONArray ObtenerColeccionCochesDB(Connection con, JSONObject obj) throws Exception{
        JSONArray retorno = new JSONArray();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            String where = "";
            if (obj == null){
                obj = new JSONObject();
            }
            
            if (obj.has("IdAutos")){
                where = " and aut.idAutos = ?";
            }
            st = con.prepareStatement("select aut.*, " +
                                    "	mco.`idModeloAutos` as idModelo, " +
                                    " 	mar.`idMarca` as idMarca, " +
                                    " 	cla.`idClasificacion` as idClasificacion, " +
                                    " 	dep.`idDepositos` as idDeposito, " +
                                    "	mco.`Modelo` as NombreModelo, " +
                                    "	mar.`Marca` as NombreMarca, " +
                                    "	cla.`Nombre` as NombreClasificacion, " +
                                    "	dep.`Nombre` as NombreDeposito " +
                                    "	from autos aut " +
                                    "	inner join modelocoche mco on " +
                                    "	aut.`Modelo` = mco.`idModeloAutos` " +
                                    "	inner join marca mar on " +
                                    "	mco.`_idMarca` = mar.`idMarca`" +
                                    "	inner join clasificacion cla on " +
                                    "	aut.`TipoClasificacion` = cla.`idClasificacion` " +
                                    "	inner join depositos dep on " +
                                    "	aut.`Depositos_idDepositos` = dep.`idDepositos` " +
                                    "	where aut.`ActivoSN` = 1"+
                                        where);
            
            if (obj.has("IdAutos")){
                st.setInt(1, obj.getInt("IdAutos"));
            }
            rs = st.executeQuery();
            while (rs.next()) {
                
                JSONObject autos = new JSONObject();
                autos.put("Id", rs.getInt("idAutos"));
                autos.put("IdDeposito", rs.getInt("IdDeposito"));
                autos.put("IdModelo", rs.getInt("IdModelo"));
                autos.put("IdMarca", rs.getInt("IdMarca"));
                autos.put("IdClasificacion", rs.getInt("IdClasificacion"));
                
                autos.put("Matricula", rs.getString("Matricula"));
                autos.put("Color", rs.getString("Color"));
                autos.put("Disponibilidad", rs.getString("Disponibilidad"));
                autos.put("Precio", rs.getString("Precio"));
                autos.put("Asientos", rs.getString("Asientos"));
                autos.put("Maleta", rs.getString("Maleta"));
                autos.put("KilometrajeTotal", rs.getString("kilometrajeTotal"));
                autos.put("Marca", rs.getString("NombreMarca"));
                autos.put("Modelo", rs.getString("NombreModelo"));
                autos.put("Clasificacion", rs.getString("NombreClasificacion"));
                autos.put("NombreDeposito", rs.getString("NombreDeposito"));
                
                retorno.put(autos);
            }
            
        } catch (SQLException ex) {
            throw new Exception("Persistencia--> " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
    public static JSONArray ObtenerColeccionCochesDisponiblesDB(Connection con, JSONObject obj) throws Exception{
        JSONArray retorno = new JSONArray();
        PreparedStatement st = null;
        ResultSet rs = null;
        int contador = 1;
        try {
            String where = "";
            if (obj == null){
                obj = new JSONObject();
            }
            if (obj.has("xIdMarca")){
                where += " and mar.idMarca = ? ";
            }
            if (obj.has("xIdModelo")){
                where += " and mco.idModeloAutos = ? ";
            }
             
            st = con.prepareStatement("select aut.*, " +
                                    "	mco.`idModeloAutos` as idModelo, " +
                                    " 	mar.`idMarca` as idMarca, " +
                                    " 	cla.`idClasificacion` as idClasificacion, " +
                                    " 	dep.`idDepositos` as idDeposito, " +
                                    "	mco.`Modelo` as NombreModelo, " +
                                    "	mar.`Marca` as NombreMarca, " +
                                    "	cla.`Nombre` as NombreClasificacion, " +
                                    "	dep.`Nombre` as NombreDeposito " +
                                    "	from autos aut " +
                                    "	inner join modelocoche mco on " +
                                    "	aut.`Modelo` = mco.`idModeloAutos` " +
                                    "	inner join marca mar on " +
                                    "	mco.`_idMarca` = mar.`idMarca`" +
                                    "	inner join clasificacion cla on " +
                                    "	aut.`TipoClasificacion` = cla.`idClasificacion` " +
                                    "	inner join depositos dep on " +
                                    "	aut.`Depositos_idDepositos` = dep.`idDepositos` " +
                                    "	where aut.`ActivoSN` = 1 " +
                                    "   and not exists (select * from corresponde cor " +
                                    "			inner join reserva res on " +
                                    "			res.`idReserva` = cor.`_idReserva` " +
                                    "			where cor.`_idAutos` = aut.`idAutos` " +
                                    "			and res.`Fecha_Reserva` between ? and ? " +
                                    "			and res.`Fecha_Entrega` between ? and ? ) "+
                                    where);
            
                st.setString(contador, obj.getString("xFechaReserva"));
                contador++;
                st.setString(contador, obj.getString("xFechaEntrega"));
                contador++;
                st.setString(contador, obj.getString("xFechaReserva"));
                contador++;
                st.setString(contador, obj.getString("xFechaEntrega"));
                contador++;
                 if (obj.has("xIdMarca")){
                    st.setInt(contador, obj.getInt("xIdMarca"));
                    contador++;
                }
                if (obj.has("xIdModelo")){
                    st.setInt(contador, obj.getInt("xIdModelo"));
                    contador++;
                }
            
            rs = st.executeQuery();
            while (rs.next()) {
                
                JSONObject autos = new JSONObject();
                autos.put("Id", rs.getInt("idAutos"));
                autos.put("IdDeposito", rs.getInt("IdDeposito"));
                autos.put("IdModelo", rs.getInt("IdModelo"));
                autos.put("IdMarca", rs.getInt("IdMarca"));
                autos.put("IdClasificacion", rs.getInt("IdClasificacion"));
                
                autos.put("Matricula", rs.getString("Matricula"));
                autos.put("Color", rs.getString("Color"));
                autos.put("Disponibilidad", rs.getString("Disponibilidad"));
                autos.put("Precio", rs.getString("Precio"));
                autos.put("Asientos", rs.getString("Asientos"));
                autos.put("Maleta", rs.getString("Maleta"));
                autos.put("KilometrajeTotal", rs.getString("kilometrajeTotal"));
                autos.put("Marca", rs.getString("NombreMarca"));
                autos.put("Modelo", rs.getString("NombreModelo"));
                autos.put("Clasificacion", rs.getString("NombreClasificacion"));
                autos.put("NombreDeposito", rs.getString("NombreDeposito"));
                
                retorno.put(autos);
            }
            
        } catch (SQLException ex) {
            throw new Exception("Persistencia--> " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
}
