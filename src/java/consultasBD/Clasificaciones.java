/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package consultasBD;

import Generico.Conexion;
import Generico.FuncionesGenericas;
import static consultasBD.Genericas.ObtenerId;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author k1fox
 */
public class Clasificaciones {
    public static JSONObject AccionClasificacion(JSONObject obj, String accion) throws Exception {
        Conexion objCon = new Conexion();
        Connection con = objCon.getConexion();
        JSONObject error = new JSONObject();
        try {
            switch(accion){
                case "Alta":
                    return GuardarClasificacionDB(con, obj);
                    
                case "Baja":
                    return EliminarClasificacionDB(con, obj);
                    
                case "Modificacion":
                    return ModificarClasificacionDB(con, obj);
                    
                default:
                    error.put("Cod", -1);
                    error.put("Mensaje", "El argumento recibido es inválido");
                    error.put("Nivel", "Dominio");
            }
            
        } catch (Exception ex) {
            con.rollback();
            Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, ex);
            if (FuncionesGenericas.IsJSONObject(ex.getMessage())){
                error = new JSONObject(ex.getMessage());
            } else {
                error.put("Cod", -1);
                error.put("Mensaje", ex.getMessage());
                error.put("Nivel", "Controlador");
            }
        } finally {
            con.close();
        }
        return error;
    }
    
    public static JSONObject GuardarClasificacionDB (Connection con, JSONObject obj) throws Exception{
        JSONObject retorno = new JSONObject();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("insert into clasificacion values (null, ?, 1)");

            st.setString(1, obj.getString("xNombre"));

            st.executeUpdate();
            
            retorno.put("Cod", 0);
            retorno.put("Mensaje", "Se ha guardado correctamente aa Clasificacion");
            retorno.put("ID", ObtenerId(con));
            
        } catch (SQLException ex) {
            retorno.put("Cod", ex.getErrorCode());
            retorno.put("Mensaje", ex.getMessage());
            retorno.put("Nivel", "Persistencia");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
    
    public static JSONObject ModificarClasificacionDB(Connection con, JSONObject obj) throws Exception{
        JSONObject retorno = new JSONObject();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("update clasificacion " +
                                        " set `Nombre` = ? " +
                                        " where `idClasificacion`=?");

            st.setString(1, obj.getString("xNombre"));
            st.setInt(2, obj.getInt("xId"));

            st.executeUpdate();
            
            retorno.put("Cod", 0);
            retorno.put("Mensaje", "Se ha modificado la Clasificacion");
            retorno.put("ID", obj.getInt("xId"));
            
        } catch (SQLException ex) {
            retorno.put("Cod", ex.getErrorCode());
            retorno.put("Mensaje", ex.getMessage());
            retorno.put("Nivel", "Persistencia");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
    
    public static JSONObject EliminarClasificacionDB (Connection con, JSONObject obj) throws Exception{
        JSONObject retorno = new JSONObject();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = con.prepareStatement("update clasificacion set activosn=0 where idClasificacion=?");

            st.setInt(1, obj.getInt("xId"));

            st.executeUpdate();
            
            retorno.put("Cod", 0);
            retorno.put("Mensaje", "Se ha eliminado la Clasificacion");
            retorno.put("ID", obj.getInt("xId"));
            
        } catch (SQLException ex) {
            retorno.put("Cod", ex.getErrorCode());
            retorno.put("Mensaje", ex.getMessage());
            retorno.put("Nivel", "Persistencia");
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
    
    public static JSONArray ColeccionClasificaciones(JSONObject obj, String accion) throws Exception{
         Conexion objCon = new Conexion();
         Connection con = objCon.getConexion();
         JSONObject error = new JSONObject();
         JSONArray resultado = new JSONArray();
         try {
            switch(accion){
                case "Todos":
                    return ObtenerColeccionClasificacionesDB(con, obj);
                    
                default:
                    error.put("Cod", -1);
                    error.put("Mensaje", "El argumento recibido es inválido");
                    error.put("Nivel", "Dominio");
            }
            
        } catch (Exception ex) {
            con.rollback();
            Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, ex);
            if (FuncionesGenericas.IsJSONObject(ex.getMessage())){
                error = new JSONObject(ex.getMessage());
            } else {
                error.put("Cod", -1);
                error.put("Mensaje", ex.getMessage());
                error.put("Nivel", "Controlador");
            }
        } finally {
            con.close();
        }
         resultado.put(error);
        return resultado;
    }
    
    public static JSONArray ObtenerColeccionClasificacionesDB(Connection con, JSONObject obj) throws Exception{
        JSONArray retorno = new JSONArray();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            String where = "";
            if (obj == null){
                obj = new JSONObject();
            }
            
            if (obj.has("IdClasificacion")){
                where = " and idClasificacion = ?";
            }
            st = con.prepareStatement("select * from clasificacion where `ActivoSN` = 1 "+
                                        where);
            
            if (obj.has("IdClasificacion")){
                st.setInt(1, obj.getInt("IdClasificacion"));
            }
            rs = st.executeQuery();
            while (rs.next()) {
                
                JSONObject clasificacion = new JSONObject();
                clasificacion.put("Id", rs.getInt("idClasificacion"));
                clasificacion.put("Nombre", rs.getString("Nombre"));
                
                retorno.put(clasificacion);
            }
            
        } catch (SQLException ex) {
            throw new Exception("Persistencia--> " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            } 
        }
        return retorno;
    }
}
