/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Generico;

import java.io.Serializable;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.sql.DataSource;
import javax.naming.InitialContext;

/**
 *
 * @author Kawa
 */
public class Conexion implements Serializable
{
    private DataSource Arrendadora;
    private Connection con;

    public Conexion() throws Exception
    {
        try 
        {
            Context c = new InitialContext();
            //Resource
            Arrendadora = (DataSource) c.lookup("ProyectoTaller");
        }
        catch (Exception ex)
        {
            java.util.logging.Logger.getLogger(getClass().getName()).log(java.util.logging.Level.SEVERE, "exception caught", ex);
            throw new Exception("Conexion--> " + ex.getMessage() + " [" + this.getClass().getSimpleName() + "]");
        }
    }

    public Connection getConexion() throws Exception
    {
        try
        {
           if (con == null)
           {
               con = Arrendadora.getConnection();
               con.setAutoCommit(false);
               return con;
           }
           else
           {
               return con;
           }
        }
        catch(Exception ex)
        {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
            throw new Exception("Coneccion--> " + ex.getMessage() + " [" + this.getClass().getSimpleName() + "]");
        }
    }
}
