/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Generico;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author Leonardo
 */
public class FuncionesFecha {

    private static String dias[] = {"Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"};
    private static String diasMin[] = {"Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"};
    private static String mesesMin[] = {"ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SET", "OCT", "NOV", "DIC"};
    private static String meses[] = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};

    public static String[] getMeses() {
        return meses;
    }

    public static void setMeses(String[] meses) {
        FuncionesFecha.meses = meses;
    }

    public static String getFechaFormato(String xFormato, String xFecha) throws Exception {
        Calendar cal = convertirStringACalendar(xFecha);
        Date creationDate = cal.getTime();

        Locale espanol = new Locale("es", "ES");
        SimpleDateFormat date_format = new SimpleDateFormat(xFormato, espanol);
        return date_format.format(creationDate);
    }

    /**
     * Obtiene la fecha del sistema.
     *
     * @return fecha con un formato (yyyyMMdd)
     * @throws java.lang.Exception
     */
    public static String getFechaSistema() throws Exception {
        Conexion objCon = new Conexion();
        Connection con = objCon.getConexion();

        try {
            PreparedStatement st = null;

            if (con.getMetaData().getDatabaseProductName().toUpperCase().equals("ORACLE")) {
                st = con.prepareStatement("select to_char(SYSDATE,'YYYYMMDD') as FECHA from dual ");
            } else {
                st = con.prepareStatement("select CONVERT(VARCHAR,GETDATE(),112) as FECHA");
            }

            ResultSet resultado = st.executeQuery();

            if (resultado.next()) {
                return resultado.getString("FECHA");
            }
        } catch (SQLException ex) {
            throw new Exception("Persistencia--> " + ex.getMessage());
        } finally {
            con.close();
        }

        return "00000000";
    }

    /**
     * Obtiene la hora del sistema.
     *
     * @return hora con un formato (hhmmss)
     * @throws java.lang.Exception
     */
    public static String getHoraSistema() throws Exception {
        Conexion objCon = new Conexion();
        Connection con = objCon.getConexion();

        try {

            PreparedStatement st = null;
            if (con.getMetaData().getDatabaseProductName().toUpperCase().equals("ORACLE")) {
                st = con.prepareStatement("select to_char(SYSDATE,'HH24MISS') as HORA from dual ");
            } else {
                st = con.prepareStatement("select substring(convert(char ,getdate(),114),1,2)+"
                        + "substring(convert(char ,getdate(),114),4,2)+substring(convert(char ,getdate(),114),7,2) as HORA");
            }
            ResultSet resultado = st.executeQuery();

            if (resultado.next()) {
                return resultado.getString("HORA");
            }
        } catch (SQLException ex) {
            throw new Exception("Persistencia--> " + ex.getMessage());
        } finally {
            con.close();
        }

        return "000000";
    }

    /**
     * Obtiene la hora del sistema.
     *
     * @return hora con un formato (hhmm)
     * @throws java.lang.Exception
     */
    public static String getHoraSistemaHHMM() throws Exception {
        Conexion objCon = new Conexion();
        Connection con = objCon.getConexion();

        PreparedStatement st = null;
        try {

            if (con.getMetaData().getDatabaseProductName().toUpperCase().equals("ORACLE")) {
                st = con.prepareStatement("select to_char(SYSDATE,'HH24MI') as HORA from dual ");
            } else {
                st = con.prepareStatement("select substring(convert(char ,getdate(),114),1,2)+"
                        + "substring(convert(char ,getdate(),114),4,2) as HORA");
            }

            ResultSet resultado = st.executeQuery();

            if (resultado.next()) {
                return resultado.getString("HORA");
            }
        } catch (SQLException ex) {
            throw new Exception("Persistencia--> " + ex.getMessage());
        } finally {
            con.close();
        }

        return "0000";
    }

    /**
     * Obtiene la hora del sistema.
     *
     * @return hora con un formato (hhmmssMMM)
     * @throws java.lang.Exception
     */
    public static String getHoraSistemaConMilis() throws Exception {
        Conexion objCon = new Conexion();
        Connection con = objCon.getConexion();
        try {

            PreparedStatement st = null;

            if (con.getMetaData().getDatabaseProductName().toUpperCase().equals("ORACLE")) {
                st = con.prepareStatement("select to_char(systimestamp,'HH24MISSFF3') as HORA from dual  ");
            } else {
                st = con.prepareStatement("select substring(convert(char ,getdate(),114),1,2) + "
                        + "substring(convert(char ,getdate(),114),4,2)+substring(convert(char ,getdate(),114),7,2) + "
                        + "substring(convert(char ,getdate(),114),10,3) as HORA");
            }
            ResultSet resultado = st.executeQuery();

            if (resultado.next()) {
                return resultado.getString("HORA");
            }
        } catch (SQLException ex) {
            throw new Exception("Persistencia--> " + ex.getMessage());
        } finally {
            con.close();
        }
        return "000000000";
    }

    /**
     * Da un formato para mostar al usuario.
     *
     * @param xFecha
     * @return una fecha con un formato (dd/MM/yyyy)
     * @throws java.lang.Exception
     */
    public static String formatoFecha(String xFecha) throws Exception {
        try {
            if (xFecha != null) {
                xFecha = xFecha.trim();
                if ((!(xFecha.equals(""))) && xFecha.length() == 8) {
                    String a, m, d;

                    a = xFecha.substring(0, 4);
                    m = xFecha.substring(4, 6);
                    d = xFecha.substring(6, 8);

                    return d + "/" + m + "/" + a;
                } else {
                    return "00/00/0000";
                }
            } else {
                return "00/00/0000";
            }
        } catch (Exception ex) {
            throw new Exception("Genericos--> " + ex.getMessage());
        }
    }

    /**
     * Da un formato para mostar al usuario.
     *
     * @param xFecha
     * @return una fecha con un formato (dd/MM/yy)
     * @throws java.lang.Exception
     */
    public static String formatoFechaDDMMYY(String xFecha) throws Exception {
        try {
            if (xFecha != null) {
                xFecha = xFecha.trim();

                if ((!(xFecha.equals(""))) && xFecha.length() == 8) {
                    String a, m, d;

                    a = xFecha.substring(2, 4);
                    m = xFecha.substring(4, 6);
                    d = xFecha.substring(6, 8);

                    return d + "/" + m + "/" + a;
                } else {
                    return "00/00/00";
                }
            } else {
                return "00/00/00";
            }
        } catch (Exception ex) {
            throw new Exception("Genericos--> " + ex.getMessage());
        }
    }

    /**
     * Da un formato para mostar al usuario.
     *
     * @param xFecha
     * @return una fecha con un formato (MM/yyyy)
     * @throws java.lang.Exception
     */
    public static String formatoFechaMesAnio(String xFecha) throws Exception {
        try {
            if (xFecha != null) {
                xFecha = xFecha.trim();
                if ((!(xFecha.equals(""))) && xFecha.length() == 8) {
                    String a, m;

                    a = xFecha.substring(0, 4);
                    m = xFecha.substring(4, 6);

                    return m + "/" + a;
                } else {
                    if ((!(xFecha.equals(""))) && xFecha.length() == 6) {
                        String a, m;

                        a = xFecha.substring(0, 4);
                        m = xFecha.substring(4, 6);

                        return m + "/" + a;
                    } else {
                        return "00/0000";
                    }
                }
            } else {
                return "00/0000";
            }
        } catch (Exception ex) {
            throw new Exception("Genericos--> " + ex.getMessage());
        }
    }

    /**
     * Da un formato para mostar al usuario.
     *
     * @param xFecha
     * @return una fecha con un formato (MM/yy)
     * @throws java.lang.Exception
     */
    public static String formatoFechaMesAnioMMYY(String xFecha, boolean xBarra) throws Exception {
        try {
            if (xFecha != null) {
                xFecha = xFecha.trim();
                if ((!(xFecha.equals(""))) && (xFecha.length() == 6 || xFecha.length() == 8)) {
                    String a, m;

                    a = xFecha.substring(2, 4);
                    m = xFecha.substring(4, 6);

                    if (xBarra) {
                        return m + "/" + a;
                    } else {
                        return m + a;
                    }
                } else {
                    if (xBarra) {
                        return "00/00";
                    } else {
                        return "0000";
                    }
                }
            } else {
                if (xBarra) {
                    return "00/00";
                } else {
                    return "0000";
                }
            }
        } catch (Exception ex) {
            throw new Exception("Genericos--> " + ex.getMessage());
        }
    }

    /**
     * Da un formato para mostar al usuario.
     *
     * @param xFecha
     * @return una fecha con un formato (mmm/yy)
     * @throws java.lang.Exception
     */
    public static String formatoFechaNombreMesAnio(String xFecha) throws Exception {
        String a = "", m = "";
        int mm;

        try {
            if (xFecha != null) {
                xFecha = xFecha.trim();

                if ((!(xFecha.equals(""))) && (xFecha.length() == 6 || xFecha.length() == 8)) {
                    a = xFecha.substring(2, 4);
                    mm = Integer.parseInt(xFecha.substring(4, 6));
                    m = mesesMin[mm - 1];

                    return m + "/" + a;
                } else {
                    return "00/00";
                }
            } else {
                return "00/00";
            }
        } catch (Exception ex) {
            throw new Exception("Genericos--> " + ex.getMessage());
        }
    }

    /**
     * Da un formato para mostar al usuario.
     *
     * @param xFecha
     * @return una fecha con un formato (DD de Nombre Mes de YYYY)
     * @throws java.lang.Exception
     */
    public static String formatoFechaDDMesYYYY(String xFecha) throws Exception {
        String fecha = "";

        try {
            if (xFecha != null && !xFecha.trim().equals("")) {
                Calendar cal = FuncionesFecha.convertirStringACalendar(xFecha);
                fecha = cal.get(Calendar.DATE) + " de " + meses[cal.get(Calendar.MONTH)] + " de " + cal.get(Calendar.YEAR);
            }
        } catch (Exception ex) {
            throw new Exception("Genericos--> " + ex.getMessage());
        }

        return fecha;
    }

    /**
     * Normaliza la fecha.
     *
     * @param xFecha
     * @return una fecha con un formato (yyyyMMdd)
     * @throws java.lang.Exception
     */
    public static String normalizarFecha(String xFecha) throws Exception {
        try {
            if (xFecha != null) {
                xFecha = xFecha.trim();

                if ((!(xFecha.equals(""))) && xFecha.length() == 10) {
                    String a, m, d;

                    d = xFecha.substring(0, 2);
                    m = xFecha.substring(3, 5);
                    a = xFecha.substring(6, 10);

                    return a + m + d;
                } else {
                    return "00000000";
                }
            } else {
                return "00000000";
            }
        } catch (Exception ex) {
            throw new Exception("Genericos--> " + ex.getMessage());
        }
    }

    /**
     * Normaliza la fecha.
     *
     * @param xFecha
     * @return una fecha con un formato (yyyyMM)
     * @throws java.lang.Exception
     */
    public static String normalizarFechaMesAnio(String xFecha) throws Exception {
        try {
            if (xFecha != null) {
                xFecha = xFecha.trim();

                if ((!(xFecha.equals(""))) && xFecha.length() == 7) {
                    String a, m;

                    m = xFecha.substring(0, 2);
                    a = xFecha.substring(3, 7);

                    return a + m;
                } else {
                    return "000000";
                }
            } else {
                return "000000";
            }
        } catch (Exception ex) {
            throw new Exception("Genericos--> " + ex.getMessage());
        }
    }

    /**
     * Da un formato para mostar al usuario.
     *
     * @param xHora
     * @return una fecha con un formato (hh:mm)
     * @throws java.lang.Exception
     */
    public static String formatoHora(String xHora) throws Exception {
        try {
            if (xHora != null) {
                xHora = xHora.trim();

                if (!(xHora.equals("")) && xHora.length() > 3) {
                    String h, m;

                    h = xHora.substring(0, 2);
                    m = xHora.substring(2, 4);

                    return h + ":" + m;
                } else {
                    return "00:00";
                }
            } else {
                return "00:00";
            }
        } catch (Exception ex) {
            throw new Exception("Genericos--> " + ex.getMessage());
        }
    }

    /**
     * Da un formato para mostar al usuario.
     *
     * @param xHora
     * @return una fecha con un formato (hh:mm:ss)
     * @throws java.lang.Exception
     */
    public static String formatoHoraHHMMSS(String xHora) throws Exception {
        try {
            if (xHora != null) {
                xHora = xHora.trim();

                if (!(xHora.equals("")) && xHora.length() > 5) {
                    String h, m, s;

                    h = xHora.substring(0, 2);
                    m = xHora.substring(2, 4);
                    s = xHora.substring(4, 6);

                    return h + ":" + m + ":" + s;
                } else {
                    return "00:00:00";
                }
            } else {
                return "00:00:00";
            }
        } catch (Exception ex) {
            throw new Exception("Genericos--> " + ex.getMessage());
        }
    }

    /**
     * Normaliza la hora.
     *
     * @param xHora
     * @return una fecha con un formato (yyyyMMdd)
     * @throws java.lang.Exception
     */
    public static String normalizarHora(String xHora) throws Exception {
        try {
            String h, m, s;
            if (xHora != null) {
                xHora = xHora.trim();

                if ((!(xHora.equals(""))) && xHora.length() == 8) {
                    h = xHora.substring(0, 2);
                    m = xHora.substring(3, 5);
                    s = xHora.substring(6, 8);

                    return h + m + s;
                } else if ((!(xHora.equals(""))) && xHora.length() == 5) {
                    h = xHora.substring(0, 2);
                    m = xHora.substring(3, 5);

                    return h + m;
                } else {
                    return "0000";
                }
            } else {
                return "#0000#";
            }
        } catch (Exception ex) {
            throw new Exception("Genericos--> " + ex.getMessage());
        }
    }

    /**
     * Obtiene el ultimo dia del mes para esta fecha.
     *
     * @param xFecha
     * @return una fecha con el ultimo dia del mes un formato (yyyyMMdd)
     * @throws java.lang.Exception
     */
    public static String getUltimoDiaDelMes(String xFecha) throws Exception {
        try {
            String f = "";
            int a, m;

            a = Integer.parseInt(xFecha.substring(0, 4));
            m = Integer.parseInt(xFecha.substring(4, 6));
            Calendar hoy = Calendar.getInstance();

            /**
             * Se genera una fecha con el dia en 1.
             */
            hoy.set(a, m, 1);

            /**
             * Resto un día
             */
            hoy.add(Calendar.DATE, -1);

            SimpleDateFormat formato = new SimpleDateFormat("yyyyMMdd");
            f = formato.format(hoy.getTime());

            return f;
        } catch (Exception ex) {
            throw new Exception("Genericos--> " + ex.getMessage());
        }
    }

    /**
     * Calcula la cantidad de mesese que hay entre dos fechas.
     *
     * @param xFechaDesde
     * @param xFechaHasta
     * @return cantidad de meses
     * @throws java.lang.Exception
     */
    public static int getCantidadMeses(String xFechaDesde, String xFechaHasta) throws Exception {
        try {
            int mDesde, mHasta, aDesde, aHasta;
// esto es un bolazo
            mDesde = Integer.parseInt(xFechaDesde.substring(4, 6));
            mHasta = Integer.parseInt(xFechaHasta.substring(4, 6));
            aDesde = Integer.parseInt(xFechaDesde.substring(0, 4));
            aHasta = Integer.parseInt(xFechaHasta.substring(0, 4));
//            mHasta = mHasta + (aDesde - aHasta) * 12;

            int noofmonths = (aHasta - aDesde) * 12;
            noofmonths = noofmonths + ((12 - (mDesde - 1) + (mHasta - 12)));

            //return (mHasta - mDesde);
            return noofmonths;
        } catch (Exception ex) {
            throw new Exception("Genericos--> " + ex.getMessage());
        }
    }

    /**
     * Convierte una fecha de tipo String a Calendar con un formato (yyyyMMdd).
     *
     * @param xFecha
     * @return
     * @throws java.lang.Exception
     */
    public static Calendar convertirStringACalendar(String xFecha) throws Exception {
        try {
            Date date;
            DateFormat formato;
            formato = new SimpleDateFormat("yyyyMMdd");
            date = (Date) formato.parse(xFecha);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);

            return cal;
        } catch (Exception ex) {
            throw new Exception("Genericos--> " + ex.getMessage());
        }
    }

    /**
     * Convierte una hora de tipo String a Calendar con un formato (HHmm).
     *
     * @param xFecha
     * @return
     * @throws java.lang.Exception
     */
    public static Calendar convertirStringACalendarHHmm(String xHora) throws Exception {
        try {
            Date date;
            DateFormat formato;
            formato = new SimpleDateFormat("HHmm");
            date = (Date) formato.parse(xHora);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);

            return cal;
        } catch (Exception ex) {
            throw new Exception("Genericos--> " + ex.getMessage());
        }
    }

    /**
     * Convierte una fecha de tipo Calendar a String con un formato (yyyyMMdd).
     *
     * @param xFecha
     * @return
     * @throws java.lang.Exception
     */
    public static String convertirCalendarAString(Calendar xCalFecha) throws Exception {
        try {
            String strdate = null;
            DateFormat formato;
            formato = new SimpleDateFormat("yyyyMMdd");

            if (xCalFecha != null) {
                strdate = formato.format(xCalFecha.getTime());
            }

            return strdate;
        } catch (Exception ex) {
            throw new Exception("Genericos--> " + ex.getMessage());
        }
    }

    /**
     * Convierte una fecha de tipo String a Calendar con un formato (hhmm).
     *
     * @param xFecha
     * @return
     * @throws java.lang.Exception
     */
    public static Calendar convertirStringACalendarHora(String xHora) throws Exception {
        try {
            Date date;
            DateFormat formato;
            formato = new SimpleDateFormat("hhmm");
            date = (Date) formato.parse(xHora);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);

            return cal;
        } catch (Exception ex) {
            throw new Exception("Genericos--> " + ex.getMessage());
        }
    }

    public static int fechaCompareTo(String xFechaDesde, String xFechaHasta) throws Exception {
        try {
            Calendar calFechaDesde = new GregorianCalendar(
                    Integer.parseInt(xFechaDesde.substring(0, 4)),
                    Integer.parseInt(xFechaDesde.substring(4, 6)),
                    Integer.parseInt(xFechaDesde.substring(6, 8)));
            Calendar calFechaHasta = new GregorianCalendar(
                    Integer.parseInt(xFechaHasta.substring(0, 4)),
                    Integer.parseInt(xFechaHasta.substring(4, 6)),
                    Integer.parseInt(xFechaHasta.substring(6, 8)));

            return calFechaDesde.compareTo(calFechaHasta);
        } catch (Exception ex) {
            throw new Exception("herramientas --> " + ex.getMessage());
        }
    }

    /**
     * Convierte una fecha de tipo Calendar a String con un formato (hhmm).
     *
     * @param xFecha
     * @return
     * @throws java.lang.Exception
     */
    public static String convertirCalendarAStringHora(Calendar xCalHora) throws Exception {
        try {
            String strdate = null;
            DateFormat formato;
            formato = new SimpleDateFormat("hhmm");

            if (xCalHora != null) {
                strdate = formato.format(xCalHora.getTime());
            }

            return strdate;
        } catch (Exception ex) {
            throw new Exception("Genericos--> " + ex.getMessage());
        }
    }

    /**
     * Retorna la diferencia Dias, Meses, Años entre dos fechas. xTipoDiferencia
     * = S(Segundos), D(Días), M(Mes), A(Años).
     *
     * @param xFechaDesde, xFechaHasta, xTipoDiferencia
     * @return
     * @throws java.lang.Exception
     */
    public static double getDiferenciaEntreFechas(Calendar xFechaMenor, Calendar xFechaMayor, String xTipoDiferencia) throws Exception {
        double diferencia = 0;

        // Obtener la fecha en milisegundos
        double milisecond1 = xFechaMenor.getTime().getTime();
        double milisecond2 = xFechaMayor.getTime().getTime();

        // Encuentra diferencia de fechas en milisegundos
        double diffInMSec = milisecond2 - milisecond1;

        if (xTipoDiferencia.equalsIgnoreCase("S")) {
            //Diferencia en Segundos
            diferencia = diffInMSec / (1000 * 60);
        } else if (xTipoDiferencia.equalsIgnoreCase("D")) {
            //Diferencia en Días
            diferencia = diffInMSec / (1000 * 60 * 60 * 24);
        } else if (xTipoDiferencia.equalsIgnoreCase("M")) {
            //Diferencia en Meses
            diferencia = (diffInMSec / (1000 * 60 * 60 * 24)) / 30;
        } else if (xTipoDiferencia.equalsIgnoreCase("A")) {
            //Diferencia en Años
            diferencia = xFechaMayor.get(Calendar.YEAR) - xFechaMenor.get(Calendar.YEAR);

            if ((xFechaMayor.get(Calendar.MONTH) - xFechaMenor.get(Calendar.MONTH)) == 0) {
                if ((xFechaMayor.get(Calendar.DAY_OF_MONTH) - xFechaMenor.get(Calendar.DAY_OF_MONTH)) < 0) {
                    diferencia--;
                }
            } else if ((xFechaMayor.get(Calendar.MONTH) - xFechaMenor.get(Calendar.MONTH)) < 0) {
                diferencia--;
            }
        }

        return diferencia;
    }

    /**
     * Retorna la diferencia Dias, Meses, Años entre dos fechas. xTipoDiferencia
     * = S(Segundos), D(Días), M(Mes), A(Años).
     *
     * @param xFechaDesde, xFechaHasta, xTipoDiferencia
     * @return
     * @throws java.lang.Exception
     */
    public static double getDiferenciaEntreFechas(String xFechaMenor, String xFechaMayor, String xTipoDiferencia) throws Exception {
        double diferencia = 0;

        Calendar fechaMenor = convertirStringACalendar(xFechaMenor);
        Calendar fechaMayor = convertirStringACalendar(xFechaMayor);

        // Obtener la fecha en milisegundos
        double milisecond1 = fechaMenor.getTime().getTime();
        double milisecond2 = fechaMayor.getTime().getTime();

        // Encuentra diferencia de fechas en milisegundos
        double diffInMSec = milisecond2 - milisecond1;

        if (xTipoDiferencia.equalsIgnoreCase("S")) {
            //Diferencia en Segundos
            diferencia = diffInMSec / (1000 * 60);
        } else if (xTipoDiferencia.equalsIgnoreCase("D")) {
            //Diferencia en Días
            diferencia = diffInMSec / (1000 * 60 * 60 * 24);
        } else if (xTipoDiferencia.equalsIgnoreCase("M")) {
            //Diferencia en Meses
            diferencia = (diffInMSec / (1000 * 60 * 60 * 24)) / 30;
            diferencia = FuncionesGenericas.truncar(diferencia, 0);
        } else if (xTipoDiferencia.equalsIgnoreCase("A")) {
            //Diferencia en Años
            diferencia = fechaMayor.get(Calendar.YEAR) - fechaMenor.get(Calendar.YEAR);

            if ((fechaMayor.get(Calendar.MONTH) - fechaMenor.get(Calendar.MONTH)) == 0) {
                if ((fechaMayor.get(Calendar.DAY_OF_MONTH) - fechaMenor.get(Calendar.DAY_OF_MONTH)) < 0) {
                    diferencia--;
                }
            } else if ((fechaMayor.get(Calendar.MONTH) - fechaMenor.get(Calendar.MONTH)) < 0) {
                diferencia--;
            }
        }

        return diferencia;
    }

    /**
     * Retorna la diferencia Dias, Meses, Años entre dos fechas. xTipoDiferencia
     * = S(Segundos), D(Días), M(Mes), A(Años).
     *
     * @param xFechaDesde, xFechaHasta, xTipoDiferencia
     * @return
     * @throws java.lang.Exception
     */
    public static long getEdad(String xFechaNacimiento, String xFecha) throws Exception {
        long edad = 0;
        Calendar fechaNacimiento = convertirStringACalendar(xFechaNacimiento);
        Calendar fecha = convertirStringACalendar(xFecha);

        //Diferencia en Años
        edad = fecha.get(Calendar.YEAR) - fechaNacimiento.get(Calendar.YEAR);

        if ((fecha.get(Calendar.MONTH) - fechaNacimiento.get(Calendar.MONTH)) == 0) {
            if ((fecha.get(Calendar.DAY_OF_MONTH) - fechaNacimiento.get(Calendar.DAY_OF_MONTH)) < 0) {
                edad--;
            }
        } else if ((fecha.get(Calendar.MONTH) - fechaNacimiento.get(Calendar.MONTH)) < 0) {
            edad--;
        }

        return edad;
    }

    /**
     * Agrega o resta dias, meses o años a una fecha dependiendo del tipo
     *
     * @param xFecha
     * @param xTipo
     * @return
     * @throws java.lang.Exception
     */
    public static String incrementarFecha(String xFecha, int xCantidad, String xTipo) throws Exception {
        String f = "";
        Calendar fecha = convertirStringACalendar(xFecha);

        if (xTipo.equalsIgnoreCase("D")) {
            fecha.add(Calendar.DATE, xCantidad);
        } else if (xTipo.equalsIgnoreCase("M")) {
            fecha.add(Calendar.MONTH, xCantidad);
        } else if (xTipo.equalsIgnoreCase("A")) {
            fecha.add(Calendar.YEAR, xCantidad);
        }

        SimpleDateFormat formato = new SimpleDateFormat("yyyyMMdd");
        f = formato.format(fecha.getTime());

        return f.trim();
    }

    /**
     * Incrementar Hora
     *
     * @param xFecha
     * @param xTipo
     * @return
     * @throws java.lang.Exception
     */
    public static String incrementarHora(String xHora, String xIncremento) throws Exception {
        String f = "";
        Calendar hora = convertirStringACalendarHHmm(xHora);

        int intHH = Integer.parseInt(xIncremento.substring(0, 2));
        int intMM = Integer.parseInt(xIncremento.substring(2, 4));

        hora.add(Calendar.HOUR, intHH);
        hora.add(Calendar.MINUTE, intMM);

        SimpleDateFormat formato = new SimpleDateFormat("HHmm");
        f = formato.format(hora.getTime());

        return f.trim();
    }

    /**
     * Incrementar Hora
     *
     * @param xFecha
     * @param xTipo
     * @return
     * @throws java.lang.Exception
     */
    public static String incrementarHora(String xHora, int xHoras, int xMinutos) throws Exception {
        String f = "";
        Calendar hora = convertirStringACalendarHHmm(xHora);

        hora.add(Calendar.HOUR, xHoras);
        hora.add(Calendar.MINUTE, xMinutos);

        SimpleDateFormat formato = new SimpleDateFormat("HHmm");
        f = formato.format(hora.getTime());

        return f.trim();
    }

    /**
     * Agrega o resta dias, meses, años, horas y minutos a una fecha dependiendo
     * del tipo
     *
     * @param xFecha
     * @param xHora
     * @param xIncrementoFecha
     * @param xIncrementoHora
     * @param xIncrementoHora
     * @param xTipo [D = Dia, M = Mes, A = Año, H = Horas y Minutos]
     * @return
     * @throws java.lang.Exception
     */
    public static String incrementarFecha(String xFecha, String xHora, int xIncrementoFecha, int xIncrementoHora, int xIncrementoMinutos, String xTipo) throws Exception {
        String f, h = "";
        Calendar fecha = convertirStringACalendar(xFecha);

        int intHH = Integer.parseInt(xHora.substring(0, 2));
        int intMM = Integer.parseInt(xHora.substring(2, 4));

        fecha.add(Calendar.HOUR, intHH);
        fecha.add(Calendar.MINUTE, intMM);

        if (xTipo.equalsIgnoreCase("D")) {
            fecha.add(Calendar.DATE, xIncrementoFecha);
        } else if (xTipo.equalsIgnoreCase("M")) {
            fecha.add(Calendar.MONTH, xIncrementoFecha);
        } else if (xTipo.equalsIgnoreCase("A")) {
            fecha.add(Calendar.YEAR, xIncrementoFecha);
        } else if (xTipo.equalsIgnoreCase("H")) {
            fecha.add(Calendar.HOUR, xIncrementoHora);
            fecha.add(Calendar.MINUTE, xIncrementoMinutos);
        }

        SimpleDateFormat formatoF = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat formatoH = new SimpleDateFormat("HHmm");

        f = formatoF.format(fecha.getTime());
        h = formatoH.format(fecha.getTime());

        return f.trim() + "-" + h.trim();
    }

    /**
     * Convierte una fecha de tipo String a Date con un formato (yyyyMMdd).
     *
     * @param xFecha
     * @return
     * @throws java.lang.Exception
     */
    public static Date convertirStringADate(String xFecha) throws Exception {
        try {
            Date date;
            DateFormat formato;
            formato = new SimpleDateFormat("yyyyMMdd");
            date = (Date) formato.parse(xFecha);

            return date;
        } catch (Exception ex) {
            throw new Exception("Genericos--> " + ex.getMessage());
        }
    }

    /**
     * Obtener el nombre del dia.
     *
     * @param xFecha
     * @param xTipo [L = LARGE, S = SMALL]
     * @return
     * @throws java.lang.Exception
     */
    public static String getNombreDia(String xFecha, String xTipo) throws Exception {
        String str = "";

        try {
            Calendar cal = convertirStringACalendar(xFecha);

            if (xTipo.equalsIgnoreCase("L")) //NOMBRE LARGE
            {
                str = dias[cal.get(Calendar.DAY_OF_WEEK) - 1];
            } else if (xTipo.equalsIgnoreCase("S")) //NOMBRE SMALL
            {
                str = diasMin[cal.get(Calendar.DAY_OF_WEEK) - 1];
            }
        } catch (Exception ex) {
            throw new Exception("Genericos--> " + ex.getMessage());
        }

        return str;
    }

    /**
     * Da un formato para mostar al usuario.
     *
     * @param xFecha
     * @return una fecha con un formato (ddMMyy)
     * @throws java.lang.Exception
     */
    public static String formatoFechaDDMMYYSinBarras(String xFecha) throws Exception {
        try {
            if (xFecha != null) {
                xFecha = xFecha.trim();

                if ((!(xFecha.equals(""))) && xFecha.length() == 8) {
                    String a, m, d;

                    a = xFecha.substring(2, 4);
                    m = xFecha.substring(4, 6);
                    d = xFecha.substring(6, 8);

                    return d + m + a;
                } else {
                    return "000000";
                }
            } else {
                return "000000";
            }
        } catch (Exception ex) {
            throw new Exception("Genericos--> " + ex.getMessage());
        }
    }

    /**
     * Agrega o resta dias, meses o años a la fecha de Hoy.
     *
     * @param xCantidad
     * @param xTipo
     * @return String
     * @throws java.lang.Exception
     */
    public static String decrementarFecha(int xCantidad, String xTipo) throws Exception {
        String f = "";
        String xFecha = FuncionesFecha.getFechaSistema();
        Calendar fecha = convertirStringACalendar(xFecha);

        if (xTipo.equalsIgnoreCase("D")) {
            fecha.add(Calendar.DATE, xCantidad);
        } else if (xTipo.equalsIgnoreCase("M")) {
            fecha.add(Calendar.MONTH, xCantidad);
        } else if (xTipo.equalsIgnoreCase("A")) {
            fecha.add(Calendar.YEAR, xCantidad);
        }

        SimpleDateFormat formato = new SimpleDateFormat("yyyyMMdd");
        f = formato.format(fecha.getTime());

        return f.trim();
    }

    public static XMLGregorianCalendar getFechaHoraGregorianCalendar(String xFecha, String xHora) throws Exception {

        try {
            GregorianCalendar gcal = (GregorianCalendar) GregorianCalendar.getInstance();
            XMLGregorianCalendar xgcal = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcal);

            int h = Integer.parseInt(xHora.substring(0, 2));
            int m = Integer.parseInt(xHora.substring(2, 4));
            int s = Integer.parseInt(xHora.substring(4, 6));

            int a = Integer.parseInt(xFecha.substring(0, 4));
            int mf = Integer.parseInt(xFecha.substring(4, 6));
            int d = Integer.parseInt(xFecha.substring(6, 8));

            xgcal.setDay(d);
            xgcal.setMonth(mf);
            xgcal.setYear(a);

            xgcal.setHour(h);
            xgcal.setMinute(m);
            xgcal.setSecond(s);

            return xgcal;
        } catch (Exception ex) {
            throw new Exception("Genericos--> " + ex.getMessage());
        }
    }

}
