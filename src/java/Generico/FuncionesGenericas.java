/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Generico;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Kawa
 */
public class FuncionesGenericas {
    
        /**
     * Truncar un número a n Decimales.
     *
     * @param xNumero
     * @param xDecimales
     * @return
     */
    public static double truncar(double xNumero, int xDecimales) throws Exception {
        double num = 0;

        try {
            String cero = "";
            String numero = String.valueOf(xNumero);
            for (int i = 0; i < xDecimales; i++) {
                cero += "0";
            }

            if (numero.indexOf(".") > 0) {
                numero += cero;
                if (xDecimales == 0) {
                    numero = numero.substring(0, numero.indexOf("."));
                } else {
                    numero = numero.substring(0, numero.indexOf(".") + xDecimales + 1);
                }
            }

            num = Double.parseDouble(numero);
        } catch (Exception ex) {
            throw new Exception("Genericos--> " + ex.getMessage());
        }

        return num;
    }

    public static int calculoDigitoVerificador(String xCi) throws Exception {
        try {
            xCi = xCi.trim();

            int digitoVerificador = 0;
            int[] col = {2, 9, 8, 7, 6, 3, 4};

            if (xCi.trim().length() == 6 || xCi.trim().length() == 7) {
                if (xCi.trim().length() == 6) {
                    xCi = "0" + xCi;
                }
                for (int i = 0; i < col.length; i++) {
                    try {
                        digitoVerificador += col[i] * Integer.parseInt(String.valueOf(xCi.charAt(i)));
                    } catch (NumberFormatException nfe) {
                        throw new Exception("#alerta: Atenci&oacute;n, la cedula tiene que contener solo numeros." + xCi);
                    }
                }

                digitoVerificador = 10 - digitoVerificador % 10;

                if (digitoVerificador == 10) {
                    digitoVerificador = 0;
                }
            } else {
                return -1;
            }

            return digitoVerificador;
        } catch (Exception ex) {
            throw new Exception("Genericos--> " + ex.getMessage());
        }
    }

    public static String formatoCi(String xCi) throws Exception {
        String ciMasDIgito = "";
        String ciIntermedio = "";
        String ci = "";

        int digito = -1;

        try {
            digito = calculoDigitoVerificador(xCi);

            ci = xCi.trim();

            if (ci.length() == 7) {
                ciIntermedio = ci.substring(0, 1) + "." + ci.substring(1, 4) + "." + ci.substring(4, 7);
            } else {
                ciIntermedio = ci.substring(0, 3) + "." + ci.substring(3, 6);
            }

            ciMasDIgito = ciIntermedio + "-" + digito;

            return ciMasDIgito;
        } catch (Exception ex) {
            throw new Exception("Genericos--> " + ex.getMessage());
        }
    }

    /**
     * Redondear un número a n Decimales.
     *
     * @param xNumero
     * @param xDecimales
     * @return
     */
    public static double redondear(double xNumero, int xDecimales) throws Exception {
        try {
            return Math.round(xNumero * Math.pow(10, xDecimales)) / Math.pow(10, xDecimales);
        } catch (Exception ex) {
            throw new Exception("Genericos--> " + ex.getMessage());
        }
    }

    public static boolean esNumerico(String xNum) throws Exception {
        try {
            Long.parseLong(xNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    /**
     * Copia un solo archivo
     */
    public static void copiarArchivo(String xRutaOrigen, String xRutaDestino) throws IOException {
        File archivoOrigen = new File(xRutaOrigen);
        File archivoDestino = new File(xRutaDestino);

        InputStream in = new FileInputStream(archivoOrigen);
        OutputStream out = new FileOutputStream(archivoDestino);

        byte[] buf = new byte[1024];
        int len;

        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }

        in.close();
        out.close();
    }


    public static boolean validarCI(String ci) throws Exception {
        try {
            String codigo;
            String digito;
            int digitoVerificador;
            ci = ci.replace("-", "");

            codigo = ci.substring(0, ci.length() - 1);
            digito = ci.substring(ci.length() - 1, ci.length());
            digitoVerificador = calculoDigitoVerificador(codigo);

            if (digitoVerificador == Integer.parseInt(digito)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            throw new Exception("Cedula incorrecta");
        }
    }

    public static boolean validarRuc(String rut) {
        int digito;
        int digitoVerificador = 0;
        int[] col = {4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2};

        if (rut.length() == 12) {
            digito = Integer.parseInt(rut.substring(rut.length() - 1));

            for (int i = 0; i < col.length; i++) {
                digitoVerificador += col[i] * Integer.parseInt(String.valueOf(rut.charAt(i)));
            }

            digitoVerificador = 11 - (digitoVerificador % 11);

            if (digitoVerificador == 11) {
                digitoVerificador = 0;
            } else if (digitoVerificador == 10) {
                digitoVerificador = 1;
            }

            if (digitoVerificador == digito) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    public static int calcularDigitoRuc(String numero) {
        int[] digitos = new int[numero.length()];
        int factor;
        int suma = 0;
        int modulo = 0;
        int digitoVerificador = -1;

        try {
            factor = 2;
            int total = digitos.length - 1;

            for (int i = total; i >= 0; i--) {
                digitos[i] = Integer.parseInt("" + numero.charAt(i));
                suma = suma + (digitos[i] * factor);
                factor = factor == 7 ? 2 : (factor + 1);
            }

            //calculo el modulo 11 de la suma
            modulo = suma % 11;
            digitoVerificador = 11 - modulo;

            if (digitoVerificador == 11) {
                digitoVerificador = 0;
            }

            if (digitoVerificador == 10) {
                digitoVerificador = 1;
            }

        } catch (Exception e) {
            digitoVerificador = -1;
        }

        return digitoVerificador;
    }

    public static String fGetNumUnico() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat formato = new SimpleDateFormat("yyyyMMddHHmmssS");
        String numero = formato.format(cal.getTime());

        return numero;
    }


    public static String[] leerArchivo(String nombreCompletoArchivo) {
        FileReader fr;
        ArrayList<String> listaLineasArchivo = new ArrayList<String>();
        try {
            fr = new FileReader(nombreCompletoArchivo);
            BufferedReader br = new BufferedReader(fr);
            String lineaActual = br.readLine();
            while (lineaActual != null) {
                listaLineasArchivo.add(lineaActual);
                lineaActual = br.readLine();
            }
        } catch (FileNotFoundException e) {
            System.out.println("Error al leer el archivo " + nombreCompletoArchivo);
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Error al leer el archivo " + nombreCompletoArchivo);
            e.printStackTrace();
        }
        System.out.println("Archivo leido satisfactoriamente");

        return listaLineasArchivo.toArray(new String[0]);
    }

    //Función para leer archivos excel y los convierte en un json array.
    //Esta función es utilizada en el plugin de mercadolibre (Poner dónde se utiliza la función por posibles cambios)
    //Debe recibir la ruta del archivo como un string, el número de la hoja en caso de que el achivo excel tenga varias (La primera hoja es 0)
    //Y por último recibir la fila donde comience a leerlo (La fila donde están los nombres de cada columna, por defecto es 0
//    public static JsonArray getExcelDataAsJsonObject(String ruta, int numeroHoja, int comienzo) throws IOException, InvalidFormatException {
//        File excelFile = new File (ruta);
//        JsonObject sheetsJsonObject = new JsonObject();
//        Workbook workbook = null;
//        workbook = new XSSFWorkbook(excelFile);
//        Sheet sheet = workbook.getSheetAt(numeroHoja);
//        JsonArray sheetArray = new JsonArray();
//        ArrayList<String> columnNames = new ArrayList<String>();
//        Iterator<Row> sheetIterator = sheet.iterator();
//        String strGetValue = "";
//
//        while (sheetIterator.hasNext()) {
//
//                Row currentRow = sheetIterator.next();
//                JsonObject jsonObject = new JsonObject();
//
//                if (currentRow.getRowNum() != 0) {
//                    if (currentRow.getRowNum() > comienzo) {
//                        for (int j = 0; j < columnNames.size(); j++) {
//                            if (currentRow.getCell(j) != null) {
//                                if (currentRow.getCell(j).getCellTypeEnum() == CellType.STRING) {
//                                    jsonObject.addProperty(columnNames.get(j), removeSpace(currentRow.getCell(j).getStringCellValue()));
//                                    } else if (currentRow.getCell(j).getCellTypeEnum() == CellType.NUMERIC) {
//
//                                        strGetValue = new BigDecimal(currentRow.getCell(j).getNumericCellValue()).toPlainString();
//                                        if (DateUtil.isCellDateFormatted(currentRow.getCell(j))) {
//                                            strGetValue = new DataFormatter().formatCellValue(currentRow.getCell(j));
//                                        } else {
//                                            strGetValue = new BigDecimal(currentRow.getCell(j).getNumericCellValue()).toPlainString();
//                                        }
//                                        String tempStrGetValue = removeSpace(strGetValue);
//                                        if (tempStrGetValue.length() == 0) {
//                                            strGetValue = "0";
//                                            jsonObject.addProperty(columnNames.get(j), strGetValue);
//                                        } else {
//                                            jsonObject.addProperty(columnNames.get(j), removeSpace(strGetValue));
//                                        }
//
//                                    } else if (currentRow.getCell(j).getCellTypeEnum() == CellType.BOOLEAN) {
//                                    jsonObject.addProperty(columnNames.get(j), currentRow.getCell(j).getBooleanCellValue());
//                                    } else if (currentRow.getCell(j).getCellTypeEnum() == CellType.BLANK) {
//                                    jsonObject.addProperty(columnNames.get(j), "");
//                                    }
//                                } else {
//                                    jsonObject.addProperty(columnNames.get(j), "");
//                            }
//                        }
//                        sheetArray.add(jsonObject);
//                    }
//
//                } else {
//                    // store column names
//                    currentRow = sheet.getRow(comienzo);
//                    for (int k = 0; k < currentRow.getPhysicalNumberOfCells(); k++) {
//                        columnNames.add(currentRow.getCell(k).getStringCellValue());
//                    }
//                }
//        }
//        workbook.close();
//        return sheetArray;
//    }
//
//    private static String removeSpace(String strString) {
//    if (strString != null && !strString.equals("")) {
//        return strString.trim();
//    }
//        return strString;
//    }
//
//    public static String dejarSoloNumero(String str) {
//
//        String resultado = "";
//        char[] colStr = str.toCharArray();
//
//        for (char caracter : colStr) {
//            if (Character.isDigit(caracter)) {
//                resultado += caracter;
//            }
//        }
//
//        return resultado;
//    }

    public static String strLinea(String str1, String str2, int largoLinea) {
        int espacios = largoLinea - str2.length();
        return String.format("%1$-" + espacios + "s", str1) + str2;
    }

    public static void logWS(String moduleName, String mensaje) {
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

            Date fecha = new Date();
            String currentDate = dateFormat.format(fecha);
            String currentTime = timeFormat.format(fecha);
            Long threadID = Thread.currentThread().getId();

            String fileoutput = "../logs/" + moduleName + "_" + currentDate + "_" + Long.toString(threadID) + ".log";
            String linea = currentTime + " : " + mensaje;

            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileoutput, true), "UTF-8"));

            try {
                out.write(linea);
                out.newLine();
            } finally {
                out.close();
            }
        } catch (IOException ex) {
            System.out.println("Error Write Log " + ex.getMessage());
        }
    }
    
    public static JSONObject GetApi(String urlApi){
        JSONObject retorno = new JSONObject();
            try {
            URL url = new URL(urlApi);//your url i.e fetch data from .
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            
            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                StringBuffer result;
                try (BufferedReader in = new BufferedReader(new InputStreamReader(conn.getErrorStream()))) {
                    String inputLine;
                    result = new StringBuffer();
                    while ((inputLine = in .readLine()) != null) {
                        result.append(inputLine);
                    }
                }

                if (IsJSONObject(result.toString())){
                    retorno = new JSONObject(result.toString());
                } else {
                   retorno.put("Cod", conn.getResponseCode());
                   retorno.put("Mensaje", result.toString());
                }
            } else {
                InputStreamReader in = new InputStreamReader(conn.getInputStream());
                BufferedReader br = new BufferedReader(in);
                String output;
                while ((output = br.readLine()) != null) {
                    if (IsJSONObject(output)){
                        retorno = new JSONObject(output);
                    }else {
                        retorno.put("Cod", conn.getResponseCode());
                        retorno.put("Mensaje", output);
                    } 
                }
            }
            
            conn.disconnect();

        } catch (Exception e) {
            if (IsJSONObject(e.getMessage())){
                retorno = new JSONObject(e.getMessage());
            } else {
                retorno.put("Cod", -1);
                retorno.put("Mensaje", e.getMessage());
            }
        }
            return retorno;
    }
    public static JSONObject PostApi(String urlApi, JSONObject params){
        JSONObject retorno = new JSONObject();
        try {
            URL obj = new URL(urlApi);
            HttpURLConnection postConnection = (HttpURLConnection) obj.openConnection();
            postConnection.setRequestMethod("POST");
    //        postConnection.setRequestProperty("userId", "a1bcdefgh");
            postConnection.setRequestProperty("Content-Type", "application/json");
            postConnection.setDoOutput(true);
        
            try (OutputStream os = postConnection.getOutputStream()) {
                os.write(params.toString().getBytes(StandardCharsets.UTF_8));
                os.flush();
            }
            
            if (postConnection.getResponseCode() == HttpURLConnection.HTTP_CREATED) {//success                 
                StringBuilder result;
                try (BufferedReader in = new BufferedReader(new InputStreamReader(postConnection.getInputStream()))) {
                    String inputLine;
                    result = new StringBuilder();
                    while ((inputLine = in .readLine()) != null) {
                        result.append(inputLine);
                    }
                }
                
                if (IsJSONObject(result.toString())){
                    retorno = new JSONObject(result.toString());
                } else {
                   retorno.put("Cod", postConnection.getResponseCode());
                   retorno.put("Mensaje", result.toString());
                }
            } else {
                
                StringBuffer result;
                try (BufferedReader in = new BufferedReader(new InputStreamReader(postConnection.getErrorStream(), StandardCharsets.UTF_8))) {
                    String inputLine;
                    result = new StringBuffer();
                    while ((inputLine = in .readLine()) != null) {
                        result.append(inputLine);
                    }
                }

                if (IsJSONObject(result.toString())){
                    retorno = new JSONObject(result.toString());
                } else {
                   retorno.put("Cod", postConnection.getResponseCode());
                   retorno.put("Mensaje", result.toString());
                }
                
            }
        } catch (Exception e) {
            if (IsJSONObject(e.getMessage())){
                retorno = new JSONObject(e.getMessage());
            } else {
                retorno.put("Cod", -1);
                retorno.put("Mensaje", e.getMessage());
            }
        }
        return retorno;
    }
    
    public static boolean IsJSONObject(String json){
        boolean retorno;
        try {
            new JSONObject(json);
            retorno = true;
        }catch(Exception ex){
            retorno = false;
        }
        return retorno;
    }
    
}
