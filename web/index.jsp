<%-- 
    Document   : index
    Created on : 25/06/2019, 06:27:21 PM
    Author     : Kawa
--%>

<%@page import="org.json.JSONObject"%>
<%@page import="consultasBD.TipoUsuarios"%>
<%@page import="consultasBD.Sucursal"%>
<%@page import="org.json.JSONArray"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<style>
    @import url(http://fonts.googleapis.com/css?family=Roboto:100);
    @import url(http://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.css);

body {
  background: #1a1a1a;
  color: white;
  font-family: 'Roboto';
}
.flat-form {
  background: #e74c3c;
  margin: 25px auto;
  width: 390px;
  height: 500px;
  position: relative;
  font-family: 'Roboto';
}
.tabs {
  background: #c0392b;
  height: 40px;
  margin: 0;
  padding: 0;
  list-style-type: none;
  width: 100%;
  position: relative;
  display: block;
  margin-bottom: 20px;
}
.tabs li {
  display: block;
  float: left;
  margin: 0;
  padding: 0;
}
.tabs a {
  background: #c0392b;
  display: block;
  float: left;
  text-decoration: none;
  color: white;
  font-size: 16px;
  padding: 12px 22px 12px 22px;
  /*border-right: 1px solid @tab-border;*/

}
.tabs li:last-child a {
  border-right: none;
  width: 174px;
  padding-left: 0;
  padding-right: 0;
  text-align: center;
}
.tabs a.active {
  background: #e74c3c;
  border-right: none;
  -webkit-transition: all 0.5s linear;
	-moz-transition: all 0.5s linear;
	transition: all 0.5s linear;
}
.form-action {
  padding: 0 20px;
  position: relative;
}

.form-action h1 {
  font-size: 42px;
  padding-bottom: 10px;
}
.form-action p {
  font-size: 12px;
  padding-bottom: 10px;
  line-height: 25px;
}
form {
  padding-right: 20px !important;
}
form input[type=text],
form input[type=password],
form select,
form input[type=submit] {
  font-family: 'Roboto';
}

form input[type=text],
form select,
form input[type=password] {
  width: 100%;
  height: 40px;
  margin-bottom: 10px;
  padding-left: 15px;
  background: #fff;
  border: none;
  color: black;
  outline: none;
}

.dark-box {
  background: #5e0400;
  box-shadow: 1px 3px 3px #3d0100 inset;
  height: 40px;
  width: 50px;
}
.form-action .dark-box.bottom {
  position: absolute;
  right: 0;
  bottom: -24px;
}
.tabs + .dark-box.top {
  position: absolute;
  right: 0;
  top: 0px;
}
.show {
  display: block;
}
.hide {
  display: none;
}

.button {
    border: none;
    display: block;
    background: #136899;
    height: 40px;
    width: 80px;
    color: #ffffff;
    text-align: center;
    border-radius: 5px;
    /*box-shadow: 0px 3px 1px #2075aa;*/
  	-webkit-transition: all 0.15s linear;
	  -moz-transition: all 0.15s linear;
	  transition: all 0.15s linear;
}

.button:hover {
  background: #1e75aa;
  /*box-shadow: 0 3px 1px #237bb2;*/
}

.button:active {
  background: #136899;
  /*box-shadow: 0 3px 1px #0f608c;*/
}

::-webkit-input-placeholder {
  color: #e74c3c;
}
:-moz-placeholder {
  /* Firefox 18- */
  color: #e74c3c;
}
::-moz-placeholder {
  /* Firefox 19+ */
  color: #e74c3c;
}
:-ms-input-placeholder {
  color: #e74c3c;
}
</style>
<%
    JSONArray Sucursales = Sucursal.ObtenerSucursales();
    JSONArray TiposUsuarios = TipoUsuarios.ObtenerTiposUsuarios();
%>
<!DOCTYPE html>
<html>
<head>

    <!-- Basic Page Needs
  ================================================== -->
    <meta charset="utf-8">
    <title>Flat Login</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Mobile Specific Metas
  ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
  ================================================== -->
    <link href="./assets/css/jquery.gritter.css" rel="stylesheet">
    
</head>
<body>

    <div class="container">
        <div class="flat-form">
            <ul class="tabs">
                <li>
                    <a href="#login" class="active">Login</a>
                </li>
                <li>
                    <a href="#register">Register</a>
                </li>
                <li>
                    <a href="#reset">Reset Password</a>
                </li>
            </ul>
            <div id="login" class="form-action show">
                <h1>Bienvenido a nuestro proyecto</h1>
                <p>
                    Ingrese sus datos acá para comenzar a usar nuestro sistema
                </p>
                <form>
                    <ul>
                        <li>
                            <input id="usuario" type="text" placeholder="Username" />
                        </li>
                        <li>
                            <input id="pass" type="password" placeholder="Password" />
                        </li>
                        <li>
                            <input id="IngresoUsuario" type="button" value="Login" class="button" />
                            <input id="caca" type="button" value="Algo random" class="button" />
                        </li>
                    </ul>
                </form>
            </div>
            <!--/#login.form-action-->
            <div id="register" class="form-action hide">
                <h1>Registro</h1>
                <p>
                    Registrar un nuevo usuario
                </p>
                <form>
                    <ul>
                        <li>
                            <input id="registerUser" type="text" placeholder="Username" />
                        </li>
                        <li>
                            <input id="registerPass" type="password" placeholder="Password" />
                        </li>
                        <li>
                            <select id="tipoUsuario">
                                <%
                                    for (int i = 0; i<TiposUsuarios.length(); i++){
                                        JSONObject obj = TiposUsuarios.getJSONObject(i);
                                        out.print("<option value=\""+obj.getInt("Id")+"\">"+obj.getString("Tipo")+"</option>");
                                    }
                                %>
                            </select>
                        </li>
                        <li>
                            <select id="tipoSucursal">
                                <%
                                    for (int i = 0; i<Sucursales.length(); i++){
                                        JSONObject obj = Sucursales.getJSONObject(i);
                                        out.print("<option value=\""+obj.getInt("Id")+"\">"+obj.getString("Nombre")+"-"+obj.getString("Ciudad")+"</option>");
                                    }
                                %>
                            </select>
                        </li>
                        <li>
                            <input id="nuevoUsuario" value="Sign Up" class="button" />
                        </li>
                    </ul>
                </form>
            </div>
            <!--/#register.form-action-->
            <div id="reset" class="form-action hide">
                <h1>Reinicio de Contraseña</h1>
                <p>
                    Reiniciar Contraseña
                </p>
                <form>
                    <ul>
                        <li>
                            <input type="text" placeholder="Email" />
                        </li>
                        <li>
                            <input type="text" placeholder="Birthday" />
                        </li>
                        <li>
                            <input type="submit" value="Send" class="button" />
                        </li>
                    </ul>
                </form>
            </div>
            <!--/#register.form-action-->
        </div>
    </div>
    <div id="algo"></div>
    <script src="assets/js/jquery-3.4.1.js" type="text/javascript"></script>
    <script src="assets/js/jquery.gritter.js" type="text/javascript"></script>
    <script src="assets/js/funcionesGenericas.js" type="text/javascript"></script>
</body>
</html>
<script>
        (function( $ ) {
      // constants
      var SHOW_CLASS = 'show',
          HIDE_CLASS = 'hide',
          ACTIVE_CLASS = 'active';

      $( '.tabs' ).on( 'click', 'li a', function(e){
        e.preventDefault();
        var $tab = $( this ),
             href = $tab.attr( 'href' );

         $( '.active' ).removeClass( ACTIVE_CLASS );
         $tab.addClass( ACTIVE_CLASS );

         $( '.show' )
            .removeClass( SHOW_CLASS )
            .addClass( HIDE_CLASS )
            .hide();

          $(href)
            .removeClass( HIDE_CLASS )
            .addClass( SHOW_CLASS )
            .hide()
            .fadeIn( 550 );
      });
    })( jQuery );
    
    $('#IngresoUsuario').click(() => {
        let parametros = {
            username: $('#usuario').val(),
            password: $('#pass').val()
        };
       cargaAjax('checkLogin.jsp', 'algo', parametros);
    });
    
    $('#nuevoUsuario').click(() =>{
        let parametros = {
          username: $('#registerUser').val(),  
          password: $('#registerPass').val(),  
          tipoUsuario: $('#tipoUsuario').val(),
          sucursal: $('#tipoSucursal').val()
        };
        cargaAjax('Register.jsp', 'algo', parametros);
    });
    
    $('#caca').click(() =>{
       cargaAjax('Prueba.jsp', 'algo', ''); 
    });

</script>
