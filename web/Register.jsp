<%-- 
    Document   : Register
    Created on : 15/07/2019, 12:56:13 AM
    Author     : k1fox
--%>

<%@page import="consultasBD.Usuarios"%>
<%@page import="org.json.JSONObject"%>
<%
    JSONObject usuario = new JSONObject();
    String mensajeJs = "";
    String tipoJs = "";
    try{
     usuario.put("xNombre", request.getParameter("username"));   
     usuario.put("xContraseña", request.getParameter("username"));   
     usuario.put("xTipoUsuario", request.getParameter("tipoUsuario"));
     usuario.put("xIdSucursal", request.getParameter("sucursal"));
     JSONObject retorno = Usuarios.AccionUsuario(usuario, "Alta");
     System.out.println("retorno = " + retorno);
     if (retorno.has("Nivel")){
        if (retorno.getInt("Cod") == 1062){
             mensajeJs = "El nombre de usuario ya existe";
             tipoJs = "alerta";
         }
     }else{
         if(retorno.getInt("Cod") == 0){
             mensajeJs = "Se ha agregado correctamente el usuario";
             tipoJs = "exito";
         }else {
            throw new Exception(retorno.getString("Mensaje"));  
         }
                   
     }
%>
<script> 
    showMensaje(`<%=mensajeJs%>`,"<%=tipoJs%>");
</script>
<%   
    }catch(Exception ex)
    {
%><script> showMensaje(`<%=ex.getMessage()%>`,"error");</script><%
    }
%>