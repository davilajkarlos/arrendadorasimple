/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    function cargaAjax(xRuta, xIdElemento, xParametros)
    {
        if($('#'+xIdElemento).length){
            $.post(xRuta, xParametros, () =>{
                $('#'+xIdElemento).html('<h1>Cargando</h1>');
            }).done( (data) =>{
                 $('#'+xIdElemento).html(data);
            }).fail( () =>{
               showMensaje('Error al cargar la página', 'error'); 
            });
        } else {
            showMensaje('El elemento que se ha querido cargar no existe', 'error'); 
        }
        
    };
    
    function showMensaje(xMensaje, xTipo)
    {
        var texto = '';
        var estilo = '';
        var timeShow = 0;

        //Mostrar mensajes
        if (xTipo == "exito")
        {
            estilo = 'growl-success';
            timeShow = 2800;
            texto = '<strong>Correcto!</strong> ';
        } else if (xTipo == "extra")
        {
            estilo = 'growl-primary';
            timeShow = 4000;
            texto = '<strong>Extra!</strong> ';
        } else if (xTipo == "info")
        {
            estilo = 'growl-info';
            timeShow = 4000;
            texto = '<strong>Info!</strong> ';
        } else if (xTipo == "alerta")
        {
            estilo = 'growl-warning';
            timeShow = 4100;
            texto = '<strong>Alerta!</strong> ';
        } else if (xTipo == "error")
        {
            estilo = 'growl-danger';
            timeShow = 5000;
            texto = '<strong>Error!</strong> ';
        }

        jQuery.gritter.add({
            title: texto,
            text: xMensaje,
            class_name: estilo,
            image: '/ArrendadoraAutosSimple/assets/images/mensajes/' + xTipo + '.png',
            sticky: false,
            time: timeShow
        });
    };
    
    function menu(element){
        $('li.active').removeClass('active');
        $(element.currentTarget).parents().addClass('active');
        var delimiter = '.jsp';
            if ($(element.currentTarget).attr('url').includes('./')){
                delimiter = '';
            }
        cargaAjax($(element.currentTarget).attr('url')+delimiter, 'contenedor');
              
    };
