<%-- 
    Document   : tabla-Coches
    Created on : 15/10/2019, 05:12:27 PM
    Author     : k1fox
--%>

<%@page import="consultasBD.Coches"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%
    try {
        String tbCoches = "";
         JSONObject xParametros = new JSONObject();
        if (request.getParameter("xParametros") != null){
            xParametros = new JSONObject(request.getParameter("xParametros"));
            
            JSONArray  coches = Coches.ColeccionCoches(xParametros, "Disponibles");
            for (int i = 0; i< coches.length(); i++){
                JSONObject obj =  coches.getJSONObject(i);
                tbCoches += "<tr class='selectAutos' nombre='"+obj.getString("Matricula")+"' id='"+obj.getInt("Id")+"' precio='"+obj.getString("Precio")+"' style='cursor: pointer;'>";
                tbCoches += "<td class='editable'><span>"+obj.getString("Matricula")+"</span></td>";
                tbCoches += "<td class='editableSelect'><span id='"+obj.getInt("IdMarca")+"' name='Marca'>"+obj.getString("Marca")+"</span></td>";
                tbCoches += "<td class='editableSelect'><span id='"+obj.getInt("IdModelo")+"' modelo='"+obj.getInt("IdMarca")+"' name='Modelo'>"+obj.getString("Modelo")+"</span></td>";
                tbCoches += "<td class='editableSelect'><span id='"+obj.getInt("IdClasificacion")+"' name='Clasificacion'>"+obj.getString("Clasificacion")+"</span></td>";
                tbCoches += "<td class='editable'><span>"+obj.getString("KilometrajeTotal")+"</span></td>";
                tbCoches += "<td class='editable'><span>"+obj.getString("Precio")+"</span></td>";
                tbCoches += "</tr>";
            }
        }
        
%>
<div class="row">
   <div class="col-md-12">
        <div class="card ">
            <div class="card-header">
                <h4 class="card-title"> Coches</h4>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-md-5 pr-md-1">
                            <div class="form-group">
                                <label>Fecha Reserva</label>
                                <input type="date" class="form-control" id="fechaReservaCoche">
                            </div>
                        </div>
                        <div class="col-md-5 pr-md-1">
                            <div class="form-group">
                                <label>Fecha Entrega</label>
                                <input type="date" class="form-control" id="fechaEntregaCoche">
                            </div>
                        </div>
                        <div class="col-md-5 pr-md-1">
                            <div class="form-group">
                                <label>Hora Reserva<small>(Opcional)</small></label>
                                <input type="time" class="form-control" id="horaReservaCoche">
                            </div>
                        </div>
                        <div class="col-md-5 pr-md-1">
                            <div class="form-group">
                                <label>Hora Entrega<small>(Opcional)</small></label>
                                <input type="time" class="form-control" id="horaEntregaCoche">
                            </div>
                        </div>
                        <div class="col-md-5 pr-md-1" id="selectMarcas"></div>
                        <div class="col-md-5 pr-md-1" id="selectModelos"></div>
                </div>
                <button class="btn btn-sm btn-primary "type="button" id="selectBuscar">Buscar <i class="tim-icons icon-zoom-split"></i> </button>
            </div>
            <div id="algo"></div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table tablesorter " id="tbCoches">
                        <thead class=" text-primary">
                            <tr>
                                <th>Matricula</th>
                                <th>Marca</th>
                                <th>Modelo</th>
                                <th>Clasificación</th>
                                <th>Kilometraje Total</th>
                                <th>Precio</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%=tbCoches%>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div> 
</div>

<script>
    
        $(document).ready(function() {

          $().ready(function() {
            $('#tbCoches').DataTable();
             $('#tbCoches').off('click', '.selectAutos');
                $('#tbCoches').on('click', '.selectAutos', (element)=>{
                $('#auto').val($(element.currentTarget).attr('nombre'));
                $('#auto').attr('idAuto', $(element.currentTarget).attr('id'));
                $('#fechaReserva').val($('#fechaReservaCoche').val());
                $('#fechaEntrega').val($('#fechaEntregaCoche').val());
                $('#precio').val($(element.currentTarget).attr('precio'));
                $('#popupSelectCoches').remove();
            });
            
            var xParametros = <%=xParametros%>;
            if (!jQuery.isEmptyObject(xParametros)) {
                $('#fechaReservaCoche').val(xParametros.xFechaReserva);
                $('#fechaEntregaCoche').val(xParametros.xFechaEntrega);
                
                $.post('Administracion/Modelos/Componentes/selectMarcas.jsp').done( (data) =>{
                    $('#selectMarcas').html(data);
                    $('#marca').val(xParametros.xIdMarca);
                });
                var parametros = {
                    idMarca: xParametros.xIdMarca
                };
                $.post('Administracion/Coches/Componentes/selectModelos.jsp', parametros).done( (data) =>{
                    $('#selectModelos').html(data);
                    $('#modelo').val(xParametros.xIdModelo);
                });
            } else {
                var actualDate = new Date();
                $('#fechaReservaCoche').val(actualDate.toISOString().substr(0, 10));
                actualDate.setDate(actualDate.getDate()+1);
                $('#fechaEntregaCoche').val(actualDate.toISOString().substr(0, 10));
                cargaAjax('Administracion/Modelos/Componentes/selectMarcas.jsp', 'selectMarcas');
            }
            
            $('#popupSelectCoches').off('change', '#marca');
            $('#popupSelectCoches').on('change', '#marca', () => {
                var parametros = {
                    idMarca: $('#marca').val()
                };
                cargaAjax('Administracion/Coches/Componentes/selectModelos.jsp', 'selectModelos', parametros);
            });
            
            $('#selectBuscar').click(()=>{
                var parametros = {};
                parametros.xFechaReserva = $('#fechaReservaCoche').val();
                parametros.xFechaEntrega = $('#fechaEntregaCoche').val();
                parametros.xIdMarca = $('#marca').val();
                parametros.xIdModelo = $('#modelo').val();
                cargaAjax('CompraVenta/Componentes/tabla-Coches.jsp', 'popupSelectCoches', 'xParametros='+JSON.stringify(parametros));
            });
        });
    });
   
</script>
<%
   }catch(Exception e){
    %><script>showMensaje('<%=e.getMessage()%>', 'error');</script><%
    }
%>
