<%-- 
    Document   : pantalla-Reserva
    Created on : 6/10/2019, 09:45:59 PM
    Author     : k1fox
--%>

<%@page import="consultasBD.Coches"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%
    try {
        String tbReserva = "";
//        JSONArray  sucursales = Coches.ColeccionCoches(null, "Todos");
//        for (int i = 0; i< sucursales.length(); i++){
//            JSONObject obj =  sucursales.getJSONObject(i);
//            tbReserva += "<tr>";
//            tbReserva += "<td class='editable'><span>"+obj.getString("Matricula")+"</span></td>";
//            tbReserva += "<td class='editableSelect'><span id='"+obj.getInt("IdMarca")+"' name='Marca'>"+obj.getString("Marca")+"</span></td>";
//            tbReserva += "<td class='editableSelect'><span id='"+obj.getInt("IdModelo")+"' modelo='"+obj.getInt("IdMarca")+"' name='Modelo'>"+obj.getString("Modelo")+"</span></td>";
//            tbReserva += "<td class='editable'><span>"+obj.getString("Disponibilidad")+"</span></td>";
//            tbReserva += "<td class='editableSelect'><span id='"+obj.getInt("IdClasificacion")+"' name='Clasificacion'>"+obj.getString("Clasificacion")+"</span></td>";
//            tbReserva += "<td class='editable'><span>"+obj.getString("KilometrajeTotal")+"</span></td>";
//            tbReserva += "<td class='editable'><span>"+obj.getString("Precio")+"</span></td>";
//            tbReserva += "<td>"+Acciones(obj.getInt("Id"))+"</td>";
//            tbReserva += "</tr>";
//        }
%>
<div class="row">
   <div class="col-md-12">
        <div class="card ">
            <div class="card-header">
                <h4 class="card-title"> Reserva</h4>
            </div>
            <div class="card-footer">
                <button class="btn btn-sm btn-primary" id="nuevo" ><i class="tim-icons icon-simple-add"></i> Agregar una nueva Reserva</button>
            </div>
            <div id="algo"></div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-2 pr-md-1">
                        <div class="form-group">
                            <label>Matricula</label>
                            <input type="text" class="form-control" id="matricula" placeholder="matricula" value="">
                        </div>
                    </div>
<!--                    <div class="col-md-2 pr-md-1">
                        <div class="form-group">
                            <label>Fecha de Reserva</label>
                            <input type="date" class="form-control" id="fechaReserva">
                        </div>
                    </div>
                    <div class="col-md-2 pr-md-1">
                        <div class="form-group">
                            <label>Fecha de Entrega</label>
                            <input type="date" class="form-control" id="fechaEntrega">
                        </div>
                    </div>-->
                </div>
                <div id="tablaReserva"></div>
            </div>
        </div>
    </div> 
</div>

<script>
    
    $(document).ready(function() {
        $().ready(function() {
//            $('#fechaReserva').val(new Date().toISOString().substr(0, 10));
//            $('#fechaEntrega').val(new Date().toISOString().substr(0, 10));
            cargaAjax('CompraVenta/tabla-Reserva.jsp', 'tablaReserva');
            $('#nuevo').click(() =>{
                $.post('CompraVenta/popup-NuevaReserva.jsp').done((data) =>{
                    $("<div id=\"popup\" title=\"Crear Reserva\">" + data + "</div>").dialog({
                        resizable: true,
                        height: '600',
                        width: '80%',
                        modal: true,
                        "close": function (e, ui) {
                            $(this).remove();
                        }
                    });
                });
            });
        });
    });
   
</script>
<%!
    
    private static String Acciones(long idCoches){
        String texto = "";
        texto +="<button class='btn btn-sm btn-success editar accionesGenerales' id='"+idCoches+"'><i class='tim-icons icon-pencil'></i> Editar</button>"; 
        texto +="<button class='btn btn-sm btn-danger eliminar-btn accionesGenerales' id='"+idCoches+"'><i class='tim-icons icon-trash-simple'></i> Borrar</button>"; 
        texto += "<div id=\"AccionesModificar\" style=\"display: none;\"><button class=\"btn btn-sm btn-success guardar-btn\""+ 
                   "id=\""+idCoches+"\""+
                   "title=\"Guardar Cambios\">"+
                   "<i class='tim-icons icon-check-2'></i> Guardar Cambios"+
                   "</button>"+
                   "<button class=\"btn btn-sm btn-danger cancelar-btn\""+ 
                   "id=\""+idCoches+"\""+
                   "title=\"Cancelar\">"+
                   "<i class='tim-icons icon-simple-remove'></i>"+
                   "</button>"+
                   "</div>";
        return texto;
    }

%>
<%
   }catch(Exception e){
    %><script>showMensaje('<%=e.getMessage()%>', 'error');</script><%
    }
%>
