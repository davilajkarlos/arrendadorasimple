<%-- 
    Document   : index
    Created on : 6/10/2019, 09:40:07 PM
    Author     : k1fox
--%>

<div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="title">100 Awesome Nucleo Icons</h5>
                    <p class="category">Handcrafted by our friends from <a href="https://nucleoapp.com/?ref=1712">NucleoApp</a></p>
                </div>
                <div class="card-body all-icons">
                    <div class="row">
                        <div class="font-icon-list col-lg-2 col-md-3 col-sm-4 col-xs-8 col-xs-8">
                            <a href="javascript:void(0);" class="opciones" url="./CompraVenta/pantalla-Reserva.jsp">
                                <div class="font-icon-detail">
                                    <i class="tim-icons icon-calendar-60"></i>
                                    <p>Hacer una reservación</p>
                                </div>
                            </a>
                        </div>
                        <div class="font-icon-list col-lg-2 col-md-3 col-sm-4 col-xs-8 col-xs-8">
                            <a href="javascript:void(0);" class="opciones" url="./CompraVenta/pantalla-Reserva.jsp">
                                <div class="font-icon-detail">
                                    <i class="tim-icons icon-cart"></i>
                                    <p>Hacer una compra</p>
                                </div>
                            </a>
                        </div>
                    </div>
                        
                </div>
            </div>
        </div>
</div>
<div id="accion"></div>
<script>
    $(document).ready(function() {

          $().ready(function() {
            $('.opciones').click(element =>{
               var url = $(element.currentTarget).attr('url'); 
               if (url !== "" && url !== null){
                   cargaAjax(url, 'accion','');
               } else {
                   showMensaje('Proximamente en Arrendadora', 'alerta');
               }
               
            });
      });
    });
</script>