<%-- 
    Document   : popup-NuevaReserva
    Created on : 6/10/2019, 09:45:02 PM
    Author     : k1fox
--%>

<%@page import="org.json.JSONObject"%>
<%
    try {
        int idCoche = -1;
        JSONObject objCoche = new JSONObject();
        JSONObject xParametros = new JSONObject();
//        if (request.getParameter("idCoche") != null) {
//            xParametros.put("IdAutos", Integer.parseInt(request.getParameter("idCoche")));
//            objCoche = Coches.ColeccionCoches(xParametros, "Todos").getJSONObject(0);
//        }
%>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Ingresar una nueva Reserva</h5>
            </div>
            <div class="card-body">
                <form>
                    <div class="row">
                        <div class="col-md-5 pr-md-1">
                            <label>Coche</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" placeholder="Auto" id="auto" readonly>
                                <div class="input-group-append">
                                    <button class="btn btn-sm" type="button" id="selectCoche"><i class="tim-icons icon-zoom-split"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 pr-md-1">
                            <label>Cliente</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" id="cliente" readonly>
                                <div class="input-group-append">
                                    <button class="btn btn-sm" type="button" id="selectCliente"><i class="tim-icons icon-zoom-split"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 pr-md-1">
                            <div class="form-group">
                                <label>Fecha Reserva</label>
                                <input type="date" class="form-control" id="fechaReserva">
                            </div>
                        </div>
                        <div class="col-md-5 pr-md-1">
                            <div class="form-group">
                                <label>Fecha Entrega</label>
                                <input type="date" class="form-control" id="fechaEntrega">
                            </div>
                        </div>
                        <div class="col-md-5 pr-md-1">
                            <div class="form-group">
                                <label>Hora Reserva<small>(Opcional)</small></label>
                                <input type="time" class="form-control" id="horaReserva">
                            </div>
                        </div>
                        <div class="col-md-5 pr-md-1">
                            <div class="form-group">
                                <label>Hora Entrega<small>(Opcional)</small></label>
                                <input type="time" class="form-control" id="horaEntrega">
                            </div>
                        </div>
                        <div class="col-md-5 pr-md-1">
                            <div class="form-group">
                                <label>Precio</label>
                                <input type="number" class="form-control" id="precio" placeholder="Precio" value="">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="card-footer">
                <button class="btn btn-fill btn-primary" id="guardar">Guardar</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {

        $().ready(function () {
//            console.log(coche);
//            
//            if (!jQuery.isEmptyObject(coche)) {
//                $.post('Administracion/Coches/Componentes/selectClasificacion.jsp').done( (html) =>{
//                    $('#selectClasificacion').html(html);
//                    $('#clasificacion').val(coche.IdClasificacion);
//                });
//                $.post('Administracion/Modelos/Componentes/selectMarcas.jsp').done( (data) =>{
//                    $('#selectMarcas').html(data);
//                    $('#marca').val(coche.IdMarca);
//                });
//                var parametros = {
//                    idMarca: coche.IdMarca
//                };
//                $.post('Administracion/Coches/Componentes/selectModelos.jsp', parametros).done( (data) =>{
//                    $('#selectModelos').html(data);
//                    $('#modelo').val(coche.IdModelo);
//                });
////                $('#clasificacion').val(coche.IdClasificacion);
////                $('#marca').val(coche.IdClasificacion);
//                
////                cargaAjax('Administracion/Coches/Componentes/selectModelos.jsp', 'selectModelos', parametros);
//                $('#deposito').attr('idDeposito', 2);
//                $('#deposito').val(coche.NombreDeposito);
//                $('#matricula').val(coche.Matricula);
//                $('#color').val(coche.Color);
//                $('#disponibilidad').val(coche.Disponibilidad);
//                $('#precio').val(coche.Precio);
//                $('#asientos').val(coche.Asientos);
//                $('#maleta').val(coche.Maleta);
//                $('#kilometraje').val(coche.KilometrajeTotal);
//                $('#guardar').html("Modificar");
//                $('#guardar').attr("IdAuto", coche.Id);
//            }else {
//            }

            $('#guardar').click(() => {
                if ($('#matricula').val() === '') {
                    showMensaje('La matricula no puede ser vac�a', 'alerta');
                } else if ($('#color').val() === '') {
                    showMensaje('El color no puede ser vac�o', 'alerta');
                } else if ($('#asientos').val() === '') {
                    showMensaje('El asiento no puede ser vac�o', 'alerta');
                } else if ($('#maleta').val() === '') {
                    showMensaje('La maleta no puede ser vac�a', 'alerta');
                } else if ($('#kilometraje').val() === '') {
                    showMensaje('El kilometraje no puede ser vac�o', 'alerta');
                } else if ($('#precio').val() === '') {
                    showMensaje('El precio no puede ser vac�o', 'alerta');
                } else if ($('#deposito').val() === '') {
                    showMensaje('El dep�sito no puede ser vac�o', 'alerta');
                } else if (!$('#clasificacion').length || $('#clasificacion').val() === '') {
                    showMensaje('Seleccione la clasificaci�n', 'alerta');
                } else if (!$('#modelo').length || $('#modelo').val() === '') {
                    showMensaje('Seleccione el modelo', 'alerta');
                } else {
                    parametros = {};
                    parametros.xIdModelo = $('#modelo').val();
                    parametros.xMatricula = $('#matricula').val();
                    parametros.xColor = $('#color').val();
                    parametros.xDisponibilidad = $('#disponibilidad').val();
                    parametros.xPrecio = $('#precio').val();
                    parametros.xTipoClasificacion = $('#clasificacion').val();
                    parametros.xIdDeposito = $('#deposito').attr('idDeposito');
                    parametros.xAsientos = $('#asientos').val();
                    parametros.xMaleta = $('#maleta').val();
                    parametros.xKilometrajeTotal = $('#kilometraje').val();
                    if ($('#guardar').attr('IdAuto') != null){
                        parametros.Tipo = 'Modificacion';
                        parametros.xId = $('#guardar').attr('IdAuto');
                    }else {
                        parametros.Tipo = 'Alta';
                    }
                    
                    cargaAjax('Administracion/Coches/ABM-Coche.jsp', 'recarga', 'xParametros=' + JSON.stringify(parametros));
                }
            });

            $('#selectCoche').click(() => {
                $.post('CompraVenta/Componentes/tabla-Coches.jsp').done((data) => {
                    $("<div id=\"popupSelectCoches\" title=\"Seleccionar Coche\">" + data + "</div>").dialog({
                        resizable: true,
                        height: '600',
                        width: '80%',
                        modal: true,
                        "close": function (e, ui) {
                            $(this).remove();
                        }
                    });
                });
            });
            
            $('#selectCliente').click(() => {
                $.post('Administracion/Coches/Componentes/tabla-Deposito.jsp').done((data) => {
                    $("<div id=\"popupSelectDeposito\" title=\"Seleccionar Dep�sito\">" + data + "</div>").dialog({
                        resizable: true,
                        height: '600',
                        width: '80%',
                        modal: true,
                        "close": function (e, ui) {
                            $(this).remove();
                        }
                    });
                });
            });

        });
    });
</script>
<%
} catch (Exception e) {
%><script>showMensaje('<%=e.getMessage()%>', 'error');</script><%
        }
%>
