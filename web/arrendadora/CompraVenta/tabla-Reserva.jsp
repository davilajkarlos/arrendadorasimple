<%-- 
    Document   : tabla-Reserva
    Created on : 6/10/2019, 09:45:16 PM
    Author     : k1fox
--%>

<%@page import="consultasBD.Coches"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%
    try {
        String tbReserva = "";
//        JSONArray  sucursales = Coches.ColeccionCoches(null, "Todos");
//        for (int i = 0; i< sucursales.length(); i++){
//            JSONObject obj =  sucursales.getJSONObject(i);
//            tbReserva += "<tr>";
//            tbReserva += "<td class='editable'><span>"+obj.getString("Matricula")+"</span></td>";
//            tbReserva += "<td class='editableSelect'><span id='"+obj.getInt("IdMarca")+"' name='Marca'>"+obj.getString("Marca")+"</span></td>";
//            tbReserva += "<td class='editableSelect'><span id='"+obj.getInt("IdModelo")+"' modelo='"+obj.getInt("IdMarca")+"' name='Modelo'>"+obj.getString("Modelo")+"</span></td>";
//            tbReserva += "<td class='editable'><span>"+obj.getString("Disponibilidad")+"</span></td>";
//            tbReserva += "<td class='editableSelect'><span id='"+obj.getInt("IdClasificacion")+"' name='Clasificacion'>"+obj.getString("Clasificacion")+"</span></td>";
//            tbReserva += "<td class='editable'><span>"+obj.getString("KilometrajeTotal")+"</span></td>";
//            tbReserva += "<td class='editable'><span>"+obj.getString("Precio")+"</span></td>";
//            tbReserva += "<td>"+Acciones(obj.getInt("Id"))+"</td>";
//            tbReserva += "</tr>";
//        }
%>
<div class="table-responsive">
    <table class="table tablesorter " id="tbReserva">
        <thead class=" text-primary">
            <tr>
                <th>Coche</th>
                <th>Marca</th>
                <th>Modelo</th>
                <th>Fecha de Reserva</th>
                <th>Fecha de Entrega</th>
                <th>Cliente</th>
                <th>Precio</th>
                <th style="width: 10%">Acciones</th>
            </tr>
        </thead>
        <tbody>
            <%=tbReserva%>
        </tbody>
    </table>
</div>

<script>
    
        $(document).ready(function() {

          $().ready(function() {
            $('#tbReserva').DataTable();
            $('#tbReserva').off('click', '.editar');
            $('#tbReserva').on('click', '.editar', (element) =>{
                var parametros ={
                    idCoche: $(element.currentTarget).attr('id')
                };
                $.post('Administracion/Coches/popup-NuevoCoche.jsp', parametros).done((data) =>{
                    $("<div id=\"popup\" title=\"Modificar Coches\">" + data + "</div>").dialog({
                        resizable: true,
                        height: '600',
                        width: '80%',
                        modal: true,
                        "close": function (e, ui) {
                            $(this).remove();
                        }
                    });
                });
//                $(element.currentTarget).parents('tr').find('.accionesGenerales').hide();
//                $(element.currentTarget).parents('tr').find('#AccionesModificar').show();
//                $(element.currentTarget).parents('tr').find(".editable").each(function(index, object){
//                    var valor = $(object).find('span').hide().html();
//                    $(object).append('<input type="text" id="inputModificado-'+index+'" class="form-control" value="'+valor+'">');
//                    $(object).find('input').focus();
//                }); 
//
//                $(element.currentTarget).parents('tr').find(".editableSelect").each(function(index, object){
//                    
//                    var opcion = $(object).children()[0];
//                    var valor = $(opcion).hide().attr('id');
//                    if ($(opcion).attr('name') === 'Departamento'){
//                        var parametros ={
//                         idCoches: $(opcion).attr('pais')
//                        };
//                        $.post('Administracion/Ciudades/Componentes/selectDepartamento.jsp', parametros).done(data =>{
//                            $(object).append('<div class="selectEditable" id=selectDep>'+data+'</div>');
//                             $(object).find('#departamento').val(valor);
//                        });
//
//                    }else if ($(opcion).attr('name') === 'Pais'){
//                        $.post('Administracion/Localidades/Componentes/selectPais.jsp').done(data =>{
//                            $(object).append(data);
//                            $(object).find('#pais').val(valor);
//                        });
//                    }else if ($(opcion).attr('name') === 'Ciudad'){
//                        var parametros ={
//                         idDepartamento: $(opcion).attr('departamento')
//                        };
//                        $.post('Administracion/Cocheses/Componentes/selectCiudad.jsp', parametros).done(data =>{
//                            $(object).append('<div class="selectEditable" id=selectCiu>'+data+'</div>');
//                            $(object).find('#ciudad').val(valor);
//                        });
//                    }
//                });
                
            });

//            $('#tbReserva').off('click', '.guardar-btn');
//            $('#tbReserva').on('click', '.guardar-btn', (element) => {
//                if ($('#departamento').val() === ''){
//                    showMensaje('Debe seleccionar una localidad v�lida', 'alerta');
//                }else if ($('#pais').val()===''){
//                    showMensaje('Debe seleccionar un pa�s v�lido', 'alerta');
//                }else if ($('#ciudad').val()===''){
//                    showMensaje('Debe seleccionar una ciudad v�lido', 'alerta');
//                }else {
//                    var parametros = {};
//                    parametros.xId= $(element.currentTarget).attr('id');
//                    parametros.xNombre= $(element.currentTarget).parents('tr').find('#inputModificado-0').val();
//                    parametros.xLocalidad= $(element.currentTarget).parents('tr').find('#inputModificado-1').val();
//                    parametros.xNombreCalle= $(element.currentTarget).parents('tr').find('#inputModificado-2').val();
//                    parametros.xNumeroCalle= $(element.currentTarget).parents('tr').find('#inputModificado-3').val();
//                    parametros.xIdPais= $(element.currentTarget).parents('tr').find('#pais').val();
//                    parametros.xIdDepartamento= $(element.currentTarget).parents('tr').find('#departamento').val();
//                    parametros.xIdCiudad= $(element.currentTarget).parents('tr').find('#ciudad').val();
//                    parametros.Tipo = 'Modificacion';
//                    cargaAjax('Administracion/Cocheses/ABM-Coches.jsp', 'recarga', 'xParametros='+JSON.stringify(parametros)); 
//                }
//                
//            });
            
            $('#tbReserva').off('click', '.eliminar-btn');
            $('#tbReserva').on('click', '.eliminar-btn', (element)=>{
               swal.fire({
                    title: 'Borrar!',
                    html: "<span>�Seguro quiere eliminar esta Ciudad?</span>",
                    type: 'warning',
                    background:'#212529',
                    showCancelButton: true,
                    confirmButtonText: 'Si, Borrar!',
                    cancelButtonText: 'No, cancelar',
                    confirmButtonColor: '#3498db',
                    cancelButtonColor: '#e74c3c',
                    customClass: {
                        container: 'card',
                        popup: 'card-header',
                        header: 'card-header',
                        title: 'card-title',
                        content: 'card-body',
                        footer: 'card-footer'
                      }
                }).then((result) => {
                    if (result.value){
                        var parametros = {};
                        parametros.xId= $(element.currentTarget).attr('id');
                        parametros.Tipo = 'Baja';
                        cargaAjax('Administracion/Cocheses/ABM-Coches.jsp', 'recarga', 'xParametros='+JSON.stringify(parametros));
                    }
                    
                });

            });
        });
    });
   
</script>
<%!
    
    private static String Acciones(long idCoches){
        String texto = "";
        texto +="<button class='btn btn-sm btn-success editar accionesGenerales' id='"+idCoches+"'><i class='tim-icons icon-pencil'></i> Editar</button>"; 
        texto +="<button class='btn btn-sm btn-danger eliminar-btn accionesGenerales' id='"+idCoches+"'><i class='tim-icons icon-trash-simple'></i> Borrar</button>"; 
        texto += "<div id=\"AccionesModificar\" style=\"display: none;\"><button class=\"btn btn-sm btn-success guardar-btn\""+ 
                   "id=\""+idCoches+"\""+
                   "title=\"Guardar Cambios\">"+
                   "<i class='tim-icons icon-check-2'></i> Guardar Cambios"+
                   "</button>"+
                   "<button class=\"btn btn-sm btn-danger cancelar-btn\""+ 
                   "id=\""+idCoches+"\""+
                   "title=\"Cancelar\">"+
                   "<i class='tim-icons icon-simple-remove'></i>"+
                   "</button>"+
                   "</div>";
        return texto;
    }

%>
<%
   }catch(Exception e){
    %><script>showMensaje('<%=e.getMessage()%>', 'error');</script><%
    }
%>
