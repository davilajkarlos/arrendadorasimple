<%-- 
    Document   : selectClasificacion
    Created on : 30/09/2019, 12:05:52 AM
    Author     : k1fox
--%>

<%@page import="consultasBD.Clasificaciones"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%
    try {
        String selectClasificacion = "";
            JSONArray clasificaciones = Clasificaciones.ColeccionClasificaciones(null, "Todos");

            for (int i = 0; i<clasificaciones.length(); i++){
                JSONObject obj = clasificaciones.getJSONObject(i);
                selectClasificacion += "<option value='"+obj.getInt("Id")+"'>"+obj.getString("Nombre")+"</option>";
            }

%>
    <div class="form-group">
        <label>Clasificación: </label>
        <select class="form-dataTable form-control" id="clasificacion">
            <option value=''>Seleccione una Clasificación</option>
            <%=selectClasificacion%>
        </select>
    </div>
<%
   }catch(Exception e){
    %><script>showMensaje('<%=e.getMessage()%>', 'error');</script><%
    }
%>