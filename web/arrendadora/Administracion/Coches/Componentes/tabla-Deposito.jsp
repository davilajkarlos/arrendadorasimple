<%-- 
    Document   : tabla-depositos
    Created on : 7/10/2019, 09:29:37 PM
    Author     : k1fox
--%>

<%@page import="consultasBD.Depositos"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%
    try {
        String tbDepositos = "";
        JSONArray  depositos = Depositos.ColeccionDepositoes(null, "Todos");
        for (int i = 0; i< depositos.length(); i++){
            JSONObject obj =  depositos.getJSONObject(i);
            tbDepositos += "<tr class= 'selectDeposito' nombre='"+obj.getString("Nombre")+"' id='"+obj.getInt("Id")+"' style='cursor: pointer;'>";
            tbDepositos += "<td class='editable'><span>"+obj.getString("Nombre")+"</span></td>";
            tbDepositos += "<td class='editable'><span>"+obj.getString("Localidad")+"</span></td>";
            tbDepositos += "<td class='editable'><span>"+obj.getString("NombreCalle")+"</span></td>";
            tbDepositos += "<td class='editable'><span>"+obj.getString("NumeroCalle")+"</span></td>";
            tbDepositos += "</tr>";
        }
%>
<div class="row">
   <div class="col-md-12">
        <div class="card ">
            <div class="card-header">
                <h4 class="card-title"> Dep�sitos</h4>
            </div>
            <div class="card-footer">
                <button  class="btn btn-sm btn-primary" id="nuevo" ><i class="tim-icons icon-simple-add"></i> Agregar un nuevo Dep�sito</button>
            </div>
            <div id="algo"></div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table tablesorter " id="tbDeposito">
                        <thead class=" text-primary">
                            <tr>
                                <th>Nombre</th>
                                <th>Localidad</th>
                                <th>Nombre Calle</th>
                                <th>N�mero Calle</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%=tbDepositos%>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div> 
</div>

<script>
    
        $(document).ready(function() {

          $().ready(function() {
            $('#tbDeposito').DataTable();
            $('#tbDeposito').off('click', '.selectDeposito');
            $('#tbDeposito').on('click', '.selectDeposito', (element)=>{
                $('#deposito').val($(element.currentTarget).attr('nombre'));
                $('#deposito').attr('idDeposito', $(element.currentTarget).attr('id'));
                $('#popupSelectDeposito').remove();
            });         
        });
    });
</script>
<%
   }catch(Exception e){
    %><script>showMensaje('<%=e.getMessage()%>', 'error');</script><%
    }
%>
