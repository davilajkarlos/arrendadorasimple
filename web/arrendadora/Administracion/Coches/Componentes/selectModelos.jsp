<%-- 
    Document   : selectCiudad
    Created on : 30/09/2019, 12:05:52 AM
    Author     : k1fox
--%>

<%@page import="consultasBD.ModelosCoches"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%
    try {
        JSONObject parametros = new JSONObject();
        if (request.getParameter("idMarca") != null && !request.getParameter("idMarca").equals("")){
           parametros.put("idMarca", Integer.parseInt(request.getParameter("idMarca")));
        }
        String selectCiudad = "";
        if(!parametros.isNull("idMarca")){
            JSONArray modelos = ModelosCoches.ColeccionModeloCoches(parametros, "Todos");

            for (int i = 0; i<modelos.length(); i++){
                JSONObject obj = modelos.getJSONObject(i);
                selectCiudad += "<option value='"+obj.getInt("Id")+"'>"+obj.getString("Nombre")+"</option>";
            }
            
        }
%>
    <div class="form-group">
        <label>Modelos: </label>
        <select class="form-dataTable form-control" id="modelo">
            <option value=''>Seleccione un Modelo</option>
            <%=selectCiudad%>
        </select>
    </div>
<%
   }catch(Exception e){
    %><script>showMensaje('<%=e.getMessage()%>', 'error');</script><%
    }
%>