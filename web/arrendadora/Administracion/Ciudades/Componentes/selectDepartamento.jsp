<%-- 
    Document   : selectPaises
    Created on : 19 ago 2019, 3:20:15
    Author     : k1fox
--%>

<%@page import="consultasBD.Departamentos"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%
    try {
        JSONObject parametros = new JSONObject();
        if (request.getParameter("idPais") != null && !request.getParameter("idPais").equals("")){
           parametros.put("IdPais", Integer.parseInt(request.getParameter("idPais")));
        }
        String selectDepartamento = "";
        if(!parametros.isNull("IdPais")){
            JSONArray deparamento = Departamentos.ColeccionDepartamentos(parametros, "Todos");

            for (int i = 0; i<deparamento.length(); i++){
                JSONObject obj = deparamento.getJSONObject(i);
                selectDepartamento += "<option value='"+obj.getInt("Id")+"'>"+obj.getString("Departamento")+"</option>";
            }
            
        }
%>
    <div class="form-group">
        <label>Departamento: </label>
        <select class="form-dataTable form-control" id="departamento">
            <option value=''>Seleccione un Departamento</option>
            <%=selectDepartamento%>
        </select>
    </div>
<%
   }catch(Exception e){
    %><script>showMensaje('<%=e.getMessage()%>', 'error');</script><%
    }
%>