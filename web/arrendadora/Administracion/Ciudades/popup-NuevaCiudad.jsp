<%-- 
    Document   : popup-NuevoPais
    Created on : 12/08/2019, 07:09:42 PM
    Author     : k1fox
--%>
<%@page import="consultasBD.Departamentos"%>
<%@page import="org.json.JSONObject"%>
<%@page import="consultasBD.Paises"%>
<%@page import="org.json.JSONArray"%>
<%
    try {
        String selectPais = "";
        JSONArray paises = Paises.ColeccionPaises(null, "Todos");
        for (int i = 0; i<paises.length(); i++){
            JSONObject obj = paises.getJSONObject(i);
            selectPais += "<option value='"+obj.getInt("Id")+"'>"+obj.getString("Nombre")+"</option>";
        }
        
%>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Ingresar una Ciudad</h5>
            </div>
            <div class="card-body">
                <form>
                    <div class="row">
                        <div class="col-md-5 pr-md-1">
                            <div class="form-group">
                                <label>Nombre del la ciudad </label>
                                <input type="text" class="form-control" id="nombre" placeholder="Ciudad" value="">
                            </div>
                        </div>
                        <div class="col-md-5 pr-md-1">
                            <div class="form-group">
                                <label>Pais: </label>
                                <select class="form-dataTable form-control" id="pais">
                                    <option value=''>Seleccione un Pais</option>
                                    <%=selectPais%>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-5 pr-md-1" id="selectDepartamento"></div>
                    </div>
                </form>
            </div>
            <div class="card-footer">
                <button class="btn btn-fill btn-primary" id="guardar">Guardar</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {

          $().ready(function() {
            $('body').off('change','#pais');
            $('body').on('change','#pais', () =>{
                var parametros = {
                    idPais: $('#pais').val()
                };
                cargaAjax('Administracion/Ciudades/Componentes/selectDepartamento.jsp','selectDepartamento',parametros);
            });
              
            $('#guardar').click(() =>{
                if ($('#nombre').val() === ''){
                 showMensaje('La ciudad no puede ser vac�o', 'alerta');
                } else if ($('#pais').val() === ''){
                    showMensaje('Seleccione el pa�s', 'alerta');
                }else if (!$('#departamento').length || $('#departamento').val() === ''){
                    showMensaje('Seleccione el departamento', 'alerta');
                }else{
                    parametros = {};
                    parametros.Nombre = $('#nombre').val();
                    parametros.Departamento = $('#departamento').val();
                    parametros.Tipo = 'Alta';
                    cargaAjax('Administracion/Ciudades/ABM-Ciudad.jsp', 'recarga', parametros);
                }
            });
          });
      });
</script>
<%
   }catch(Exception e){
    %><script>showMensaje('<%=e.getMessage()%>', 'error');</script><%
    }
%>