<%-- 
    Document   : index
    Created on : 12/08/2019, 06:46:11 PM
    Author     : k1fox
--%>
<%@page import="consultasBD.Ciudades"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%
    try {
        String tbCiudades = "";
        JSONArray ciudades = Ciudades.ColeccionCiudades(null, "Todos");
        for (int i = 0; i<ciudades.length(); i++){
            JSONObject obj = ciudades.getJSONObject(i);
            tbCiudades += "<tr>";
            tbCiudades += "<td class='editable'><span>"+obj.getString("Ciudad")+"</span></td>";
            tbCiudades += "<td class='editableSelect'><span id='"+obj.getInt("IdDepartamento")+"' pais='"+obj.getInt("IdPais")+"' name='Departamento'>"+obj.getString("Departamento")+"</span></td>";
            tbCiudades += "<td class='editableSelect'><span id='"+obj.getInt("IdPais")+"' name='Pais'>"+obj.getString("Pais")+"</span></td>";
            tbCiudades += "<td>"+Acciones(obj.getInt("Id"))+"</td>";
            tbCiudades += "</tr>";
        }
%>
<div class="row">
   <div class="col-md-12">
        <div class="card ">
            <div class="card-header">
                <h4 class="card-title"> Ciudades</h4>
            </div>
            <div class="card-footer">
                <button  class="btn btn-sm btn-primary" id="nuevo" ><i class="tim-icons icon-simple-add"></i> Agregar una nueva Ciudad</button>
            </div>
            <div id="algo"></div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table tablesorter " id="tbCiudad">
                        <thead class=" text-primary">
                            <tr>
                                <th>Ciudad</th>
                                <th>Localidad</th>
                                <th>Pa�s</th>
                                <th style="width: 30%">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%=tbCiudades%>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div> 
</div>

<script>
    
        $(document).ready(function() {

          $().ready(function() {
            $('#tbCiudad').DataTable();
            $('#nuevo').click(() =>{
                $.post('Administracion/Ciudades/popup-NuevaCiudad.jsp').done((data) =>{
                    $("<div id=\"popup\" title=\"Crear Ciudad\">" + data + "</div>").dialog({
                        resizable: true,
                        height: '600',
                        width: '80%',
                        modal: true,
                        "close": function (e, ui) {
                            $(this).remove();
                        }
                    });
                });
            });
            
            $('#tbCiudad').off('click', '.editar');
            $('#tbCiudad').on('click', '.editar', (element) =>{
                $(element.currentTarget).parents('tr').find('.accionesGenerales').hide();
                $(element.currentTarget).parents('tr').find('#AccionesModificar').show();
                $(element.currentTarget).parents('tr').find(".editable").each(function(index, object){
                    var valor = $(object).find('span').hide().html();
                    $(object).append('<input type="text" id="inputModificado-'+index+'" class="form-control" value="'+valor+'">');
                    $(object).find('input').focus();
                }); 

                $(element.currentTarget).parents('tr').find(".editableSelect").each(function(index, object){
                    
                    var opcion = $(object).children()[0];
                    var valor = $(opcion).hide().attr('id');
                    if ($(opcion).attr('name') === 'Departamento'){
                        var parametros ={
                         idPais: $(opcion).attr('pais')
                        };
                        $.post('Administracion/Ciudades/Componentes/selectDepartamento.jsp', parametros).done(data =>{
                            $(object).append('<div id=selectDep>'+data+'</div>');
                             $(object).find('#departamento').val(valor);
                        });

                    }else if ($(opcion).attr('name') === 'Pais'){
                        $.post('Administracion/Localidades/Componentes/selectPais.jsp').done(data =>{
                            $(object).append(data);
                            $(object).find('#pais').val(valor);
                        });
                    }
                });
                
            });
                
            $('#tbCiudad').off('change','#pais');
              $('#tbCiudad').on('change','#pais', () =>{
                  var parametros = {
                      idPais: $('#pais').val()
                  };
                  cargaAjax('Administracion/Ciudades/Componentes/selectDepartamento.jsp','selectDep',parametros);
              });    
            
            $('#tbCiudad').off('click', '.cancelar-btn');
            $('#tbCiudad').on('click', '.cancelar-btn', (element) =>{
                $(element.currentTarget).parents('tr').find('input:not(:checkbox)').remove();
                $(element.currentTarget).parents('tr').find('#filterAcciones').remove();
                $(element.currentTarget).parents('tr').find('span').show();
                $(element.currentTarget).parents('tr').find('#AccionesModificar').hide();
                $(element.currentTarget).parents('tr').find('.accionesGenerales').show();
                $(element.currentTarget).parents('tr').find('div.form-group').remove();
            });
            
            $('#tbCiudad').off('click', '.guardar-btn');
            $('#tbCiudad').on('click', '.guardar-btn', (element) => {
                if ($('#departamento').val() === ''){
                    showMensaje('Debe seleccionar una localidad v�lida', 'alerta');
                }else if ($('#pais').val()===''){
                    showMensaje('Debe seleccionar un pa�s v�lido', 'alerta');
                }else {
                    var parametros = {};
                    parametros.Id= $(element.currentTarget).attr('id');
                    parametros.Nombre= $(element.currentTarget).parents('tr').find('#inputModificado-0').val();
                    parametros.Departamento= $(element.currentTarget).parents('tr').find('#departamento').val();
                    parametros.Tipo = 'Modificacion';
                    cargaAjax('Administracion/Ciudades/ABM-Ciudad.jsp', 'recarga', parametros); 
                }
                
            });
            
            $('#tbCiudad').off('click', '.eliminar-btn');
            $('#tbCiudad').on('click', '.eliminar-btn', (element)=>{
               swal.fire({
                    title: 'Borrar!',
                    html: "<span>�Seguro quiere eliminar esta Ciudad?</span>",
                    type: 'warning',
                    background:'#212529',
                    showCancelButton: true,
                    confirmButtonText: 'Si, Borrar!',
                    cancelButtonText: 'No, cancelar',
                    confirmButtonColor: '#3498db',
                    cancelButtonColor: '#e74c3c',
                    customClass: {
                        container: 'card',
                        popup: 'card-header',
                        header: 'card-header',
                        title: 'card-title',
                        content: 'card-body',
                        footer: 'card-footer'
                      }
                }).then((result) => {
                    if (result.value){
                        var parametros = {};
                        parametros.Id= $(element.currentTarget).attr('id');
                        parametros.Tipo = 'Baja';
                        cargaAjax('Administracion/Ciudades/ABM-Ciudad.jsp', 'recarga', parametros);
                    }
                    
                });

            });
        });
    });
   
</script>
<%!
    
    private static String Acciones(long idPais){
        String texto = "";
        texto +="<button class='btn btn-sm btn-success editar accionesGenerales' id='"+idPais+"'><i class='tim-icons icon-pencil'></i> Editar</button>"; 
        texto +="<button class='btn btn-sm btn-danger eliminar-btn accionesGenerales' id='"+idPais+"'><i class='tim-icons icon-trash-simple'></i> Borrar</button>"; 
        texto += "<div id=\"AccionesModificar\" style=\"display: none;\"><button class=\"btn btn-sm btn-success guardar-btn\""+ 
                   "id=\""+idPais+"\""+
                   "title=\"Guardar Cambios\">"+
                   "<i class='tim-icons icon-check-2'></i> Guardar Cambios"+
                   "</button>"+
                   "<button class=\"btn btn-sm btn-danger cancelar-btn\""+ 
                   "id=\""+idPais+"\""+
                   "title=\"Cancelar\">"+
                   "<i class='tim-icons icon-simple-remove'></i>"+
                   "</button>"+
                   "</div>";
        return texto;
    }

%>
<%
   }catch(Exception e){
    %><script>showMensaje('<%=e.getMessage()%>', 'error');</script><%
    }
%>
