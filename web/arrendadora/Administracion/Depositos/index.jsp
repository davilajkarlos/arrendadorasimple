<%-- 
    Document   : index
    Created on : 29/09/2019, 03:06:19 AM
    Author     : k1fox
--%>

<%@page import="consultasBD.Depositos"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%
    try {
        String tbDepositos = "";
        JSONArray  depositos = Depositos.ColeccionDepositoes(null, "Todos");
        System.out.println(depositos);
        for (int i = 0; i< depositos.length(); i++){
            JSONObject obj =  depositos.getJSONObject(i);
            tbDepositos += "<tr>";
            tbDepositos += "<td class='editable'><span>"+obj.getString("Nombre")+"</span></td>";
            tbDepositos += "<td class='editable'><span>"+obj.getString("Localidad")+"</span></td>";
            tbDepositos += "<td class='editable'><span>"+obj.getString("NombreCalle")+"</span></td>";
            tbDepositos += "<td class='editable'><span>"+obj.getString("NumeroCalle")+"</span></td>";
            tbDepositos += "<td>"+Acciones(obj.getInt("Id"))+"</td>";
            tbDepositos += "</tr>";
        }
%>
<div class="row">
   <div class="col-md-12">
        <div class="card ">
            <div class="card-header">
                <h4 class="card-title"> Dep�sitos</h4>
            </div>
            <div class="card-footer">
                <button  class="btn btn-sm btn-primary" id="nuevo" ><i class="tim-icons icon-simple-add"></i> Agregar un nuevo Dep�sito</button>
            </div>
            <div id="algo"></div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table tablesorter " id="tbDeposito">
                        <thead class=" text-primary">
                            <tr>
                                <th>Nombre</th>
                                <th>Localidad</th>
                                <th>Nombre Calle</th>
                                <th>N�mero Calle</th>
                                <th style="width: 10%">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%=tbDepositos%>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div> 
</div>

<script>
    
        $(document).ready(function() {

          $().ready(function() {
            $('#tbDeposito').DataTable();
            $('#nuevo').click(() =>{
                $.post('Administracion/Depositos/popup-NuevoDeposito.jsp').done((data) =>{
                    $("<div id=\"popup\" title=\"Crear Dep�sito\">" + data + "</div>").dialog({
                        resizable: true,
                        height: '600',
                        width: '80%',
                        modal: true,
                        "close": function (e, ui) {
                            $(this).remove();
                        }
                    });
                });
            });
            
            $('#tbDeposito').off('click', '.editar');
            $('#tbDeposito').on('click', '.editar', (element) =>{
                $(element.currentTarget).parents('tr').find('.accionesGenerales').hide();
                $(element.currentTarget).parents('tr').find('#AccionesModificar').show();
                $(element.currentTarget).parents('tr').find(".editable").each(function(index, object){
                    var valor = $(object).find('span').hide().html();
                    $(object).append('<input type="text" id="inputModificado-'+index+'" class="form-control" value="'+valor+'">');
                    $(object).find('input').focus();
                }); 
                
            });
                              
            $('#tbDeposito').off('click', '.cancelar-btn');
            $('#tbDeposito').on('click', '.cancelar-btn', (element) =>{
                $(element.currentTarget).parents('tr').find('input:not(:checkbox)').remove();
                $(element.currentTarget).parents('tr').find('#filterAcciones').remove();
                $(element.currentTarget).parents('tr').find('span').show();
                $(element.currentTarget).parents('tr').find('#AccionesModificar').hide();
                $(element.currentTarget).parents('tr').find('.accionesGenerales').show();
                $(element.currentTarget).parents('tr').find('div.form-group').remove();
                $(element.currentTarget).parents('tr').find('div.selectEditable').remove();
            });
            
            $('#tbDeposito').off('click', '.guardar-btn');
            $('#tbDeposito').on('click', '.guardar-btn', (element) => {
                if ($('#inputModificado-0').val() === ''){
                   showMensaje('El nombre del dep�sito no puede ser vac�o', 'alerta');
                }else if ($('#inputModificado-1').val()===''){
                    showMensaje('La localidad no puede ser vac�a', 'alerta');
                }else if ($('#inputModificado-2').val()===''){
                    showMensaje('El nombre de la calle no puede ser vac�a', 'alerta');
                }else if ($('#inputModificado-3').val()===''){
                    showMensaje('El n�mero de la calle no puede ser vac�a', 'alerta');
                }else {
                    var parametros = {};
                    parametros.xId= $(element.currentTarget).attr('id');
                    parametros.xNombre= $(element.currentTarget).parents('tr').find('#inputModificado-0').val();
                    parametros.xLocalidad= $(element.currentTarget).parents('tr').find('#inputModificado-1').val();
                    parametros.xNombreCalle= $(element.currentTarget).parents('tr').find('#inputModificado-2').val();
                    parametros.xNumeroCalle= $(element.currentTarget).parents('tr').find('#inputModificado-3').val();
                    parametros.Tipo = 'Modificacion';
                    cargaAjax('Administracion/Depositos/ABM-Deposito.jsp', 'recarga', 'xParametros='+JSON.stringify(parametros)); 
                }
                
            });
            
            $('#tbDeposito').off('click', '.eliminar-btn');
            $('#tbDeposito').on('click', '.eliminar-btn', (element)=>{
               swal.fire({
                    title: 'Borrar!',
                    html: "<span>�Seguro quiere eliminar esta Ciudad?</span>",
                    type: 'warning',
                    background:'#212529',
                    showCancelButton: true,
                    confirmButtonText: 'Si, Borrar!',
                    cancelButtonText: 'No, cancelar',
                    confirmButtonColor: '#3498db',
                    cancelButtonColor: '#e74c3c',
                    customClass: {
                        container: 'card',
                        popup: 'card-header',
                        header: 'card-header',
                        title: 'card-title',
                        content: 'card-body',
                        footer: 'card-footer'
                      }
                }).then((result) => {
                    if (result.value){
                        var parametros = {};
                        parametros.xId= $(element.currentTarget).attr('id');
                        parametros.Tipo = 'Baja';
                        cargaAjax('Administracion/Depositos/ABM-Deposito.jsp', 'recarga', 'xParametros='+JSON.stringify(parametros));
                    }
                    
                });

            });
        });
    });
   
</script>
<%!
    
    private static String Acciones(long idPais){
        String texto = "";
        texto +="<button class='btn btn-sm btn-success editar accionesGenerales' id='"+idPais+"'><i class='tim-icons icon-pencil'></i> Editar</button>"; 
        texto +="<button class='btn btn-sm btn-danger eliminar-btn accionesGenerales' id='"+idPais+"'><i class='tim-icons icon-trash-simple'></i> Borrar</button>"; 
        texto += "<div id=\"AccionesModificar\" style=\"display: none;\"><button class=\"btn btn-sm btn-success guardar-btn\""+ 
                   "id=\""+idPais+"\""+
                   "title=\"Guardar Cambios\">"+
                   "<i class='tim-icons icon-check-2'></i> Guardar Cambios"+
                   "</button>"+
                   "<button class=\"btn btn-sm btn-danger cancelar-btn\""+ 
                   "id=\""+idPais+"\""+
                   "title=\"Cancelar\">"+
                   "<i class='tim-icons icon-simple-remove'></i>"+
                   "</button>"+
                   "</div>";
        return texto;
    }

%>
<%
   }catch(Exception e){
    %><script>showMensaje('<%=e.getMessage()%>', 'error');</script><%
    }
%>
