<%-- 
    Document   : index
    Created on : 29/09/2019, 03:06:19 AM
    Author     : k1fox
--%>

<%@page import="consultasBD.Sucursal"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%
    try {
        String tbSucursales = "";
        JSONArray  sucursales = Sucursal.ColeccionSucursales(null, "Todos");
        for (int i = 0; i< sucursales.length(); i++){
            JSONObject obj =  sucursales.getJSONObject(i);
            tbSucursales += "<tr class='selectSucursal' nombre='"+obj.getString("Nombre")+"' id='"+obj.getInt("Id")+"' style='cursor: pointer;'>";
            tbSucursales += "<td class='editable'><span>"+obj.getString("Nombre")+"</span></td>";
            tbSucursales += "<td class='editableSelect'><span id='"+obj.getInt("IdPais")+"' name='Pais'>"+obj.getString("Pais")+"</span></td>";
            tbSucursales += "<td class='editableSelect'><span id='"+obj.getInt("IdDepartamento")+"' pais='"+obj.getInt("IdPais")+"' name='Departamento'>"+obj.getString("Departamento")+"</span></td>";
            tbSucursales += "<td class='editableSelect'><span id='"+obj.getInt("IdCiudad")+"' departamento='"+obj.getInt("IdDepartamento")+"' name='Ciudad'>"+obj.getString("Ciudad")+"</span></td>";
            tbSucursales += "<td class='editable'><span>"+obj.getString("Localidad")+"</span></td>";
            tbSucursales += "<td class='editable'><span>"+obj.getString("NombreCalle")+"</span></td>";
            tbSucursales += "<td class='editable'><span>"+obj.getString("NumeroCalle")+"</span></td>";
            tbSucursales += "</tr>";
        }
%>
<div class="row">
   <div class="col-md-12">
        <div class="card ">
            <div class="card-header">
                <h4 class="card-title"> Sucursales</h4>
            </div>
            <div id="algo"></div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table tablesorter " id="tbSelectSucursal">
                        <thead class=" text-primary">
                            <tr>
                                <th>Nombre</th>
                                <th>Pa�s</th>
                                <th>Departamento</th>
                                <th>Ciudad</th>
                                <th>Lugar</th>
                                <th>Nombre Calle</th>
                                <th>N�mero Calle</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%=tbSucursales%>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div> 
</div>

<script>
    
        $(document).ready(function() {

          $().ready(function() {
            $('#tbSelectSucursal').DataTable();
            
            $('#tbSelectSucursal').off('click', '.selectSucursal');
            $('#tbSelectSucursal').on('click', '.selectSucursal', (element)=>{
                $('#sucursal').val($(element.currentTarget).attr('nombre'));
                $('#sucursal').attr('idSucursal', $(element.currentTarget).attr('id'));
                $('#popupSelectSucursal').remove();
            });
        });
    });
   
</script>
<%
   }catch(Exception e){
    %><script>showMensaje('<%=e.getMessage()%>', 'error');</script><%
    }
%>
