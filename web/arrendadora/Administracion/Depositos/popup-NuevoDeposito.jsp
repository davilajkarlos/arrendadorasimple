<%-- 
    Document   : popup-NuevoPais
    Created on : 12/08/2019, 07:09:42 PM
    Author     : k1fox
--%>
<%@page import="consultasBD.Departamentos"%>
<%@page import="org.json.JSONObject"%>
<%@page import="consultasBD.Paises"%>
<%@page import="org.json.JSONArray"%>
<%
    try {
       
%>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Ingresar un Dep�sito</h5>
            </div>
            <div class="card-body">
                <form>
                    <div class="row">
                        <div class="col-md-5 pr-md-1">
                            <div class="form-group">
                                <label>Nombre del De�sito</label>
                                <input type="text" class="form-control" id="nombre" placeholder="Nombre" value="">
                            </div>
                        </div>
                        <div class="col-md-5 pr-md-1">
                            <div class="form-group">
                                <label>Localidad </label>
                                <input type="text" class="form-control" id="localidad" placeholder="Localidad" value="">
                            </div>
                        </div>
                        <div class="col-md-5 pr-md-1">
                            <div class="form-group">
                                <label>Nombre de la calle </label>
                                <input type="text" class="form-control" id="nombreCalle" placeholder="Calle" value="">
                            </div>
                        </div>
                        <div class="col-md-5 pr-md-1">
                            <div class="form-group">
                                <label>N�mero de la calle </label>
                                <input type="text" class="form-control" id="numeroCalle" placeholder="N�mero" value="">
                            </div>
                        </div>
                        <div class="col-md-5 pr-md-1">
                            <label>Sucursal</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" placeholder="Sucursal" id="sucursal" readonly>
                                <div class="input-group-append">
                                   <button class="btn btn-sm" type="button" id="selecionarSucursal"><i class="tim-icons icon-zoom-split"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="card-footer">
                <button class="btn btn-fill btn-primary" id="guardar">Guardar</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {

          $().ready(function() {
              
              $('#selecionarSucursal').click(() =>{
                $.post('Administracion/Depositos/Componentes/tabla-Sucursales.jsp').done((data) =>{
                    $("<div id=\"popupSelectSucursal\" title=\"Seleccionar Sucursal\">" + data + "</div>").dialog({
                        resizable: true,
                        height: '600',
                        width: '80%',
                        modal: true,
                        "close": function (e, ui) {
                            $(this).remove();
                        }
                    });
                });
            });
            
            $('#guardar').click(() =>{
                if ($('#nombre').val() === ''){
                 showMensaje('El nombre del dep�sito no puede ser vac�o', 'alerta');
                } else if ($('#localidad').val() === ''){
                    showMensaje('La localidad no puede ser vac�a', 'alerta');
                }else if ($('#nombreCalle').val() === ''){
                    showMensaje('El nombre de la calle no puede ser vac�a', 'alerta');
                }else if ($('#numeroCalle').val() === ''){
                    showMensaje('El n�mero de la calle no puede ser vac�a', 'alerta');
                }else if ($('#sucursal').val() === ''){
                    showMensaje('Seleccione una sucursal', 'alerta');
                }else{
                    parametros = {};
                    parametros.xNombre = $('#nombre').val();
                    parametros.xLocalidad = $('#localidad').val();
                    parametros.xNombreCalle = $('#nombreCalle').val();
                    parametros.xNumeroCalle = $('#numeroCalle').val();
                    parametros.xIdSucursal = $('#sucursal').attr('idSucursal');
                    parametros.Tipo = 'Alta';
                    cargaAjax('Administracion/Depositos/ABM-Deposito.jsp', 'recarga', 'xParametros='+JSON.stringify(parametros));
                }
            });
          });
      });
</script>
<%
   }catch(Exception e){
    %><script>showMensaje('<%=e.getMessage()%>', 'error');</script><%
    }
%>