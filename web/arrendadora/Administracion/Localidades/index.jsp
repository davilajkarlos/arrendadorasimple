<%-- 
    Document   : index
    Created on : 12/08/2019, 06:46:11 PM
    Author     : k1fox
--%>
<%@page import="consultasBD.Departamentos"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="consultasBD.Paises"%>
<%
    try {
        String tbLocalidad = "";
        JSONArray localidades = Departamentos.ColeccionDepartamentos(null, "Todos");
        System.out.println("localidades = " + localidades);
        for (int i = 0; i<localidades.length(); i++){
            JSONObject obj = localidades.getJSONObject(i);
            tbLocalidad += "<tr>";
            tbLocalidad += "<td class='editable'><span>"+obj.getString("Departamento")+"</span></td>";
            tbLocalidad += "<td class='editableSelect'><span id='"+obj.getInt("IdPais")+"' name='Pais'>"+obj.getString("Pais")+"</span></td>";
            tbLocalidad += "<td>"+Acciones(obj.getInt("Id"))+"</td>";
            tbLocalidad += "</tr>";
        }
%>
<div class="row">
   <div class="col-md-12">
        <div class="card ">
            <div class="card-header">
                <h4 class="card-title"> Departamento</h4>
            </div>
            <div class="card-footer">
                <button  class="btn btn-sm btn-primary" id="nuevo"><i class="tim-icons icon-simple-add"></i> Agregar nueva Localidad</button>
            </div>
            <div id="algo"></div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table tablesorter " id="tbLocalidad">
                        <thead class=" text-primary">
                            <tr>
                                <th>Nombre</th>
                                <th>Pais</th>
                                <th style="width: 30%">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%=tbLocalidad%>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div> 
</div>

<script>
    
        $(document).ready(function() {

          $().ready(function() {
            $('#tbLocalidad').DataTable();
            $('#nuevo').click(() =>{

                $.post('Administracion/Localidades/popup-NuevaLocalidad.jsp').done((data) =>{
                    $("<div id=\"popup\" title=\"Crear Nueva Localidad\">" + data + "</div>").dialog({
                        resizable: true,
                        height: '600',
                        width: '80%',
                        modal: true,
                        "close": function (e, ui) {
                            $(this).remove();
                        }
                    });
                });
            });
            
            $('#tbLocalidad').off('click', '.editar');
            $('#tbLocalidad').on('click', '.editar', (element) =>{
                $(element.currentTarget).parents('tr').find('.accionesGenerales').hide();
                $(element.currentTarget).parents('tr').find('#AccionesModificar').show();
                $(element.currentTarget).parents('tr').find(".editable").each(function(index, object){
                    var valor = $(object).find('span').hide().html();
                    $(object).append('<input type="text" id="inputModificado-'+index+'" class="form-control" value="'+valor+'">');
                    $(object).find('input').focus();
                }); 
                
                $(element.currentTarget).parents('tr').find(".editableSelect").each(function(index, object){

                    var opcion = $(object).children()[0];
                    var valor = $(opcion).hide().attr('id');
                    if ($(opcion).attr('name') === 'Pais'){
                        $.post('Administracion/Localidades/Componentes/selectPais.jsp').done(data =>{
                            $(object).append(data);
                            $(object).find('#pais').val(valor);
                        });
                    }
                });
            });
            
            $('#tbLocalidad').off('click', '.cancelar-btn');
            $('#tbLocalidad').on('click', '.cancelar-btn', (element) =>{
                $(element.currentTarget).parents('tr').find('input:not(:checkbox)').remove();
                $(element.currentTarget).parents('tr').find('#filterAcciones').remove();
                $(element.currentTarget).parents('tr').find('span').show();
                $(element.currentTarget).parents('tr').find('#AccionesModificar').hide();
                $(element.currentTarget).parents('tr').find('.accionesGenerales').show();
                $(element.currentTarget).parents('tr').find('div.form-group').remove();
            });
            
            $('#tbLocalidad').off('click', '.guardar-btn');
            $('#tbLocalidad').on('click', '.guardar-btn', (element) => {
                if ($('#pais').val()===''){
                    showMensaje('Debe seleccionar un pa�s v�lido', 'alerta');
                }else {
                    var parametros = {};
                    parametros.Id= $(element.currentTarget).attr('id');
                    parametros.Nombre= $(element.currentTarget).parents('tr').find('#inputModificado-0').val();
                    parametros.Pais= $(element.currentTarget).parents('tr').find('#pais').val();
                    parametros.Tipo = 'Modificacion';
                    cargaAjax('Administracion/Localidades/ABM-Localidad.jsp', 'recarga', parametros); 
                }
                
            });
            
            $('#tbLocalidad').off('click', '.eliminar-btn');
            $('#tbLocalidad').on('click', '.eliminar-btn', (element)=>{
               swal.fire({
                    title: 'Borrar!',
                    html: "<span>�Seguro quiere eliminar esta Ciudad?</span>",
                    type: 'warning',
                    background:'#212529',
                    showCancelButton: true,
                    confirmButtonText: 'Si, Borrar!',
                    cancelButtonText: 'No, cancelar',
                    confirmButtonColor: '#3498db',
                    cancelButtonColor: '#e74c3c',
                    customClass: {
                        container: 'card',
                        popup: 'card-header',
                        header: 'card-header',
                        title: 'card-title',
                        content: 'card-body',
                        footer: 'card-footer'
                      }
                }).then((result) =>{
                    if (result.value){
                        var parametros = {};
                        parametros.Id= $(element.currentTarget).attr('id');
                        parametros.Tipo = 'Baja';
                        cargaAjax('Administracion/Localidades/ABM-Localidad.jsp', 'recarga', parametros);
                    }
                    
                });

            });
        });
    });
   
</script>
<%!
    
    private static String Acciones(long idDepartamento){
        String texto = "";
        texto +="<button class='btn btn-sm btn-success editar accionesGenerales' id='"+idDepartamento+"'><i class='tim-icons icon-pencil'></i> Editar</button>"; 
        texto +="<button class='btn btn-sm btn-danger eliminar-btn accionesGenerales' id='"+idDepartamento+"'><i class='tim-icons icon-trash-simple'></i> Borrar</button>"; 
        texto += "<div id=\"AccionesModificar\" style=\"display: none;\"><button class=\"btn btn-sm btn-success guardar-btn\""+ 
                   "id=\""+idDepartamento+"\""+
                   "title=\"Guardar Cambios\">"+
                   "<i class='tim-icons icon-check-2'></i> Guardar Cambios"+
                   "</button>"+
                   "<button class=\"btn btn-sm btn-danger cancelar-btn\""+ 
                   "id=\""+idDepartamento+"\""+
                   "title=\"Cancelar\">"+
                   "<i class='tim-icons icon-simple-remove'></i>"+
                   "</button>"+
                   "</div>";
        return texto;
    }

%>
<%
   }catch(Exception e){
    %><script>showMensaje('<%=e.getMessage()%>', 'error');</script><%
    }
%>
