<%-- 
    Document   : ABM-Departamentos
    Created on : 17 ago 2019, 16:51:34
    Author     : k1fox
--%>

<%@page import="consultasBD.Departamentos"%>
<%@page import="org.json.JSONObject"%>
<%
    JSONObject localidad = new JSONObject();
    String mensajeJs = "";
    String tipoJs = "";
    String tipo = "";
    try{
        if (request.getParameter("Nombre") != null){
            localidad.put("xNombre", request.getParameter("Nombre"));
        }
        if (request.getParameter("Pais") != null){
            localidad.put("xIdPais", request.getParameter("Pais"));
        }
        if (request.getParameter("Id") != null){
            localidad.put("xId", request.getParameter("Id"));
        }
        if (request.getParameter("Tipo") != null){
            tipo = request.getParameter("Tipo");
        }
        JSONObject retorno = Departamentos.AccionDepartamentos(localidad, tipo);
        if (retorno.has("Nivel")){
           if (retorno.getInt("Cod") == 1062){
                mensajeJs = "La localidad ya existe";
                tipoJs = "alerta";
            }else {
                mensajeJs = "Error Cod: "+retorno.getInt("Cod")+"<br/>";
                mensajeJs += "Nivel: "+retorno.getString("Nivel")+"<br/>";
                mensajeJs += "Mensaje: "+retorno.getString("Mensaje");
                tipoJs = "error";
            }
        }else{
            if(retorno.getInt("Cod") == 0){
                mensajeJs = retorno.getString("Mensaje");
                tipoJs = "exito";
            }else {
               throw new Exception(retorno.getString("Mensaje"));  
            }

        }
%>
<script> 
    showMensaje(`<%=mensajeJs%>`,'<%=tipoJs%>');
    if('<%=tipoJs%>' === 'exito'){
        $('#popup').remove();
        cargaAjax('./Administracion/Localidades/', 'tablaLocal');
    }
</script>
<%   
    }catch(Exception ex)
    {
%><script> showMensaje(`<%=ex.getMessage()%>`,"error");</script><%
    }
%>