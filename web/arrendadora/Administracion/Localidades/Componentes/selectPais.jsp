<%-- 
    Document   : selectPais
    Created on : 26/08/2019, 02:06:03 AM
    Author     : k1fox
--%>

<%@page import="consultasBD.Paises"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>
<%
    try {

        String selectPais = "";
        JSONArray paises = Paises.ColeccionPaises(null, "Todos");
        for (int i = 0; i<paises.length(); i++){
            JSONObject obj = paises.getJSONObject(i);
            selectPais += "<option value='"+obj.getInt("Id")+"'>"+obj.getString("Nombre")+"</option>";
        }
%>
    <div class="form-group">
        <label>Pais: </label>
        <select class="form-dataTable form-control" id="pais">
            <option value=''>Seleccione un pais</option>
            <%=selectPais%>
        </select>
    </div>
<%
   }catch(Exception e){
    %><script>showMensaje('<%=e.getMessage()%>', 'error');</script><%
    }
%>