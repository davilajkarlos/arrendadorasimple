<%-- 
    Document   : popup-NuevoPais
    Created on : 12/08/2019, 07:09:42 PM
    Author     : k1fox
--%>

<%@page import="consultasBD.Paises"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%
    try {
        String selectPais = "";
        JSONArray paises = Paises.ColeccionPaises(null, "Todos");
        for (int i = 0; i<paises.length(); i++){
            JSONObject obj = paises.getJSONObject(i);
            selectPais += "<option value='"+obj.getInt("Id")+"'>"+obj.getString("Nombre")+"</option>";
        }
        
%>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Ingresar una Departamento</h5>
            </div>
            <div class="card-body">
                <form>
                    <div class="row">
                        <div class="col-md-5 pr-md-1">
                            <div class="form-group">
                                <label>Nombre del Departamento </label>
                                <input type="text" class="form-control" id="nombre" placeholder="Localidad" value="">
                            </div>
                        </div>
                        <div class="col-md-5 pr-md-1">
                            <div class="form-group">
                                <label>Pais: </label>
                                <select class="form-dataTable form-control" id="pais">
                                    <option value=''>Seleccione un Pais</option>
                                    <%=selectPais%>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="card-footer">
                <button class="btn btn-fill btn-primary" id="guardar">Guardar</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {

          $().ready(function() {
              
              $('#guardar').click(() =>{
                  if ($('#nombre').val() === ''){
                    showMensaje('La localidad no puede ser vac�o', 'alerta');
                  } else if ($('#pais').val() === ''){
                    showMensaje('Seleccione un pa�s', 'alerta');
                  }else {
                    parametros = {};
                    parametros.Nombre = $('#nombre').val();
                    parametros.Pais = $('#pais').val();
                    parametros.Tipo= 'Alta';
                    cargaAjax('Administracion/Localidades/ABM-Localidad.jsp', 'recarga', parametros);
                  }
              });
          });
      });
</script>
<%
   }catch(Exception e){
    %><script>showMensaje('<%=e.getMessage()%>', 'error');</script><%
    }
%>