<%-- 
    Document   : ABM-Clasificaciones
    Created on : 17 ago 2019, 16:51:34
    Author     : k1fox
--%>

<%@page import="consultasBD.Clasificaciones"%>
<%@page import="org.json.JSONObject"%>
<%
    JSONObject clasificacion = new JSONObject();
    String mensajeJs = "";
    String tipoJs = "";
    String tipo = "";
    try{
        
        clasificacion = new JSONObject(request.getParameter("xParametros"));
        tipo = clasificacion.getString("Tipo");
     JSONObject retorno = Clasificaciones.AccionClasificacion(clasificacion, tipo);
     if (retorno.has("Nivel")){
        if (retorno.getInt("Cod") == 1062){
             mensajeJs = "La Marca ya existe";
             tipoJs = "alerta";
         }else {
             mensajeJs = "Error Cod: "+retorno.getInt("Cod")+"<br/>";
             mensajeJs += "Nivel: "+retorno.getString("Nivel")+"<br/>";
             mensajeJs += "Mensaje: "+retorno.getString("Mensaje");
             tipoJs = "error";
         }
     }else{
         if(retorno.getInt("Cod") == 0){
            mensajeJs = retorno.getString("Mensaje");
            tipoJs = "exito";
         }else {
            throw new Exception(retorno.getString("Mensaje"));  
         }
                   
     }
%>
<script> 
    if('<%=tipoJs%>' === 'exito'){
        $('#popupCrearPais').remove();
        cargaAjax('./Administracion/ClasificacionCoches/', 'tablaClasificaciones');
    }
    showMensaje(`<%=mensajeJs%>`,'<%=tipoJs%>');
</script>
<%   
    }catch(Exception ex)
    {
%><script> showMensaje(`<%=ex.getMessage()%>`,"error");</script><%
    }
%>