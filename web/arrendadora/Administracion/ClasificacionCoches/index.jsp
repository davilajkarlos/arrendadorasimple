<%-- 
    Document   : index
    Created on : 12/08/2019, 06:46:11 PM
    Author     : k1fox
--%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="consultasBD.Clasificaciones"%>
<%
    try {
        String tbClasificaciones = "";
        JSONArray clasificaciones = Clasificaciones.ColeccionClasificaciones(null, "Todos");
        for (int i = 0; i<clasificaciones.length(); i++){
            JSONObject obj = clasificaciones.getJSONObject(i);
            tbClasificaciones += "<tr>";
            tbClasificaciones += "<td class='editable'><span>"+obj.getString("Nombre")+"</span></td>";
            tbClasificaciones += "<td>"+Acciones(obj.getInt("Id"))+"</td>";
            tbClasificaciones += "</tr>";
        }
%>
<div class="row">
   <div class="col-md-12">
        <div class="card ">
            <div class="card-header">
                <h4 class="card-title"> Clasificaciones</h4>
            </div>
            <div class="card-footer">
                <button  class="btn btn-sm btn-primary" id="nuevo"><i class="tim-icons icon-simple-add"></i> Agregar nueva Clasificaci�n</button>
            </div>
            <div id="algo"></div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table tablesorter " id="tbClasificaciones">
                        <thead class=" text-primary">
                            <tr>
                                <th>Nombre</th>
                                <th style="width: 30%">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%=tbClasificaciones%>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div> 
</div>

<script>
    
        $(document).ready(function() {

          $().ready(function() {
            $('#tbClasificaciones').DataTable();
            $('#nuevo').click(() =>{

                $.post('Administracion/ClasificacionCoches/popup-NuevaClasificacion.jsp').done((data) =>{
                    $("<div id=\"popupCrearPais\" title=\"Crear Pais\">" + data + "</div>").dialog({
                        resizable: true,
                        height: '600',
                        width: '80%',
                        modal: true,
                        "close": function (e, ui) {
                            $(this).remove();
                        }
                    });
                });
            });
            
            $('#tbClasificaciones').off('click', '.editar');
            $('#tbClasificaciones').on('click', '.editar', (element) =>{
                $(element.currentTarget).parents('tr').find('.accionesGenerales').hide();
                $(element.currentTarget).parents('tr').find('#AccionesModificar').show();
                $(element.currentTarget).parents('tr').find(".editable").each(function(index, object){
                    var valor = $(object).find('span').hide().html();
                    $(object).append('<input type="text" id="inputModificado-'+index+'" class="form-control" value="'+valor+'">');
                    $(object).find('input').focus();
                }); 
            });
            
            $('#tbClasificaciones').off('click', '.cancelar-btn');
            $('#tbClasificaciones').on('click', '.cancelar-btn', (element) =>{
                $(element.currentTarget).parents('tr').find('input:not(:checkbox)').remove();
                $(element.currentTarget).parents('tr').find('#filterAcciones').remove();
                $(element.currentTarget).parents('tr').find('span').show();
                $(element.currentTarget).parents('tr').find('#AccionesModificar').hide();
                $(element.currentTarget).parents('tr').find('.accionesGenerales').show();
            });
            
            $('#tbClasificaciones').off('click', '.guardar-btn');
            $('#tbClasificaciones').on('click', '.guardar-btn', (element) => {
                var parametros = {};
                parametros.xId= $(element.currentTarget).attr('id');
                parametros.xNombre= $(element.currentTarget).parents('tr').find('#inputModificado-0').val();
                parametros.Tipo = 'Modificacion';
                cargaAjax('Administracion/ClasificacionCoches/ABM-Clasificacion.jsp', 'recarga', 'xParametros='+JSON.stringify(parametros));
            });
            
            $('#tbClasificaciones').off('click', '.eliminar-btn');
            $('#tbClasificaciones').on('click', '.eliminar-btn', (element)=>{
               swal.fire({
                    title: 'Borrar!',
                    html: "<span>�Seguro quiere eliminar este Pa�s?</span>",
                    type: 'warning',
                    background:'#212529',
                    showCancelButton: true,
                    confirmButtonText: 'Si, Borrar!',
                    cancelButtonText: 'No, cancelar',
                    confirmButtonColor: '#3498db',
                    cancelButtonColor: '#e74c3c',
                    customClass: {
                        container: 'card',
                        popup: 'card-header',
                        header: 'card-header',
                        title: 'card-title',
                        content: 'card-body',
                        footer: 'card-footer'
                      }
                }).then((result) => {
                    if (result.value){
                        var parametros = {};
                        parametros.xId= $(element.currentTarget).attr('id');
                        parametros.Tipo = 'Baja';
                        cargaAjax('Administracion/ClasificacionCoches/ABM-Clasificacion.jsp', 'recarga', 'xParametros='+JSON.stringify(parametros));
                    }
                });

            });
        });
    });
   
</script>
<%!
    
    private static String Acciones(long idClasificacion){
        String texto = "";
        texto +="<button class='btn btn-sm btn-success editar accionesGenerales' id='"+idClasificacion+"'><i class='tim-icons icon-pencil'></i> Editar</button>"; 
        texto +="<button class='btn btn-sm btn-danger eliminar-btn accionesGenerales' id='"+idClasificacion+"'><i class='tim-icons icon-trash-simple'></i> Borrar</button>"; 
        texto += "<div id=\"AccionesModificar\" style=\"display: none;\"><button class=\"btn btn-sm btn-success guardar-btn\""+ 
                   "id=\""+idClasificacion+"\""+
                   "title=\"Guardar Cambios\">"+
                   "<i class='tim-icons icon-check-2'></i> Guardar Cambios"+
                   "</button>"+
                   "<button class=\"btn btn-sm btn-danger cancelar-btn\""+ 
                   "id=\""+idClasificacion+"\""+
                   "title=\"Cancelar\">"+
                   "<i class='tim-icons icon-simple-remove'></i>"+
                   "</button>"+
                   "</div>";
        return texto;
    }

%>
<%
   }catch(Exception e){
    %><script>showMensaje('<%=e.getMessage()%>', 'error');</script><%
    }
%>
