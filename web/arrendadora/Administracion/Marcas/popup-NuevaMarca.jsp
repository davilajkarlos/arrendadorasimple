<%-- 
    Document   : popup-NuevoMarca
    Created on : 12/08/2019, 07:09:42 PM
    Author     : k1fox
--%>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Ingresar una Marca</h5>
            </div>
            <div class="card-body">
                <form>
                    <div class="row">
                        <div class="col-md-5 pr-md-1">
                            <div class="form-group">
                                <label>Nombre del marca </label>
                                <input type="text" class="form-control" id="nombre" placeholder="Marca" value="">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="card-footer">
                <button class="btn btn-fill btn-primary" id="guardar">Guardar</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {

          $().ready(function() {
              $('#guardar').click(() =>{
                  if ($('#nombre').val() !== ''){
                    parametros = {};
                    parametros.xMarca = $('#nombre').val();
                    parametros.Tipo = 'Alta';
                    cargaAjax('Administracion/Marcas/ABM-Marcas.jsp', 'recarga', 'xParametros='+JSON.stringify(parametros));
                  } else {
                      showMensaje('La marca no puede ser vac�o', 'alerta');
                  }
              });
          });
      });
</script>
