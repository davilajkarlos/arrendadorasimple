<%-- 
    Document   : index
    Created on : 12/08/2019, 06:46:11 PM
    Author     : k1fox
--%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="consultasBD.Marcas"%>
<%
    try {
        String tbMarcas = "";
        JSONArray marcas = Marcas.ColeccionMarcas(null, "Todos");
        for (int i = 0; i<marcas.length(); i++){
            JSONObject obj = marcas.getJSONObject(i);
            tbMarcas += "<tr>";
            tbMarcas += "<td class='editable'><span>"+obj.getString("Nombre")+"</span></td>";
            tbMarcas += "<td>"+Acciones(obj.getInt("Id"))+"</td>";
            tbMarcas += "</tr>";
        }
%>
<div class="row">
   <div class="col-md-12">
        <div class="card ">
            <div class="card-header">
                <h4 class="card-title"> Marcas</h4>
            </div>
            <div class="card-footer">
                <button  class="btn btn-sm btn-primary" id="nuevo"><i class="tim-icons icon-simple-add"></i> Agregar nuevo pa�s</button>
            </div>
            <div id="algo"></div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table tablesorter " id="tbMarcas">
                        <thead class=" text-primary">
                            <tr>
                                <th>Nombre</th>
                                <th style="width: 30%">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%=tbMarcas%>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div> 
</div>

<script>
    
        $(document).ready(function() {

          $().ready(function() {
            $('#tbMarcas').DataTable();
            $('#nuevo').click(() =>{

                $.post('Administracion/Marcas/popup-NuevaMarca.jsp').done((data) =>{
                    $("<div id=\"popupCrearPais\" title=\"Crear Pais\">" + data + "</div>").dialog({
                        resizable: true,
                        height: '600',
                        width: '80%',
                        modal: true,
                        "close": function (e, ui) {
                            $(this).remove();
                        }
                    });
                });
            });
            
            $('#tbMarcas').off('click', '.editar');
            $('#tbMarcas').on('click', '.editar', (element) =>{
                $(element.currentTarget).parents('tr').find('.accionesGenerales').hide();
                $(element.currentTarget).parents('tr').find('#AccionesModificar').show();
                $(element.currentTarget).parents('tr').find(".editable").each(function(index, object){
                    var valor = $(object).find('span').hide().html();
                    $(object).append('<input type="text" id="inputModificado-'+index+'" class="form-control" value="'+valor+'">');
                    $(object).find('input').focus();
                }); 
            });
            
            $('#tbMarcas').off('click', '.cancelar-btn');
            $('#tbMarcas').on('click', '.cancelar-btn', (element) =>{
                $(element.currentTarget).parents('tr').find('input:not(:checkbox)').remove();
                $(element.currentTarget).parents('tr').find('#filterAcciones').remove();
                $(element.currentTarget).parents('tr').find('span').show();
                $(element.currentTarget).parents('tr').find('#AccionesModificar').hide();
                $(element.currentTarget).parents('tr').find('.accionesGenerales').show();
            });
            
            $('#tbMarcas').off('click', '.guardar-btn');
            $('#tbMarcas').on('click', '.guardar-btn', (element) => {
                var parametros = {};
                parametros.xId= $(element.currentTarget).attr('id');
                parametros.xMarca= $(element.currentTarget).parents('tr').find('#inputModificado-0').val();
                parametros.Tipo = 'Modificacion';
                cargaAjax('Administracion/Marcas/ABM-Marcas.jsp', 'recarga', 'xParametros='+JSON.stringify(parametros));
            });
            
            $('#tbMarcas').off('click', '.eliminar-btn');
            $('#tbMarcas').on('click', '.eliminar-btn', (element)=>{
               swal.fire({
                    title: 'Borrar!',
                    html: "<span>�Seguro quiere eliminar este Pa�s?</span>",
                    type: 'warning',
                    background:'#212529',
                    showCancelButton: true,
                    confirmButtonText: 'Si, Borrar!',
                    cancelButtonText: 'No, cancelar',
                    confirmButtonColor: '#3498db',
                    cancelButtonColor: '#e74c3c',
                    customClass: {
                        container: 'card',
                        popup: 'card-header',
                        header: 'card-header',
                        title: 'card-title',
                        content: 'card-body',
                        footer: 'card-footer'
                      }
                }).then((result) => {
                    if (result.value){
                        var parametros = {};
                        parametros.xId= $(element.currentTarget).attr('id');
                        parametros.Tipo = 'Baja';
                        cargaAjax('Administracion/Marcas/ABM-Marcas.jsp', 'recarga', 'xParametros='+JSON.stringify(parametros));
                    }
                });

            });
        });
    });
   
</script>
<%!
    
    private static String Acciones(long idMarca){
        String texto = "";
        texto +="<button class='btn btn-sm btn-success editar accionesGenerales' id='"+idMarca+"'><i class='tim-icons icon-pencil'></i> Editar</button>"; 
        texto +="<button class='btn btn-sm btn-danger eliminar-btn accionesGenerales' id='"+idMarca+"'><i class='tim-icons icon-trash-simple'></i> Borrar</button>"; 
        texto += "<div id=\"AccionesModificar\" style=\"display: none;\"><button class=\"btn btn-sm btn-success guardar-btn\""+ 
                   "id=\""+idMarca+"\""+
                   "title=\"Guardar Cambios\">"+
                   "<i class='tim-icons icon-check-2'></i> Guardar Cambios"+
                   "</button>"+
                   "<button class=\"btn btn-sm btn-danger cancelar-btn\""+ 
                   "id=\""+idMarca+"\""+
                   "title=\"Cancelar\">"+
                   "<i class='tim-icons icon-simple-remove'></i>"+
                   "</button>"+
                   "</div>";
        return texto;
    }

%>
<%
   }catch(Exception e){
    %><script>showMensaje('<%=e.getMessage()%>', 'error');</script><%
    }
%>
