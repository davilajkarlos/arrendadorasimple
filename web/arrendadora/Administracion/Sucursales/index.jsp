<%-- 
    Document   : index
    Created on : 29/09/2019, 03:06:19 AM
    Author     : k1fox
--%>

<%@page import="consultasBD.Sucursal"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%
    try {
        String tbSucursales = "";
        JSONArray  sucursales = Sucursal.ColeccionSucursales(null, "Todos");
        for (int i = 0; i< sucursales.length(); i++){
            JSONObject obj =  sucursales.getJSONObject(i);
            tbSucursales += "<tr>";
            tbSucursales += "<td class='editable'><span>"+obj.getString("Nombre")+"</span></td>";
            tbSucursales += "<td class='editableSelect'><span id='"+obj.getInt("IdPais")+"' name='Pais'>"+obj.getString("Pais")+"</span></td>";
            tbSucursales += "<td class='editableSelect'><span id='"+obj.getInt("IdDepartamento")+"' pais='"+obj.getInt("IdPais")+"' name='Departamento'>"+obj.getString("Departamento")+"</span></td>";
            tbSucursales += "<td class='editableSelect'><span id='"+obj.getInt("IdCiudad")+"' departamento='"+obj.getInt("IdDepartamento")+"' name='Ciudad'>"+obj.getString("Ciudad")+"</span></td>";
            tbSucursales += "<td class='editable'><span>"+obj.getString("Localidad")+"</span></td>";
            tbSucursales += "<td class='editable'><span>"+obj.getString("NombreCalle")+"</span></td>";
            tbSucursales += "<td class='editable'><span>"+obj.getString("NumeroCalle")+"</span></td>";
            tbSucursales += "<td>"+Acciones(obj.getInt("Id"))+"</td>";
            tbSucursales += "</tr>";
        }
%>
<div class="row">
   <div class="col-md-12">
        <div class="card ">
            <div class="card-header">
                <h4 class="card-title"> Sucursales</h4>
            </div>
            <div class="card-footer">
                <button  class="btn btn-sm btn-primary" id="nuevo" ><i class="tim-icons icon-simple-add"></i> Agregar una nueva Sucursal</button>
            </div>
            <div id="algo"></div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table tablesorter " id="tbSucursal">
                        <thead class=" text-primary">
                            <tr>
                                <th>Nombre</th>
                                <th>Pa�s</th>
                                <th>Departamento</th>
                                <th>Ciudad</th>
                                <th>Lugar</th>
                                <th>Nombre Calle</th>
                                <th>N�mero Calle</th>
                                <th style="width: 10%">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%=tbSucursales%>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div> 
</div>

<script>
    
        $(document).ready(function() {

          $().ready(function() {
            $('#tbSucursal').DataTable();
            $('#nuevo').click(() =>{
                $.post('Administracion/Sucursales/popup-NuevaSucursal.jsp').done((data) =>{
                    $("<div id=\"popup\" title=\"Crear Sucursal\">" + data + "</div>").dialog({
                        resizable: true,
                        height: '600',
                        width: '80%',
                        modal: true,
                        "close": function (e, ui) {
                            $(this).remove();
                        }
                    });
                });
            });
            
            $('#tbSucursal').off('click', '.editar');
            $('#tbSucursal').on('click', '.editar', (element) =>{
                $(element.currentTarget).parents('tr').find('.accionesGenerales').hide();
                $(element.currentTarget).parents('tr').find('#AccionesModificar').show();
                $(element.currentTarget).parents('tr').find(".editable").each(function(index, object){
                    var valor = $(object).find('span').hide().html();
                    $(object).append('<input type="text" id="inputModificado-'+index+'" class="form-control" value="'+valor+'">');
                    $(object).find('input').focus();
                }); 

                $(element.currentTarget).parents('tr').find(".editableSelect").each(function(index, object){
                    
                    var opcion = $(object).children()[0];
                    var valor = $(opcion).hide().attr('id');
                    if ($(opcion).attr('name') === 'Departamento'){
                        var parametros ={
                         idPais: $(opcion).attr('pais')
                        };
                        $.post('Administracion/Ciudades/Componentes/selectDepartamento.jsp', parametros).done(data =>{
                            $(object).append('<div class="selectEditable" id=selectDep>'+data+'</div>');
                             $(object).find('#departamento').val(valor);
                        });

                    }else if ($(opcion).attr('name') === 'Pais'){
                        $.post('Administracion/Localidades/Componentes/selectPais.jsp').done(data =>{
                            $(object).append(data);
                            $(object).find('#pais').val(valor);
                        });
                    }else if ($(opcion).attr('name') === 'Ciudad'){
                        var parametros ={
                         idDepartamento: $(opcion).attr('departamento')
                        };
                        $.post('Administracion/Sucursales/Componentes/selectCiudad.jsp', parametros).done(data =>{
                            $(object).append('<div class="selectEditable" id=selectCiu>'+data+'</div>');
                            $(object).find('#ciudad').val(valor);
                        });
                    }
                });
                
            });
                
            $('#tbSucursal').off('change','#pais');
              $('#tbSucursal').on('change','#pais', () =>{
                  var parametros = {
                      idPais: $('#pais').val()
                  };
                  cargaAjax('Administracion/Ciudades/Componentes/selectDepartamento.jsp','selectDep',parametros);
              });    
              
            $('#tbSucursal').off('change','#departamento');
              $('#tbSucursal').on('change','#departamento', () =>{
                  var parametros = {
                      idDepartamento: $('#departamento').val()
                  };
                  cargaAjax('Administracion/Sucursales/Componentes/selectCiudad.jsp','selectCiu',parametros);
              });    
            
            $('#tbSucursal').off('click', '.cancelar-btn');
            $('#tbSucursal').on('click', '.cancelar-btn', (element) =>{
                $(element.currentTarget).parents('tr').find('input:not(:checkbox)').remove();
                $(element.currentTarget).parents('tr').find('#filterAcciones').remove();
                $(element.currentTarget).parents('tr').find('span').show();
                $(element.currentTarget).parents('tr').find('#AccionesModificar').hide();
                $(element.currentTarget).parents('tr').find('.accionesGenerales').show();
                $(element.currentTarget).parents('tr').find('div.form-group').remove();
                $(element.currentTarget).parents('tr').find('div.selectEditable').remove();
            });
            
            $('#tbSucursal').off('click', '.guardar-btn');
            $('#tbSucursal').on('click', '.guardar-btn', (element) => {
                if ($('#departamento').val() === ''){
                    showMensaje('Debe seleccionar una localidad v�lida', 'alerta');
                }else if ($('#pais').val()===''){
                    showMensaje('Debe seleccionar un pa�s v�lido', 'alerta');
                }else if ($('#ciudad').val()===''){
                    showMensaje('Debe seleccionar una ciudad v�lido', 'alerta');
                }else {
                    var parametros = {};
                    parametros.xId= $(element.currentTarget).attr('id');
                    parametros.xNombre= $(element.currentTarget).parents('tr').find('#inputModificado-0').val();
                    parametros.xLocalidad= $(element.currentTarget).parents('tr').find('#inputModificado-1').val();
                    parametros.xNombreCalle= $(element.currentTarget).parents('tr').find('#inputModificado-2').val();
                    parametros.xNumeroCalle= $(element.currentTarget).parents('tr').find('#inputModificado-3').val();
                    parametros.xIdPais= $(element.currentTarget).parents('tr').find('#pais').val();
                    parametros.xIdDepartamento= $(element.currentTarget).parents('tr').find('#departamento').val();
                    parametros.xIdCiudad= $(element.currentTarget).parents('tr').find('#ciudad').val();
                    parametros.Tipo = 'Modificacion';
                    cargaAjax('Administracion/Sucursales/ABM-Sucursal.jsp', 'recarga', 'xParametros='+JSON.stringify(parametros)); 
                }
                
            });
            
            $('#tbSucursal').off('click', '.eliminar-btn');
            $('#tbSucursal').on('click', '.eliminar-btn', (element)=>{
               swal.fire({
                    title: 'Borrar!',
                    html: "<span>�Seguro quiere eliminar esta Ciudad?</span>",
                    type: 'warning',
                    background:'#212529',
                    showCancelButton: true,
                    confirmButtonText: 'Si, Borrar!',
                    cancelButtonText: 'No, cancelar',
                    confirmButtonColor: '#3498db',
                    cancelButtonColor: '#e74c3c',
                    customClass: {
                        container: 'card',
                        popup: 'card-header',
                        header: 'card-header',
                        title: 'card-title',
                        content: 'card-body',
                        footer: 'card-footer'
                      }
                }).then((result) => {
                    if (result.value){
                        var parametros = {};
                        parametros.xId= $(element.currentTarget).attr('id');
                        parametros.Tipo = 'Baja';
                        cargaAjax('Administracion/Sucursales/ABM-Sucursal.jsp', 'recarga', 'xParametros='+JSON.stringify(parametros));
                    }
                    
                });

            });
        });
    });
   
</script>
<%!
    
    private static String Acciones(long idPais){
        String texto = "";
        texto +="<button class='btn btn-sm btn-success editar accionesGenerales' id='"+idPais+"'><i class='tim-icons icon-pencil'></i> Editar</button>"; 
        texto +="<button class='btn btn-sm btn-danger eliminar-btn accionesGenerales' id='"+idPais+"'><i class='tim-icons icon-trash-simple'></i> Borrar</button>"; 
        texto += "<div id=\"AccionesModificar\" style=\"display: none;\"><button class=\"btn btn-sm btn-success guardar-btn\""+ 
                   "id=\""+idPais+"\""+
                   "title=\"Guardar Cambios\">"+
                   "<i class='tim-icons icon-check-2'></i> Guardar Cambios"+
                   "</button>"+
                   "<button class=\"btn btn-sm btn-danger cancelar-btn\""+ 
                   "id=\""+idPais+"\""+
                   "title=\"Cancelar\">"+
                   "<i class='tim-icons icon-simple-remove'></i>"+
                   "</button>"+
                   "</div>";
        return texto;
    }

%>
<%
   }catch(Exception e){
    %><script>showMensaje('<%=e.getMessage()%>', 'error');</script><%
    }
%>
