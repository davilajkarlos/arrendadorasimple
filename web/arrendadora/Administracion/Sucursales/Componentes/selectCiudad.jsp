<%-- 
    Document   : selectCiudad
    Created on : 30/09/2019, 12:05:52 AM
    Author     : k1fox
--%>

<%@page import="consultasBD.Ciudades"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%
    try {
        JSONObject parametros = new JSONObject();
        if (request.getParameter("idDepartamento") != null && !request.getParameter("idDepartamento").equals("")){
           parametros.put("IdDepartamento", Integer.parseInt(request.getParameter("idDepartamento")));
        }
        String selectCiudad = "";
        if(!parametros.isNull("IdDepartamento")){
            JSONArray deparamento = Ciudades.ColeccionCiudades(parametros, "Todos");

            for (int i = 0; i<deparamento.length(); i++){
                JSONObject obj = deparamento.getJSONObject(i);
                selectCiudad += "<option value='"+obj.getInt("Id")+"'>"+obj.getString("Ciudad")+"</option>";
            }
            
        }
%>
    <div class="form-group">
        <label>Ciudad: </label>
        <select class="form-dataTable form-control" id="ciudad">
            <option value=''>Seleccione una Ciudad</option>
            <%=selectCiudad%>
        </select>
    </div>
<%
   }catch(Exception e){
    %><script>showMensaje('<%=e.getMessage()%>', 'error');</script><%
    }
%>