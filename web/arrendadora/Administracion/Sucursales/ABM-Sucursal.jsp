<%-- 
    Document   : ABM-Sucursal
    Created on : 30/09/2019, 12:43:53 AM
    Author     : k1fox
--%>

<%@page import="consultasBD.Sucursal"%>
<%@page import="org.json.JSONObject"%>
<%
    
    String mensajeJs = "";
    String tipoJs = "";
    String tipo = "";
    try{
        JSONObject sucursal = new JSONObject(request.getParameter("xParametros"));
        tipo = sucursal.getString("Tipo");
        
     JSONObject retorno = Sucursal.AccionSucursal(sucursal, tipo);
     if (retorno.has("Nivel")){
        if (retorno.getInt("Cod") == 1062){
             mensajeJs = "La sucursal ya existe";
             tipoJs = "alerta";
         }else {
             mensajeJs = "Error Cod: "+retorno.getInt("Cod")+"<br/>";
             mensajeJs += "Nivel: "+retorno.getString("Nivel")+"<br/>";
             mensajeJs += "Mensaje: "+retorno.getString("Mensaje");
             tipoJs = "error";
         }
     }else{
         if(retorno.getInt("Cod") == 0){
             mensajeJs = retorno.getString("Mensaje");
             tipoJs = "exito";
         }else {
            throw new Exception(retorno.getString("Mensaje"));  
         }
                   
     }
%>
<script> 
    showMensaje('<%=mensajeJs%>','<%=tipoJs%>');
    if('<%=tipoJs%>' === 'exito'){
        $('#popup').remove();
        cargaAjax('./Administracion/Sucursales/', 'accion');
    }
</script>
<%   
    }catch(Exception ex)
    {
%><script> showMensaje(`<%=ex.getMessage()%>`,"error");</script><%
    }
%>