<%-- 
    Document   : popup-NuevoPais
    Created on : 12/08/2019, 07:09:42 PM
    Author     : k1fox
--%>
<%@page import="consultasBD.Departamentos"%>
<%@page import="org.json.JSONObject"%>
<%@page import="consultasBD.Paises"%>
<%@page import="org.json.JSONArray"%>
<%
    try {
       
%>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Ingresar una Sucursal</h5>
            </div>
            <div class="card-body">
                <form>
                    <div class="row">
                        <div class="col-md-5 pr-md-1">
                            <div class="form-group">
                                <label>Nombre de la Sucursal</label>
                                <input type="text" class="form-control" id="nombre" placeholder="Nombre" value="">
                            </div>
                        </div>
                        <div class="col-md-5 pr-md-1">
                            <div class="form-group">
                                <label>Localidad </label>
                                <input type="text" class="form-control" id="localidad" placeholder="Localidad" value="">
                            </div>
                        </div>
                        <div class="col-md-5 pr-md-1">
                            <div class="form-group">
                                <label>Nombre de la calle </label>
                                <input type="text" class="form-control" id="nombreCalle" placeholder="Calle" value="">
                            </div>
                        </div>
                        <div class="col-md-5 pr-md-1">
                            <div class="form-group">
                                <label>N�mero de la calle </label>
                                <input type="text" class="form-control" id="numeroCalle" placeholder="N�mero" value="">
                            </div>
                        </div>
                        <div class="col-md-5 pr-md-1" id="selectPais"></div>
                        <div class="col-md-5 pr-md-1" id="selectDepartamento"></div>
                        <div class="col-md-5 pr-md-1" id="selectCiudad"></div>
                    </div>
                </form>
            </div>
            <div class="card-footer">
                <button class="btn btn-fill btn-primary" id="guardar">Guardar</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {

          $().ready(function() {
            cargaAjax('Administracion/Localidades/Componentes/selectPais.jsp','selectPais');
            $('#popup').off('change','#pais');
            $('#popup').on('change','#pais', () =>{
                var parametros = {
                    idPais: $('#pais').val()
                };
                cargaAjax('Administracion/Ciudades/Componentes/selectDepartamento.jsp','selectDepartamento',parametros);
            });
            
            $('#popup').off('change','#departamento');
            $('#popup').on('change','#departamento', () =>{
                var parametros = {
                    idDepartamento: $('#departamento').val()
                };
                cargaAjax('Administracion/Sucursales/Componentes/selectCiudad.jsp','selectCiudad',parametros);
            }); 
              
            $('#guardar').click(() =>{
                if ($('#nombre').val() === ''){
                 showMensaje('La ciudad no puede ser vac�o', 'alerta');
                } else if ($('#pais').val() === ''){
                    showMensaje('Seleccione el pa�s', 'alerta');
                }else if (!$('#departamento').length || $('#departamento').val() === ''){
                    showMensaje('Seleccione el departamento', 'alerta');
                }else{
                    parametros = {};
                    parametros.xNombre = $('#nombre').val();
                    parametros.xIdDepartamento = $('#departamento').val();
                    parametros.xIdPais = $('#pais').val();
                    parametros.xIdCiudad = $('#ciudad').val();
                    parametros.xLocalidad = $('#localidad').val();
                    parametros.xNombreCalle = $('#nombreCalle').val();
                    parametros.xNumeroCalle = $('#numeroCalle').val();
                    parametros.Tipo = 'Alta';
                    cargaAjax('Administracion/Sucursales/ABM-Sucursal.jsp', 'recarga', 'xParametros='+JSON.stringify(parametros));
                }
            });
          });
      });
</script>
<%
   }catch(Exception e){
    %><script>showMensaje('<%=e.getMessage()%>', 'error');</script><%
    }
%>