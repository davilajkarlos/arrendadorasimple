<%-- 
    Document   : index
    Created on : 12/08/2019, 06:46:11 PM
    Author     : k1fox
--%>
<%@page import="consultasBD.ModelosCoches"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="consultasBD.Paises"%>
<%
    try {
        String tbModelo = "";
        JSONArray modelos = ModelosCoches.ColeccionModeloCoches(null, "Todos");
        for (int i = 0; i<modelos.length(); i++){
            JSONObject obj = modelos.getJSONObject(i);
            tbModelo += "<tr>";
            tbModelo += "<td class='editable'><span>"+obj.getString("Nombre")+"</span></td>";
            tbModelo += "<td class='editableSelect'><span id='"+obj.getInt("IdMarca")+"' name='Marca'>"+obj.getString("Marca")+"</span></td>";
            tbModelo += "<td>"+Acciones(obj.getInt("Id"))+"</td>";
            tbModelo += "</tr>";
        }
%>
<div class="row">
   <div class="col-md-12">
        <div class="card ">
            <div class="card-header">
                <h4 class="card-title"> Modelos</h4>
            </div>
            <div class="card-footer">
                <button  class="btn btn-sm btn-primary" id="nuevo"><i class="tim-icons icon-simple-add"></i> Agregar nueva Modelos</button>
            </div>
            <div id="algo"></div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table tablesorter " id="tbModelo">
                        <thead class=" text-primary">
                            <tr>
                                <th>Nombre</th>
                                <th>Marca</th>
                                <th style="width: 30%">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%=tbModelo%>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div> 
</div>

<script>
    
        $(document).ready(function() {

          $().ready(function() {
            $('#tbModelo').DataTable();
            $('#nuevo').click(() =>{

                $.post('Administracion/Modelos/popup-NuevoModelo.jsp').done((data) =>{
                    $("<div id=\"popup\" title=\"Crear Nuevo Modelo\">" + data + "</div>").dialog({
                        resizable: true,
                        height: '600',
                        width: '80%',
                        modal: true,
                        "close": function (e, ui) {
                            $(this).remove();
                        }
                    });
                });
            });
            
            $('#tbModelo').off('click', '.editar');
            $('#tbModelo').on('click', '.editar', (element) =>{
                $(element.currentTarget).parents('tr').find('.accionesGenerales').hide();
                $(element.currentTarget).parents('tr').find('#AccionesModificar').show();
                $(element.currentTarget).parents('tr').find(".editable").each(function(index, object){
                    var valor = $(object).find('span').hide().html();
                    $(object).append('<input type="text" id="inputModificado-'+index+'" class="form-control" value="'+valor+'">');
                    $(object).find('input').focus();
                }); 
                
                $(element.currentTarget).parents('tr').find(".editableSelect").each(function(index, object){

                    var opcion = $(object).children()[0];
                    var valor = $(opcion).hide().attr('id');
                    if ($(opcion).attr('name') === 'Marca'){
                        $.post('Administracion/Modelos/Componentes/selectMarcas.jsp').done(data =>{
                            $(object).append(data);
                            $(object).find('#marca').val(valor);
                        });
                    }
                });
            });
            
            $('#tbModelo').off('click', '.cancelar-btn');
            $('#tbModelo').on('click', '.cancelar-btn', (element) =>{
                $(element.currentTarget).parents('tr').find('input:not(:checkbox)').remove();
                $(element.currentTarget).parents('tr').find('#filterAcciones').remove();
                $(element.currentTarget).parents('tr').find('span').show();
                $(element.currentTarget).parents('tr').find('#AccionesModificar').hide();
                $(element.currentTarget).parents('tr').find('.accionesGenerales').show();
                $(element.currentTarget).parents('tr').find('div.form-group').remove();
            });
            
            $('#tbModelo').off('click', '.guardar-btn');
            $('#tbModelo').on('click', '.guardar-btn', (element) => {
                if ($('#pais').val()===''){
                    showMensaje('Debe seleccionar un pa�s v�lido', 'alerta');
                }else {
                    var parametros = {};
                    parametros.xId= $(element.currentTarget).attr('id');
                    parametros.xModelo= $(element.currentTarget).parents('tr').find('#inputModificado-0').val();
                    parametros.xIdMarca= $(element.currentTarget).parents('tr').find('#marca').val();
                    parametros.Tipo = 'Modificacion';
                    cargaAjax('Administracion/Modelos/ABM-Modelos.jsp', 'recarga', 'xParametros='+JSON.stringify(parametros)); 
                }
                
            });
            
            $('#tbModelo').off('click', '.eliminar-btn');
            $('#tbModelo').on('click', '.eliminar-btn', (element)=>{
               swal.fire({
                    title: 'Borrar!',
                    html: "<span>�Seguro quiere eliminar esta Ciudad?</span>",
                    type: 'warning',
                    background:'#212529',
                    showCancelButton: true,
                    confirmButtonText: 'Si, Borrar!',
                    cancelButtonText: 'No, cancelar',
                    confirmButtonColor: '#3498db',
                    cancelButtonColor: '#e74c3c',
                    customClass: {
                        container: 'card',
                        popup: 'card-header',
                        header: 'card-header',
                        title: 'card-title',
                        content: 'card-body',
                        footer: 'card-footer'
                      }
                }).then((result) =>{
                    if (result.value){
                        var parametros = {};
                        parametros.xId= $(element.currentTarget).attr('id');
                        parametros.Tipo = 'Baja';
                        cargaAjax('Administracion/Modelos/ABM-Modelos.jsp', 'recarga', 'xParametros='+JSON.stringify(parametros));
                    }
                    
                });

            });
        });
    });
   
</script>
<%!
    
    private static String Acciones(long idModelo){
        String texto = "";
        texto +="<button class='btn btn-sm btn-success editar accionesGenerales' id='"+idModelo+"'><i class='tim-icons icon-pencil'></i> Editar</button>"; 
        texto +="<button class='btn btn-sm btn-danger eliminar-btn accionesGenerales' id='"+idModelo+"'><i class='tim-icons icon-trash-simple'></i> Borrar</button>"; 
        texto += "<div id=\"AccionesModificar\" style=\"display: none;\"><button class=\"btn btn-sm btn-success guardar-btn\""+ 
                   "id=\""+idModelo+"\""+
                   "title=\"Guardar Cambios\">"+
                   "<i class='tim-icons icon-check-2'></i> Guardar Cambios"+
                   "</button>"+
                   "<button class=\"btn btn-sm btn-danger cancelar-btn\""+ 
                   "id=\""+idModelo+"\""+
                   "title=\"Cancelar\">"+
                   "<i class='tim-icons icon-simple-remove'></i>"+
                   "</button>"+
                   "</div>";
        return texto;
    }

%>
<%
   }catch(Exception e){
    %><script>showMensaje('<%=e.getMessage()%>', 'error');</script><%
    }
%>
