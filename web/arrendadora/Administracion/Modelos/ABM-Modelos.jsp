<%-- 
    Document   : ABM-ModelosCoches
    Created on : 30/09/2019, 12:43:53 AM
    Author     : k1fox
--%>

<%@page import="consultasBD.ModelosCoches"%>
<%@page import="org.json.JSONObject"%>
<%
    
    String mensajeJs = "";
    String tipoJs = "";
    String tipo = "";
    try{
        JSONObject modelos = new JSONObject(request.getParameter("xParametros"));
        tipo = modelos.getString("Tipo");
        
     JSONObject retorno = ModelosCoches.AccionModeloCoche(modelos, tipo);
     if (retorno.has("Nivel")){
        if (retorno.getInt("Cod") == 1062){
             mensajeJs = "El modelos ya existe";
             tipoJs = "alerta";
         }else {
             mensajeJs = "Error Cod: "+retorno.getInt("Cod")+"<br/>";
             mensajeJs += "Nivel: "+retorno.getString("Nivel")+"<br/>";
             mensajeJs += "Mensaje: "+retorno.getString("Mensaje");
             tipoJs = "error";
         }
     }else{
         if(retorno.getInt("Cod") == 0){
             mensajeJs = retorno.getString("Mensaje");
             tipoJs = "exito";
         }else {
            throw new Exception(retorno.getString("Mensaje"));  
         }
                   
     }
%>
<script> 
    showMensaje('<%=mensajeJs%>','<%=tipoJs%>');
    if('<%=tipoJs%>' === 'exito'){
        $('#popup').remove();
        cargaAjax('./Administracion/Modelos/', 'divModelos');
    }
</script>
<%   
    }catch(Exception ex)
    {
%><script> showMensaje(`<%=ex.getMessage()%>`,"error");</script><%
    }
%>