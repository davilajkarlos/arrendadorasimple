<%-- 
    Document   : popup-NuevoPais
    Created on : 12/08/2019, 07:09:42 PM
    Author     : k1fox
--%>

<%@page import="consultasBD.Paises"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%
    try {
        
%>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Ingresar una Modelos</h5>
            </div>
            <div class="card-body">
                <form>
                    <div class="row">
                        <div class="col-md-5 pr-md-1">
                            <div class="form-group">
                                <label>Nombre del Departamento/Estado </label>
                                <input type="text" class="form-control" id="nombre" placeholder="Modelos" value="">
                            </div>
                        </div>
                        <div class="col-md-5 pr-md-1" id="selectMarcas"></div>
                    </div>
                </form>
            </div>
            <div class="card-footer">
                <button class="btn btn-fill btn-primary" id="guardar">Guardar</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {

          $().ready(function() {
              cargaAjax('Administracion/Modelos/Componentes/selectMarcas.jsp','selectMarcas');
              $('#guardar').click(() =>{
                  if ($('#nombre').val() === ''){
                    showMensaje('El modelo no puede ser vac�o', 'alerta');
                  } else if ($('#marca').val() === ''){
                    showMensaje('Seleccione una marca', 'alerta');
                  }else {
                    parametros = {};
                    parametros.xModelo = $('#nombre').val();
                    parametros.xIdMarca = $('#marca').val();
                    parametros.Tipo= 'Alta';
                    cargaAjax('Administracion/Modelos/ABM-Modelos.jsp', 'recarga', 'xParametros='+JSON.stringify(parametros));
                  }
              });
          });
      });
</script>
<%
   }catch(Exception e){
    %><script>showMensaje('<%=e.getMessage()%>', 'error');</script><%
    }
%>