<%-- 
    Document   : selectMarca
    Created on : 26/08/2019, 02:06:03 AM
    Author     : k1fox
--%>

<%@page import="consultasBD.Marcas"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>
<%
    try {

        String selectMarca = "";
        JSONArray marcas = Marcas.ColeccionMarcas(null, "Todos");
        for (int i = 0; i<marcas.length(); i++){
            JSONObject obj = marcas.getJSONObject(i);
            selectMarca += "<option value='"+obj.getInt("Id")+"'>"+obj.getString("Nombre")+"</option>";
        }
%>
    <div class="form-group">
        <label>Marcas: </label>
        <select class="form-dataTable form-control" id="marca">
            <option value=''>Seleccione una marca</option>
            <%=selectMarca%>
        </select>
    </div>
<%
   }catch(Exception e){
    %><script>showMensaje('<%=e.getMessage()%>', 'error');</script><%
    }
%>