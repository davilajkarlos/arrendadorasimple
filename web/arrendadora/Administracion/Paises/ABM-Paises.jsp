<%-- 
    Document   : ABM-Paises
    Created on : 17 ago 2019, 16:51:34
    Author     : k1fox
--%>

<%@page import="consultasBD.Paises"%>
<%@page import="org.json.JSONObject"%>
<%
    JSONObject pais = new JSONObject();
    String mensajeJs = "";
    String tipoJs = "";
    String tipo = "";
    try{
        if (request.getParameter("Nombre") != null){
            pais.put("xNombre", request.getParameter("Nombre"));
        }
        if (request.getParameter("Id") != null){
            pais.put("xId", request.getParameter("Id"));
        }
        if (request.getParameter("Tipo") != null){
            tipo = request.getParameter("Tipo");
        }
     JSONObject retorno = Paises.AccionPaises(pais, tipo);
     if (retorno.has("Nivel")){
        if (retorno.getInt("Cod") == 1062){
             mensajeJs = "El Pais ya existe";
             tipoJs = "alerta";
         }else {
             mensajeJs = "Error Cod: "+retorno.getInt("Cod")+"<br/>";
             mensajeJs += "Nivel: "+retorno.getString("Nivel")+"<br/>";
             mensajeJs += "Mensaje: "+retorno.getString("Mensaje");
             tipoJs = "error";
         }
     }else{
         if(retorno.getInt("Cod") == 0){
            mensajeJs = retorno.getString("Mensaje");
            tipoJs = "exito";
         }else {
            throw new Exception(retorno.getString("Mensaje"));  
         }
                   
     }
%>
<script> 
    if('<%=tipoJs%>' === 'exito'){
        $('#popupCrearPais').remove();
        cargaAjax('./Administracion/Paises/', 'tablaPaises');
    }
    showMensaje(`<%=mensajeJs%>`,'<%=tipoJs%>');
</script>
<%   
    }catch(Exception ex)
    {
%><script> showMensaje(`<%=ex.getMessage()%>`,"error");</script><%
    }
%>