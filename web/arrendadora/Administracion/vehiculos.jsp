<%-- 
    Document   : vehiculos
    Created on : 6/10/2019, 10:00:15 PM
    Author     : k1fox
--%>

<div class="" >
    <ul class="nav" style="border-bottom: 1px solid #ddd;">
        <li class="listaAdmin" ><a target="Coches" href="#divCoches" data-toggle="tab" class="btn btn-sm active" ><strong>Coches</strong></a></li>
        <li class="listaAdmin"><a target="Modelos" href="#divModelos" data-toggle="tab" class="btn btn-sm" ><strong>Modelos</strong></a></li>
        <li class="listaAdmin"><a target="Marcas" href="#divMarcas" data-toggle="tab" class="btn btn-sm" ><strong>Marcas</strong></a></li>
        <li class="listaAdmin"><a target="Clasificaciones" href="#divClasificaciones" data-toggle="tab" class="btn btn-sm" ><strong>Clasificaciones</strong></a></li>
    </ul>

    <div id="divPadding" class="tab-content" style="padding-bottom: 0px; display:none;"></div>
    <div class="tab-content">

        <div id="divCoches" class="tab-pane active" style="padding-bottom: 0px;">
            <div id="tablaCoches"></div>
        </div>

        <div id="divModelos" class="tab-pane" style="padding-bottom: 0px;">
            <div id="tablaModelos"></div>
        </div>

        <div id="divMarcas" class="tab-pane" style="padding-bottom: 0px;">
            <div id="tablaMarcas"></div>
        </div>
        <div id="divClasificaciones" class="tab-pane" style="padding-bottom: 0px;">
            <div id="tablaClasificaciones"></div>
        </div>
    </div>
</div>
<script>
     $(document).ready(function() {

          $().ready(function() {
            cargaAjax('Administracion/Coches/index.jsp', 'tablaCoches');  
            $('.listaAdmin >a.active').css({"color": '#ff5e00'});
              
            $('.listaAdmin').click(element =>{
              var url = $(element.currentTarget).children().attr('target');
               if (url !== "" && url !== null){
                   if (url === 'Coches'){
                       cargaAjax('Administracion/Coches/index.jsp', 'tablaCoches');
                       $('#tablaModelos').empty();
                       $('#tablaMarcas').empty();
                       $('#tablaClasificaciones').empty();
                   }else if (url === 'Modelos'){
                       cargaAjax('Administracion/Modelos/index.jsp', 'tablaModelos');
                       $('#tablaCoches').empty();
                       $('#tablaMarcas').empty();
                       $('#tablaClasificaciones').empty();
                   } else if (url === 'Marcas'){
                       cargaAjax('Administracion/Marcas/index.jsp', 'tablaMarcas');
                       $('#tablaCoches').empty();
                       $('#tablaModelos').empty();
                       $('#tablaClasificaciones').empty();
                   }else if (url === 'Clasificaciones'){
                       cargaAjax('Administracion/ClasificacionCoches/index.jsp', 'tablaClasificaciones');
                       $('#tablaCoches').empty();
                       $('#tablaModelos').empty();
                       $('#tablaMarcas').empty();
                   }
               } else {
                   showMensaje('Proximamente en Arrendadora', 'alerta');
               }
               $('.listaAdmin >a').css({"color": '#ffffff'});
               $(element.currentTarget).children().css({"color": '#ff5e00'});
            });
      });
    });
