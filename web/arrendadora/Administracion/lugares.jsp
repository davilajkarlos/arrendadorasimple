<%-- 
    Document   : ejemplo
    Created on : 12/08/2019, 12:42:28 AM
    Author     : k1fox
--%>

<div class="" >
    <ul class="nav" style="border-bottom: 1px solid #ddd;">
        <li class="listaAdmin" ><a target="Paises" href="#divPaises" data-toggle="tab" class="btn btn-sm active" ><strong>Paises</strong></a></li>
        <li class="listaAdmin"><a target="Ciudades" href="#divCiudades" data-toggle="tab" class="btn btn-sm" ><strong>Ciudades</strong></a></li>
        <li class="listaAdmin"><a target="Localidad" href="#divLocal" data-toggle="tab" class="btn btn-sm" ><strong>Deparamento</strong></a></li>
    </ul>

    <div id="divPadding" class="tab-content" style="padding-bottom: 0px; display:none;"></div>
    <div class="tab-content">
        <!-- #Vacunas -->
        <div id="divPaises" class="tab-pane active" style="padding-bottom: 0px;">
            <div id="tablaPaises"></div>
        </div>
        <!-- #Antiparasitarios -->
        <div id="divCiudades" class="tab-pane" style="padding-bottom: 0px;">
            <div id="tablaCiudad"></div>
        </div>
        <!-- #Peluquería -->
        <div id="divLocal" class="tab-pane" style="padding-bottom: 0px;">
            <div id="tablaLocal"></div>
        </div>
    </div>
</div>
<script>
     $(document).ready(function() {

          $().ready(function() {
            cargaAjax('Administracion/Paises/index.jsp', 'tablaPaises');  
            $('.listaAdmin >a.active').css({"color": '#ff5e00'});
              
            $('.listaAdmin').click(element =>{
              var url = $(element.currentTarget).children().attr('target');
               if (url !== "" && url !== null){
                   if (url === 'Paises'){
                       cargaAjax('Administracion/Paises/index.jsp', 'tablaPaises');
                       $('#tablaCiudad').empty();
                       $('#tablaLocal').empty();
                   }else if (url === 'Ciudades'){
                       cargaAjax('Administracion/Ciudades/index.jsp', 'tablaCiudad');
                       $('#tablaPaises').empty();
                       $('#tablaLocal').empty();
                   } else if (url === 'Localidad'){
                       cargaAjax('Administracion/Localidades/index.jsp', 'tablaLocal');
                       $('#tablaPaises').empty();
                       $('#tablaCiudad').empty();
                   }
               } else {
                   showMensaje('Proximamente en Arrendadora', 'alerta');
               }
               $('.listaAdmin >a').css({"color": '#ffffff'});
               $(element.currentTarget).children().css({"color": '#ff5e00'});
            });
      });
    });
</script>