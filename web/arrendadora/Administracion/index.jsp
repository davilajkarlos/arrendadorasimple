<%-- 
    Document   : index
    Created on : 11/08/2019, 10:03:00 PM
    Author     : k1fox
--%>

<div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="title">100 Awesome Nucleo Icons</h5>
                    <p class="category">Handcrafted by our friends from <a href="https://nucleoapp.com/?ref=1712">NucleoApp</a></p>
                </div>
                <div class="card-body all-icons">
                    <div class="row">
                        <div class="font-icon-list col-lg-2 col-md-3 col-sm-4 col-xs-8 col-xs-8">
                            <a href="javascript:void(0);" class="opciones" url="./Administracion/vehiculos.jsp">
                                <div class="font-icon-detail">
                                    <i class="tim-icons icon-bus-front-12"></i>
                                    <p>Coches</p>
                                </div>
                            </a>
                        </div>
                        <div class="font-icon-list col-lg-2 col-md-3 col-sm-4 col-xs-8 col-xs-8">
                            <a href="javascript:void(0);" class="opciones" url="./Administracion/Usuarios/">
                                <div class="font-icon-detail">
                                    <i class="tim-icons icon-single-02"></i>
                                    <p>Usuarios/Clientes</p>
                                </div>
                            </a>
                        </div>
                        <div class="font-icon-list col-lg-2 col-md-3 col-sm-4 col-xs-8 col-xs-8">
                            <a href="javascript:void(0);" class="opciones" url="./Administracion/Sucursales/">
                                <div class="font-icon-detail">
                                    <i class="tim-icons icon-square-pin"></i>
                                    <p>Sucursales</p>
                                </div>
                            </a>
                        </div>
                        <div class="font-icon-list col-lg-2 col-md-3 col-sm-4 col-xs-8 col-xs-8">
                            <a href="javascript:void(0);" class="opciones" url="./Administracion/lugares.jsp">
                                <div class="font-icon-detail">
                                    <i class="tim-icons icon-world"></i>
                                    <p>Paises/
                                        Ciudades/
                                        Localidades</p>
                                </div>
                            </a>
                        </div>
                        <div class="font-icon-list col-lg-2 col-md-3 col-sm-4 col-xs-8 col-xs-8">
                            <a href="javascript:void(0);" class="opciones" url="./Administracion/Depositos/">
                                <div class="font-icon-detail">
                                    <i class="tim-icons icon-app"></i>
                                    <p>Dep�sito</p>
                                </div>
                            </a>
                        </div>
                    </div>
                        
                </div>
            </div>
        </div>
</div>
<div id="accion"></div>
<script>
    $(document).ready(function() {

          $().ready(function() {
            $('.opciones').click(element =>{
               var url = $(element.currentTarget).attr('url'); 
               if (url !== "" && url !== null){
                   cargaAjax(url, 'accion','');
               } else {
                   showMensaje('Proximamente en Arrendadora', 'alerta');
               }
               
            });
      });
    });
</script>