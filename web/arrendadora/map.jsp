<%-- 
    Document   : home
    Created on : 10/08/2019, 12:04:35 AM
    Author     : k1fox
--%>

<div class="row">
    <div class="col-md-12">
        <div class="card card-plain">
            <div class="card-header">
                Google Maps
            </div>
            <div class="card-body">
                <div id="map" class="map"></div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        // Javascript method's body can be found in assets/js/demos.js
        demo.initGoogleMaps();
    });
</script>