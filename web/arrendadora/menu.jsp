<%-- 
    Document   : menu
    Created on : 11/08/2019, 04:50:10 PM
    Author     : k1fox
--%>

<%@page import="java.nio.charset.StandardCharsets"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.io.IOException"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.InputStream"%>
<%@ include file="../validate.jsp" %>
<%
    String fileName = "/WEB-INF/menu.json";
    InputStream ins = application.getResourceAsStream(fileName);
    String result = "";
    try
    {
        if(ins == null)
        {
            response.setStatus(response.SC_NOT_FOUND);
        }
        else
        {
            BufferedReader br = new BufferedReader((new InputStreamReader(ins,StandardCharsets.UTF_8)));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }

            result = sb.toString();
        }
    }
    catch(IOException e)
    {
        out.println(e.getMessage());
    }
    
    String html = "";
    String active = "class=\"active\"";
    JSONArray arr = new JSONArray(result);
     for (int i=0; i < arr.length(); i++) {
         JSONObject objItem = arr.getJSONObject(i);
         if ((int) session.getAttribute("Tipo") <= objItem.getInt("Nivel")){
             html+="<li "+active+">"+
                    "<a href=\"javascript:void(0)\" class=\"menu\" url=\""+objItem.getString("Url")+"\">"+
                    "<i class=\""+objItem.getString("Class")+"\"></i>"+
                    "<p>"+objItem.getString("Descripcion")+"</p>"+
                    "</a>"+
                    "</li>";
         active = "";
         }
     };
     
     out.print(html);
%>
<script>
        $(document).ready(function() {

          $().ready(function() {
            var active = $('li.active').find('a').attr('url');
            var delimiter = '.jsp';
            if (active.includes('./')){
                delimiter = '';
            }
            cargaAjax(active+delimiter, 'contenedor', '');
            $('.menu').click( element => {
               menu(element);
            });
          });
      });
</script>