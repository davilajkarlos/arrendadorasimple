<%-- 
    Document   : validate
    Created on : 11/08/2019, 05:50:03 PM
    Author     : k1fox
--%>

<%
    try
    {
        if(session != null)
        {
            //En caso de que el usuario sea invalido
            if(session.getAttribute("Id")==null)
            {
                System.out.println("El Usuario es inv�lido");
                 throw new Exception("La sesion ha expirado");
            }
        }
        else
        {
            System.out.println("La Session ha expirado!");
             throw new Exception("La sesion ha expirado");
        }
    }
    catch(Exception ex)
    {
%><script>
    document.location = '/ArrendadoraAutosSimple/index.jsp';
    javascript: showMensaje('<%=ex.getMessage()%>', 'info');
</script><%
        return;
    }
%>